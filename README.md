# The Põder Static Analysis Framework

Põder is a static analysis framework combining demand-driven constraint solving with SMT-Based verfication. 
It is developed by Kalmer Apinis and others at the University of Tartu, Laboratory for Software Science. 

This work is supported by the Estonian Research Council grant PSG61 "High-Assurance Software Development With Sound Interactive Static Analysis".

### Building & Running

##### Using SBT

    git clone https://bitbucket.org/kalmera/poder.git
    cd poder
    sbt assembly
    java -jar target/scala-2.12/põder-1.0.jar  

##### Using IntelliJ IDEA
 
1. Clone `git clone https://bitbucket.org/kalmera/poder.git`
2. In IntelliJ IDEA, select "Import Project" or File->New->Project from Existing Sources...
3. From "import project from external model" select "sbt", then click "next" and "finish"
4. We suggest to "enable auto-import" of sbt.
5. Open P6der.scala and run the põder.P6der object using "Run 'P6der'".
