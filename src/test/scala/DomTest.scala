import põder.dom._
import org.scalatest.FunSuite

class DomTest extends FunSuite {
  import põder.dom.Logic._

  val D = LogLat

  val a = LVar(0); a.alias = "a"
  val b = LVar(1); b.alias = "b"
  val c = LVar(2); c.alias = "c"


  val tp: Logic  = True
  val bt: Logic  = False
  val b1: Logic = eqL(a,1)
  val b2: Logic = cop("<=", a, 1)
  val b3: Logic = cop("<=", 1, a)
  val b4: Logic = forall(b,cop("<=", a, 1))
  val b5: Logic = forall(b,cop("<=", a, 10))
  val b6: Logic = forall(b,cop("<=", a, b))
  val b7: Logic = or(cop("<=", a, 1), cop(">", a, 1))

  val all: Seq[Logic] = Seq(tp,bt,b1,b2,b3,b4,b5)

  test("true") {
    assert(true)
  }

  test("simple"){
    assert(D.leq(b1,b2))
    assert(D.leq(b1,b3))
    assert(!D.leq(b2,b3))
    assert(!D.leq(b3,b2))
  }

  test("forall"){
    assert(D.leq(b4,b5))
    assert(!D.leq(b4,b6))
    assert(!D.leq(b6,b4))
  }
//
//  test("non-trivial"){
//    val x = ForallL(LVar(0),Or(And(App(BinPropOp("="),App(LVar(1)), App(IVal(20))), App(BinPropOp("="),App(LVar(2)), App(IVal(30))), App(BinPropOp("="),App(LVar(3)), App(IVal(5))), And(App(BinPropOp("="),App(LVar(3)), App(IVal(6))), App(BinPropOp("="),App(LVar(3)), App(BinPropOp("+"),App(LVar(1)), App(IVal(1)))), App(BinPropOp("="),App(LVar(2)), App(IVal(30))), App(BinPropOp("="),App(LVar(1)), App(IVal(5))), App(BinPropOp(">="),App(IVal(10)), App(LVar(1)))))))
//    val y = ForallL(LVar(0),   And(App(BinPropOp("="),App(LVar(1)), App(IVal(20))), App(BinPropOp("="),App(LVar(2)), App(IVal(30))), App(BinPropOp("="),App(LVar(3)), App(IVal(5)))))
//
//    assert(D.leq(y,x))
//  }

  test("non-trivial 222"){
    val x = ForallL(LVar(0),Or(And(App(BinIntProp("="),App(LVar(1)), App(IVal(20))), App(BinIntProp("="),App(LVar(2)), App(IVal(30))), App(BinIntProp("="),App(LVar(3)), App(IVal(5)))), And(App(BinIntProp("="),App(LVar(3)), App(IVal(6))), App(BinIntProp("="),App(LVar(3)), App(BinIntProp("+"),App(LVar(1)), App(IVal(1)))), App(BinIntProp("="),App(LVar(2)), App(IVal(30))), App(BinIntProp("="),App(LVar(1)), App(IVal(5))), App(BinIntProp(">="),App(IVal(10)), App(LVar(1))))))
    val y = ForallL(LVar(0),   And(App(BinIntProp("="),App(LVar(1)), App(IVal(20))), App(BinIntProp("="),App(LVar(2)), App(IVal(30))), App(BinIntProp("="),App(LVar(3)), App(IVal(5)))))

    assert(D.leq(y,x))
  }

  test("equal extreme" ) {
    assert(!D.equal(tp, bt))
    assert(!D.equal(bt, tp))
    assert(D.equal(bt, bt))
    assert(D.equal(tp, tp))
  }
  test("equal" ){
    assert(D.leq(b7,tp))
    assert(D.leq(tp,b7))
    for { x <- all
          y <- all } {
      if (D.equal(x, y)) {
        assert(D.leq(x, y))
        assert(D.leq(y,x))
      }
      if (D.leq(x, y) && D.leq(y, x)) {
        assert(D.equal(x, y))
        assert(D.equal(y, x))
      }
    }

  }

  test("reflexivity") {
    all.foreach{
      x => assert(D.leq(x,x), "for x = "+x.toSimpleString)
    }
  }

  test("leq top") {
    all.foreach{
      x =>
        assert(D.leq(x,True), "for x = "+x.toSimpleString)
        if(x!=True) assert(!D.leq(True,x), "for x = "+x.toSimpleString)
    }
  }

  test("bot leq") {
    all.foreach{
      x => assert(D.leq(False,x), "for x = "+x.toSimpleString)
    }
  }

  test("leq join"){
    for (x <- all; y <- all){
      assert(D.leq(x,D.join(x,y)), "for x = "+x.toSimpleString)
      assert(D.leq(y,D.join(x,y)), "for y = "+x.toSimpleString)
    }
  }

}
