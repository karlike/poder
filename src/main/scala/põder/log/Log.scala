package põder.log

import javafx.application.Platform
import scalafx.beans.property.StringProperty

import scala.collection.mutable

object Log {
  var simple_log = false

  class SubLog {
    val property: StringProperty = StringProperty("")

    def enqueue(elems: String*): Unit = {
      Platform.runLater(() => {
        val v = property.value
        if (v.isEmpty)
          property.value = elems.mkString("\n")
        else
          property.value = v ++ "\n" ++ elems.mkString("\n")
      })
    }
  }


  val logs = mutable.Map.empty[Option[Any], SubLog]
  var currentKey: Option[Any] = None
  var currentLog: SubLog = logs.getOrElseUpdate(currentKey, new SubLog)

  var nodes: List[Option[Any]] = List(None)

  def pushNode(n: Any): Unit = {
    if (simple_log)
      return
    nodes +:= Some(n)
    if (currentKey.contains(n))
      return
    currentKey = Some(n)
    currentLog = logs.getOrElseUpdate(currentKey, new SubLog)
  }

  def popNode(): Unit = {
    if (simple_log)
      return
    nodes = nodes.tail
    currentKey = nodes.head
    currentLog = logs.getOrElseUpdate(currentKey, new SubLog)
  }

  def log(xs: String*): Unit = {
    if (simple_log)
      return
    currentLog.enqueue(xs: _*)
  }

  def logCommon(xs: String*): Unit = {
    if (simple_log)
      println(xs.mkString(""))
    else
      logs.getOrElseUpdate(None, new SubLog).enqueue(xs: _*)
  }

  def getLogCommon: StringProperty = {
    if (simple_log)
      return null
    logs.getOrElseUpdate(None, new SubLog).property
  }

  def getLog(n: Any): StringProperty = {
    if (simple_log)
      return null
    if (logs contains Some(n)) {
      logs(Some(n)).property
    } else {
      new StringProperty("(empty)")
    }
  }

}
