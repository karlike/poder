package põder

import java.io.{File, IOException}

import com.mxgraph.model.mxCell
import com.mxgraph.swing.mxGraphComponent
import javafx.application.Platform
import org.objectweb.asm.ClassReader
import põder.analyses.Registry
import põder.framework._
import põder.framework.cfg._
import põder.framework.solver.{SelectSolver, Solver}
import põder.log.Log
import põder.util.json.{JVal, Json, JsonF}
import põder.util.typ.{ArrayT, MethodType, ObjectT, Parser, VoidT}
import põder.util.{PerfCount, Project, SMTHelper}
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.beans.property.BooleanProperty
import scalafx.beans.value.ObservableValue
import scalafx.embed.swing.SwingNode
import scalafx.event.ActionEvent
import scalafx.geometry.Orientation.Vertical
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._
import scalafx.scene.input.KeyEvent
import scalafx.scene.layout._
import scalafx.scene.text.{Font, Text}
import scalafx.stage.{DirectoryChooser, Stage, WindowEvent}

import scala.collection.mutable
import scala.io.Source
import scala.reflect.io.{Directory, Path}
import scala.util.matching.Regex
import sys.process._

object P6der {
  var gui = false
  var analysisName = "modular"
  var solverName = "slr"
  var exceptions = false
  var juliet = false
  var libpath: Option[String] = None

  def printUsage(): Unit = {
  }

  def processArgs: List[String] => (String,Option[String],String) = {
    case "--lib"::p::xs =>
      libpath = Some(p)
      processArgs(xs)
    case "--exceptions"::xs =>
      exceptions = true
      processArgs(xs)
    case "--analysis"::x::xs =>
      analysisName = x
      processArgs(xs)
    case "--solver"::x::xs =>
      solverName = x
      processArgs(xs)
    case "--juliet"::xs =>
      juliet = true
      processArgs(xs)
    case "--gui"::xs =>
      gui = true
      processArgs(xs)
    case List(base,cls) =>
      (base,libpath,cls)
    case _ =>
      printUsage()
      sys.exit()
  }

  val pl = new ProcessLogger {
    val files: mutable.Set[String] = mutable.Set.empty
    def deleteFiles(path: Path): Unit = {
      files.foreach(s => (path / Path(s)).delete())
      files.clear()
    }
    private val reg = "\\[wrote (.*)\\]".r
    override def out(s: => String): Unit = ()
    override def err(s: => String): Unit =
      reg.findFirstMatchIn(s).foreach(s => files += s.group(1))

    override def buffer[T](f: => T): T = f
  }

  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      P6derGuiApp.main(Array())
    } else {
      val (b,l,c) = processArgs(args.toList)
      val file = c.replace(".",File.separator)
      val path = Path(b) .toCanonical
      if ((path / Path(file++".java")).exists) {
        val r = Process(s"javac -verbose -g ${file}.java", path.jfile) ! pl
        if (r==0 && (path / Path(file++".class")).exists) {
          val p = new Project(b, l getOrElse b,true)
          p.exceptions = exceptions
          p.analysisName = analysisName
          p.juliet = juliet
          p.source_base = path.toString()
          p.mainClass = c
          if (gui) {
            val app = new JFXApp {
              stage = new JFXApp.PrimaryStage {
                opacity = 0
              }
              val pdr = new P6derGui(p)
              pdr.stage.show()
              pdr.stage.onCloseRequest = handle {
                pl.deleteFiles(path)
              }
              Platform.runLater(() => stage.close())
            }
            app.main(Array())
          } else {
            Log.simple_log = true
            val app = new P6derCmdLine(p, solverName)
            app.main(Array())
            pl.deleteFiles(path)
          }
        }
      }
    }
  }
}

abstract class CFGProgram(
  val project: Project
) extends ProgramCFG
{

  def addCfg(c: String, m: String, t: MethodType): Unit

  val next = mutable.Map.empty[Node, Seq[(TFType[Annot], Node)]]
  val prev = mutable.Map.empty[Node, Seq[(TFType[Annot], Node)]]
  val usedMeths = mutable.Set.empty[(String, String, MethodType)]

  var (basePath, mainPath, mainClass, _) = loadClassFullPath(project.fullMainPath)


  private def loadClass(reader: ClassReader): (String,String,Map[(String, String, MethodType), CFG.Graph[TFType[Annot]]]) = {
    val classPrinter = new ClassCFGPrinter(project.exceptions)
    reader.accept(classPrinter, 0)

    project.superMap.update(classPrinter.className, classPrinter.superName)

    if (mainClass != null && classPrinter.className.endsWith(mainClass) && mainPath != classPrinter.packageName) {
      val pathPat = s"(.*)(${Regex.quote(classPrinter.packageName)})".r
      mainPath match {
        case pathPat(b, p) =>
          mainPath = p
          basePath = basePath ++ b
        case _ =>
          println(s"Warning: Cannot establish base path for class ${classPrinter.className}.")
      }
    }
    for {cfg <- classPrinter.methodGraphs.values
         e <- cfg.edges}
    {
      prev.update(e.to, prev.getOrElse(e.to, Seq()) :+ (e.data, e.from))
      next.update(e.from, next.getOrElse(e.from, Seq()) :+ (e.data, e.to))
    }
    classPrinter.methodGraphs.foreach{ case ((c,m,t),cfg) => project.storeGraph(c,m,t,cfg)}
    (classPrinter.packageName, classPrinter.className, classPrinter.methodGraphs.toMap)
  }


  def loadClassFullPath(fullPath: String): (String,String,String,Map[(String, String, MethodType), CFG.Graph[TFType[Annot]]]) = {
    val file = new java.io.File(fullPath)
    if (file.exists && file.isFile) {
      val fileData = Source.fromFile(file, "ISO8859-1")
      val fileDataBytes = fileData.map(_.toByte).toArray
      val (pn, cn, gs) = loadClass(new ClassReader(fileDataBytes))
      var pns = pn.split(Regex.quote("/")).toSeq
      val cpp = file.getCanonicalPath
      var cp = cpp.split(Regex.quote(File.separator)).toSeq.init
      while (pns.nonEmpty && cp.last == pns.last) {
        cp = cp.init
        pns = pns.init
      }
      (cp.mkString("",File.separator,File.separator), pn, cn, gs)
    } else {
      println(s"Error: Failed to load main class ${fullPath}.")
      sys.exit()
    }
  }

  def loadClassFile(classFile: String): Unit = {
    val fullPath =
      if (java.nio.file.Files.isRegularFile(java.nio.file.Paths.get(basePath++classFile)))
        basePath++classFile else basePath++mainPath++classFile
    val fileData = Source.fromFile(fullPath, "ISO8859-1")
    val fileDataBytes = fileData.map(_.toByte).toArray
    val (pn,cn, gs) = loadClass(new ClassReader(fileDataBytes))
  }

  private val loadedClasses = mutable.Set.empty[String]
  private def loadClassName(className: String): Unit = {
    if (loadedClasses contains className)
      return

    loadedClasses += className

    var cr: ClassReader = null
    try {
      cr = new ClassReader(className)
    } catch {
      case a: IOException =>
//        printf("cannot load resource %s: %s\n",className,a.toString)
        loadClassFile(className++".class")
        return
    }
    loadClass(cr)
  }

  private def setPrevNextMap(cfg: CFG.Graph[TFType[Annot]]): Unit = {
    for {e <- cfg.edges} {
      if (!prev.getOrElse(e.to, Seq()).contains((e.data, e.from)))
        prev.update(e.to, prev.getOrElse(e.to, Seq()) :+ (e.data, e.from))
      if (!next.getOrElse(e.from, Seq()).contains((e.data, e.from)))
        next.update(e.from, next.getOrElse(e.from, Seq()) :+ (e.data, e.to))
    }
  }


  def graphFun(c:String, m:String, t:MethodType): CFG.Graph[TFType[Annot]] = {
    project.loadGraph(c, m, t) match {
      case Some(cfg) =>
        setPrevNextMap(cfg)
        addCfg(c, m, t)
        cfg
      case None =>
        loadClassName(c)
        project.loadGraph(c, m, t) match {
          case Some(cfg) =>
            addCfg(c, m, t)
            cfg
          case None =>
            graphFun(project.superMap(c), m, t)
        }
    }
  }

  def startVarsFun(c:String, m:String, t:MethodType): Node = {
    graphFun(c,m,t).start
  }

  def returnVarsFun(c:String, m:String, t:MethodType): Node = {
    graphFun(c, m, t).retrn
  }

  def getMethodThrowingNodes(c: String, m: String, t: MethodType): List[Node] = {
    graphFun(c, m, t).throwingNodes
  }

  def getMethod(c:String, m:String, t:MethodType): CFG.Graph[TFType[Annot]] = {
    try {
      graphFun(c,m,t)
    }catch {
      case e: NullPointerException =>
        println(s"Cannot load class: $c")
        throw e
      case e:NoSuchElementException =>
        println(s"Cannot find the method: $c.$m")
        throw e
    }
  }
}


abstract class ConstrSys(cfgp: CFGProgram, project: Project, solverName: String) {
  val spec: MapAnalysis[Node] = Registry.getAnalysis(project.analysisName)(cfgp)

  private def varFun(c:String, m:String, t:MethodType) = if (spec.forward) cfgp.returnVarsFun(c,m,t) else cfgp.startVarsFun(c,m,t)
  cfgp.startMethods =
    if (project.juliet)
      Set(("good", MethodType(List(),VoidT)), ("bad",MethodType(List(),VoidT)))
    else
      Set(("main",MethodType(List(ArrayT(ObjectT("java/lang/String"))),VoidT)))

  val sys = {
    if (spec.forward) {
      val ss =  MapAnalysis2Sem(spec, cfgp.startVarsFun, cfgp.returnVarsFun, cfgp.getMethodThrowingNodes, !spec.forward)
      MakeCSys[Node, TFType[Annot]](ss, cfgp.prev.withDefaultValue(Seq())(_))
    } else {
      val ss =  MapAnalysis2Sem(spec, cfgp.returnVarsFun, cfgp.startVarsFun, cfgp.getMethodThrowingNodes, !spec.forward)
      MakeCSys[Node, TFType[Annot]](ss, cfgp.next.withDefaultValue(Seq())(_))
    }
  }

  type node = Node
  type cvar = node
  type cval = spec.dl

  def finished(): Unit
  def update(): Unit
  def storeNode(): Unit
  def saveResults(): Unit

  def previousSol(v:(cvar,spec.key)): Option[(cval,Boolean)] = {
    val d: Option[(cval,Boolean)] = project.loadResult(v._1)
    d
  }

  val solver = new SelectSolver[(node,spec.key), cval](solverName, previousSol, sys, spec.L, update, finished)

  val st = new Thread() {
    override def run():Unit = {
      val startVars = cfgp.startMethods.map{case (m,t) => varFun(cfgp.mainPath++cfgp.mainClass,m,t)}.flatMap{ n =>
        spec.startKeys.map((n,_))
      }
      Log.log(s"Analyzing '${cfgp.mainClass}' using analysis '${project.analysisName}'.")
      PerfCount.sub("verify") { solver.verify_reachable(previousSol, startVars) }
      val result = PerfCount.sub("solver") {solver.solve(startVars)}
      sys.postProcess()
      update()
      PerfCount.sub("verify") { solver.verify(result) }
      //for (((v,k),d) <- result if project.isLibrary(v.methodInfo.cls)) {
      //  project.saveResult(v,(d,v.methodInfo.mthd!="main"))
      //}
    }
  }
}

class P6derCmdLine(project: Project, solver: String)
  extends App
{
  private val cfgp = new CFGProgram(project) {
    override def addCfg(c: String, m: String, t: MethodType): Unit = ()
  }

  private val cs = new ConstrSys(cfgp, project, solver) {
    override def finished(): Unit = ()
    override def update(): Unit = ()
    override def storeNode(): Unit = ()
    override def saveResults(): Unit = ()
  }

  val (bp,mp,mc,cfg) = cfgp.loadClassFullPath(project.fullMainPath)
  cfgp.startMethods.foreach{case (m,t) =>
    cfgp.addCfg(cfgp.mainClass,m,t)
  }

  cs.solver.runButton()
  cs.st.run()
}

class P6derGui(
  val project: Project
)
{
  val nm = mutable.Map.empty[Node, mxCell]
  val nt = mutable.Map.empty[Node, Tab]
  val comps = mutable.Set.empty[mxGraphComponent]

  var current_cs : Option[ConstrSys] = None

  def isStandard(c:String, m:String): Boolean = {
     c.startsWith("java") || c.startsWith("scala")
  }

  val stdMethods = new CheckBox("Std. Mthds")
  def skipStandard: Boolean = !stdMethods.selected.value

  def select(n: CFG.E[TFType[Annot]]): Unit = {
  }
  private var nd: Option[Node] = None

  def select(n: Node): Unit = {
    nd = Some(n)
    current_cs match {
      case Some(cs) =>
        implicit val j: JsonF[JVal] = Json
        val jv = j.Obj(cs.solver.st.withFilter(_._1._1 == n).map{
          case ((_,k),d) => (cs.spec.Key.toJson(k).toText,cs.spec.L.toJson(d))
        })
        val vv = Json.toTreeItems(jv)
        Platform.runLater(() => {
          log.text <== Log.getLog(n)
          val ti = new TreeItem[String]("data")
          vv.foreach(ti.children.append(_))
          tv.root = ti
          tv.showRoot = false
        })
      case None =>
    }
  }

  def solverStart(aName: String, sName: String): Unit = {
    val csc = cs(aName, sName)
    cfgp.loadClassFile(s"${cfgp.mainClass}.class")
    cfgp.startMethods.foreach { case (m, t) =>
      cfgp.addCfg(cfgp.mainPath++cfgp.mainClass, m, t)
    }
    sb.disable = false
    rb.disable = false
    sv.disable = false
    analysisSelector.disable = true
    csc.st.start()
    current_cs = Some(csc)
  }


  lazy private val cfgp = new CFGProgram(project) {
    def addCfg(c:String, m:String, t:MethodType): Unit = {
      if (!(usedMeths contains (c,m,t)) && !(skipStandard && isStandard(c,m))) {
        usedMeths.add((c,m,t))
        val node = new SwingNode()
        val rgr = getMethod(c,m,t)
        val (gr,nm1) = rgr.toMXG(select,select)
        nm ++= nm1
        val comp = new mxGraphComponent(gr)

        comps += comp
        comp.setConnectable(false)
        comp.setDragEnabled(false)
        comp.setAutoScroll(true)
        node.content = comp

        val tab = new Tab()
        tab.text = c ++ "." ++ m
        tab.content = node

        for (n <- rgr.nodes)
          nt += n -> tab

        Platform.runLater(() => {
          graphs.tabs += tab
        })
      }
    }
  }

  def cs(analysisName: String, solverName: String): ConstrSys =
    new ConstrSys(cfgp, project, solverName) {

      private var done = false
      def finished(): Unit = {
        done = true
        Log.logCommon(s"Analysis finished.")
        Platform.runLater(() => {
          pi.progress = 1
          tb.disable = true
          sb.disable = true
          rb.disable = true
          sv.setText("save results")
        })
      }

      def saveResults(): Unit = {
        for (((v,k),d) <- solver.st.toMap if project.isLibrary(v.methodInfo.cls)) {
          project.saveResult(v,(d,v.methodInfo.mthd!="main"))
        }
        if (done) {
          sv.disable = true
        }
      }

      def storeNode(): Unit = {
//        for{node <- nd
//            v <- solver.st.get(node)}{
//          val confidence = done && node.methodInfo.mthd != "main" // high confidence if not main method
//          project.saveResult(node,(v,confidence))
//        }
      }

      var stepOverStd = false
      def update(): Unit = {
        if (skipStandard && solver.currentNode.exists(x => isStandard(x._1.methodInfo.cls,x._1.methodInfo.mthd))) {
          if (!stepOverStd) {
            Log.logCommon(s"Stepping over std. method call.")
            stepOverStd = true
          }
          solver.stepButton()
        } else {
          stepOverStd = false
          Log.logCommon(s"Analysis paused.")
          Platform.runLater(() => {
            pi.progress = 1
            tb.disable = true
          })

          def setColor(v: Option[cvar], cs: (String, String)*): Unit = {
            v match {
              case Some(n) if nm contains n =>
                val v = nm(n)
                v.setStyle(cfg.CFG.changeStyle(v.getStyle, cs: _*))
              case _ => ()
            }
          }

          if (comps != null) {
            setColor(oldNode, "fillColor" -> "#000000", "fontColor" -> "white")
            oldUpd.foreach(x => setColor(Some(x), "fillColor" -> "#000000", "fontColor" -> "white"))
            oldUpd.clear()
            solver.updatedSet.foreach(x => setColor(Some(x._1), "fillColor" -> "#0000BB", "fontColor" -> "white"))
            solver.widenedSet.foreach(x => setColor(Some(x._1), "fillColor" -> "#BB0000", "fontColor" -> "white"))

            oldUpd ++= solver.updatedSet.map(_._1)
            oldUpd ++= solver.widenedSet.map(_._1)

            if (solver.currentNode.isDefined && (solver.widenedSet contains solver.currentNode.get))
              setColor(solver.currentNode.map(_._1), "fillColor" -> "#FF0000", "fontColor" -> "white")
            else
              setColor(solver.currentNode.map(_._1), "fillColor" -> "#00AA00", "fontColor" -> "white")
            oldNode = solver.currentNode.map(_._1)
            solver.clearSets()

            solver.currentNode.foreach(x => nt.get(x._1).foreach(graphs.getSelectionModel.select(_)))

            comps.foreach(_.refresh())
          }
        }
      }

      tv.root = new TreeItem("<tyhi>")

      var oldNode: Option[cvar] = None
      var oldUpd = mutable.Set.empty[cvar]
    }


  val graphs = new TabPane()
  val tv = new TreeView[String]()
  val log = new TextArea()
  val info = new TabPane()
  val commonlog = new TextArea {
    text <== Log.getLogCommon
    def of: (ObservableValue[String, String], String, String) => Unit = (_,_,l) => {
      Platform.runLater( () =>
        this.delegate.positionCaret(l.length)
      )
    }
    text.onChange[String](of)
  }

  val result = new Tab()
  result.text = "result"
  result.content = tv
  val logTab = new Tab()
  logTab.text = "log"
  logTab.content = log
  info.tabs += result
  info.tabs += logTab

  val sb = new Button("step")
  val rb = new Button("run")
  val tb = new Button("stop")
  val sv = new Button("save progress")
  val analysisSelector: GridPane = new GridPane() {
    val solverChoice = new ChoiceBox[String]()
    val startButton = new Button("Start!")

    solverChoice.getItems.addAll(SelectSolver.allSol.keys.toSeq :_*)
    solverChoice.selectionModel.value.clearAndSelect(0)
    startButton.onAction = { _: ActionEvent =>
      Log.logCommon(s"Start button pushed.")
      solverStart(project.analysisName,
        solverChoice.selectionModel.value.getSelectedItem)
    }
    margin = Insets(4)
    hgap = 10
    vgap = 10
    val atext = new Text("Analysis:"){
      opacity <== when(disabled) choose 0.4 otherwise 1
    }
    val stext = new Text("Solver:") {
      opacity <== when(disabled) choose 0.4 otherwise 1
    }

    this.add(stext,1,0)
    this.add(solverChoice,2,0)
    this.add(stdMethods,3,0)
    this.add(startButton,4,0)
  }

  val pi = new ProgressIndicator{
    maxHeight = 20
    maxWidth = 20
    progress = 1
  }

  val analysisControl: GridPane = new GridPane() {
    sb.prefWidth = 75
    rb.prefWidth = 75
    tb.prefWidth = 75
    sv.prefWidth = 125
    tb.disable = true
    sb.disable = true
    rb.disable = true
    sv.disable = true
    sb.onAction = { _: ActionEvent =>
      Log.logCommon(s"Step button pushed.")
      SMTHelper.monitor = Some(() => ())
      pi.progress = -1
      tb.disable = false
      current_cs.foreach(_.solver.stepButton())
    }
    rb.onAction = { _: ActionEvent =>
      Log.logCommon(s"Run button pushed.")
      SMTHelper.monitor = Some { () => () }
      pi.progress = -1
      tb.disable = false
      current_cs.foreach(_.solver.runButton())
    }
    tb.onAction = { _: ActionEvent =>
      Log.logCommon(s"Stop button pushed.")
      SMTHelper.monitor = Some(() => throw new BreakTransferFunction)
      current_cs.foreach(_.solver.stopButton())
      tb.disable = true
    }
    sv.onAction = { _: ActionEvent =>
      Log.logCommon(s"Save button pushed.")
      SMTHelper.monitor = Some(() => ())
      current_cs.foreach(_.saveResults())
    }
    padding = Insets(4)
    hgap <== 4
    this.addRow(0, sb, rb, pi, tb, sv)
  }

  private val infos = new SplitPane {
    dividerPositions = 0.8
    orientation = Vertical
    items.add(info)
    items.add(commonlog)
  }


  val stage : Stage = new Stage {
    title = "P6der"
    scene = new Scene {
      root = new SplitPane {
        private val grapControls = new BorderPane() {
          this.center = graphs
          this.bottom = new TilePane() {
            private var currentZoom = 100
            val zm = new Spinner[Integer](5, 400, currentZoom, 5)
            zm.prefWidth = 85
            zm.styleClass += Spinner.StyleClassSplitArrowsHorizontal
            zm.editable = true
            zm.value.onChange {
              val z = zm.value.value
              comps foreach {
                _.zoom((z / 100.0) / (currentZoom / 100.0))
              }
              currentZoom = z
            }
            val store = new Button("store")
            store.onAction = handle {
              current_cs.foreach(_.storeNode())
            }
            hgap <== 10
            children += new Text("Zoom:")
            children += zm
            children += store
          }
        }
        val aControls: VBox = new VBox {
          children += analysisSelector
          children += new Separator()
          children += analysisControl
          children += infos
        }
        items.add(grapControls)
        items.add(aControls)
      }
      onKeyPressed = { e:KeyEvent =>
        if (e.controlDown && e.code.name.toLowerCase == "l")
          Log.logCommon(PerfCount.status)
        else if (e.controlDown && e.code.name.toLowerCase == "k")
          Log.logCommon(PerfCount.statusFlat)
        else if (e.controlDown && e.code.name.toLowerCase == "p")
          Log.logCommon(PerfCount.statusFlat)
      }
    }
    onCloseRequest = { e:WindowEvent =>
      Log.logCommon(s"Closing ...")
      current_cs.foreach(_.solver.killButton())
      current_cs.foreach(_.solver.stopButton())
      SMTHelper.monitor = Some(() => throw new BreakTransferFunction)
      System.exit(0)
    }
  }
}
class DirChooserField(stage: Stage) extends HBox {
  private val self = this
  val button = new Button("...") {
    onAction = handle {
      chooseDir(stage)
    }
  }
  val field: TextField = new TextField {
    prefWidth  <== self.prefWidth-button.width
    prefHeight <== self.prefHeight
  }
  val directoryChooser: DirectoryChooser = new DirectoryChooser(){
    title = "Vali lähtekoodi kataloog ..."
  }
  private def chooseDir(stage:Stage):Unit = {
    var folder = new File(field.text.value)
    if (!folder.exists()) folder = new File(".")
    directoryChooser.initialDirectory = folder
    val f = directoryChooser.showDialog(stage)
    if (f != null && f.isDirectory) {
      field.text = f.getCanonicalPath
      field.fireEvent(new ActionEvent)
    }
  }
  children = Seq(field, button)
  field.disable <==> button.disable
}

object CreateProject {
  def createProject(src: String, cls: String, ana:String, ex:Boolean, jul:Boolean, prj:String, libp:String): Project = {
    Path(libp).createDirectory()
    val p = new Project(prj,libp)
    p.analysisName = ana
    p.exceptions   = !ex
    p.mainClass    = cls
    p.juliet       = jul
    p.source_base  = src
    p.createOnDisk()
    p
  }
  def findAllClasses(path:String):Seq[String] = {
    val pl = (if (path.last==File.separatorChar) -1 else 0) + path.length
    def pathToPackage(s:String): String = {
      s.substring(pl+1,s.length-6).replace(File.separator,".")
    }
    val f = new File(path)
    def r(d:Int)(f:File):Seq[String] = {
      if (d == 7)
        Seq()
      else if (f.isFile && f.toString.split("\\.").last == "class")
        Seq(pathToPackage(f.toString))
      else if (f.isDirectory)
        f.listFiles().flatMap(r(d+1))
      else
        Seq()
    }
    r(0)(f)
  }
  val stage: Stage = new Stage {
    val stage: Stage = this
    title = "Uus Projekt"
    scene = new Scene {
      val fH = 20
      val fW = 900
      val lblW = 150
      private val projLbl = new Label("Kasutaja koodi projekt:") {
        minWidth = lblW
      }
      private val projPath = new DirChooserField(stage) {
        var overridden = false
        val unavaliable : BooleanProperty = BooleanProperty(true)
        prefWidth = fW
        prefHeight = fH
        field.text.onChange((_,_,newv) => {
          val p = Path(newv)
          unavaliable.set(p.exists || !p.parent.exists)
        })
        field.onAction = handle {
          overridden = true
        }
      }
      private val libPath = new DirChooserField(stage) {
        var overridden = false
        val unavaliable : BooleanProperty = BooleanProperty(true)
        prefWidth = fW
        prefHeight = fH
        field.text.onChange((_,_,newv) => {
          val p = Path(newv)
          unavaliable.set(!p.exists && !p.parent.exists)
        })
        field.onAction = handle {
          overridden = true
        }
      }
      private val libChk = new CheckBox("Eraldi standardteek:"){
        minWidth = lblW
        selected.onChange((_,_,v) =>
          if(v) {
            libPath.field.disable = false
            libPath.field.text.unbind()
          } else {
            libPath.field.disable = true
            libPath.field.text <== projPath.field.text
          }
        )
        libPath.field.disable = true
        libPath.field.text <== projPath.field.text
      }
      private val mainLbl = new Label("Peaklass:") {
        minWidth = lblW
      }
      private val mainCls = new ComboBox[String] {
        prefWidth = fW
        prefHeight = fH
        onAction = handle {
          if (!projPath.overridden) {
            val base = Path(sourcePath.field.text.value)
            if (base != null && !selectionModel.value.isEmpty) {
              val path = base./(Directory("analysis_" ++ selectionModel.value.getSelectedItem)).toString
              projPath.field.text = path
            }
          }
        }
      }
      private val sourceLbl = new Label("Lähtekataloog:") {
        minWidth = lblW
      }
      private val sourcePath : DirChooserField = new DirChooserField(stage) {
        prefWidth = fW
        prefHeight = fH
        private var last = 0
        field.onAction = handle {
          mainCls.getItems.clear()
          mainCls.selectionModel.value.clearSelection()
          new Thread {
            override def run(): Unit = {
              last += 1;
              val l = last
              val cs = findAllClasses(field.text.value)
              if (last == l)
                Platform.runLater { () =>
                  mainCls.getItems.addAll(cs: _*)
                }
            }
          }.start()
        }
        field.text = new File(".").getCanonicalPath
        field.fireEvent(new ActionEvent)
      }
      private val anaLbl = new Label("Analüüs:"){
        minWidth = lblW
      }
      private val anaFld = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
        items.get().addAll(Registry.getNames:_*)
        selectionModel.value.clearAndSelect(0)
      }
      private val ignEx = new Label("Ilma erinditeta:"){
        minWidth = lblW
      }
      private val ignExBx = new CheckBox {
        prefWidth = fH+3
        prefHeight = fH+3
      }
      private val jul = new Label("Juliet:"){
        minWidth = lblW
      }
      private val julBx = new CheckBox {
        prefWidth = fH+3
        prefHeight = fH+3
      }
      private val cncl = new Button("Katkesta") {
        onAction = handle {
          stage.hide()
          StartPage.stage.show()
        }
        margin = Insets(25,25,10,25)
      }
      private val crt = new Button("Loo projekt") {
        disable <== projPath.unavaliable && libPath.unavaliable && mainCls.selectionModel.value.selectedIndexProperty() < 0
        onAction = handle {
          val p = createProject(
            sourcePath.field.text.value,
            mainCls.selectionModel.value.getSelectedItem,
            anaFld.selectionModel.value.getSelectedItem,
            ignExBx.selected.value,
            julBx.selected.value,
            projPath.field.text.value,
            libPath.field.text.value)
          stage.hide()
          val w = new P6derGui(p)
          w.stage.show()
        }
        margin = Insets(25,25,10,0)
      }

      root = new BorderPane {
          top   =  new BorderPane { prefHeight = 10 }
          left  =  new BorderPane { prefWidth = 25 }
          right =  new BorderPane { prefWidth = 25 }
          center = new VBox {
            children = Seq(
              new HBox {alignment = Pos.CenterLeft; children = Seq(sourceLbl, sourcePath)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(mainLbl  , mainCls)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(anaLbl   , anaFld)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(ignEx    , ignExBx)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(jul      , julBx)},
              new BorderPane {prefHeight = 15},
              new HBox {alignment = Pos.CenterLeft; children = Seq(projLbl  , projPath)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(libChk   , libPath)}
            )
          }
        bottom = new HBox {
          alignment = Pos.CenterRight
          children = Seq(cncl,crt)
        }
      }
    }

    width = 500
    minWidth = 210

    height = 300
    minHeight = 220
  }
}

object OpenProject {
  var pField: TextField = new TextField()

  def getWorkingVersions(directoryName: String): (Seq[String], mutable.Map[String, Seq[String]]) = {
    var workingVrs = Seq.empty[String]
    val verToAn = mutable.Map.empty[String, Seq[String]]

    val vrs = getListOfSubDirectories(directoryName, "poder_v")
    for  {vr <- vrs} {
      val ans = getListOfSubDirectories(directoryName ++ File.separator ++ vr, "", Registry.getNames)
      var workingAns = Seq.empty[String]
      for  {an <- ans} {
        Project(Path(directoryName ++ File.separator ++ vr ++ File.separator ++ an)) match {
          case Some(proj) =>
            workingAns = workingAns :+ an
        }
      }
      if (workingAns.nonEmpty) {
        verToAn(vr) = workingAns
        workingVrs = workingVrs :+ vr
      }
    }
    (workingVrs, verToAn)
  }
  def getListOfSubDirectories(directoryName: String, startsWith: String = "", inList: Seq[String] = Seq.empty): Seq[String] = {
    if (new File(directoryName).exists())
      new File(directoryName)
        .listFiles
        .filter(_.isDirectory)
        .filter(_.getName.startsWith(startsWith) || startsWith == "")
        .filter(fn => inList.contains(fn.getName) || inList.isEmpty)
        .map(_.getName)
    else
      Seq.empty
  }
  def getCurrentVersionIndx(versionList: Seq[String]): Int = {
    val dummy_prj = new Project("", "")
    versionList.indexOf("poder_v" ++ dummy_prj.poder_ver ++ "_" ++ dummy_prj.java_ver)
  }
  def getPath(str: String): Path = {
    Path(str)
  }
  def updateProjPath(projPath: String): Unit = {
    pField.text.value = projPath
    pField.fireEvent(new ActionEvent)
  }

  val stage: Stage = new Stage {
    val stage: Stage = this
    var workingVrs: Seq[String] = Seq.empty
    var verToAn: mutable.Map[String, Seq[String]] = mutable.Map.empty[String, Seq[String]]

    title = "Ava Projekt"
    scene = new Scene {

      val fH = 20
      val fW = 900
      val lblW = 150
      private val anaLbl = new Label("Analüüs:"){
        minWidth = lblW
      }
      private val anaFld: ChoiceBox[String] = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
      }
      private val verLbl = new Label("Versioon:"){
        minWidth = lblW
      }
      private val verFld: ChoiceBox[String] = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
        onAction = handle {
          anaFld.getItems.clear()
          anaFld.getItems.addAll(verToAn(selectionModel.value.getSelectedItem): _*)
          anaFld.selectionModel.value.clearAndSelect(0)
        }
      }
      private val projLbl = new Label("Kasutaja koodi projekt:") {
        minWidth = lblW
      }
      private val projPath: DirChooserField = new DirChooserField(stage) {
        pField = field
        prefWidth = fW
        prefHeight = fH
        field.onAction = handle {
          verFld.getItems.clear()
          val vrs = getWorkingVersions(field.text.value)
          workingVrs = vrs._1
          verToAn = vrs._2
          val current_vr_indx = getCurrentVersionIndx(workingVrs)
          if (current_vr_indx >= 0) {
            workingVrs = Seq(workingVrs(current_vr_indx))
          } else if (workingVrs.length <= 0) {
            new Alert(AlertType.Information) {
              initOwner(OpenProject.stage)
              title = "Probleem"
              headerText = "Projekti laadimine ebaõnnestus"
              contentText = "Etteantud asukohast polnud võimalik leida 'conf' faili."
            }.showAndWait()
          } else {
            new Alert(AlertType.Information) {
              initOwner(OpenProject.stage)
              title = "Probleem"
              headerText = "Hoiatus! Projektis polnud õiges versioonis analüüsi."
              contentText = "Võid valida teise versiooni, mis ei pruugi töötada"
            }.showAndWait()
          }
          verFld.getItems.addAll(workingVrs: _*)
          verFld.selectionModel.value.clearAndSelect(0)
        }
      }
      private val cncl = new Button("Katkesta") {
        onAction = handle {
          stage.hide()
          StartPage.stage.show()
        }
        margin = Insets(25,25,10,25)
      }
      private val crt = new Button("Ava projekt") {
        disable <== verFld.selectionModel.value.selectedIndexProperty() < 0 && anaFld.selectionModel.value.selectedIndexProperty() < 0
        onAction = handle {
          val path = Path(projPath.field.text.value ++
                          File.separator ++ verFld.selectionModel.value.getSelectedItem ++
                          File.separator ++ anaFld.selectionModel.value.getSelectedItem)
          Project(path) match {
            case Some(proj) =>
              val w = new P6derGui(proj)
              stage.hide()
              w.stage.show()
            case None =>
              new Alert(AlertType.Information) {
                initOwner(stage)
                title = "Probleem"
                headerText = "Projekti laadimine ebaõnnestus."
                contentText = "Etteantud asukohast polnud võimalik laadida 'conf' faili."
              }.showAndWait()
          }
        }
        margin = Insets(25,25,10,0)
      }
      root = new BorderPane {
        top   =  new BorderPane { prefHeight = 10 }
        left  =  new BorderPane { prefWidth = 25 }
        right =  new BorderPane { prefWidth = 25 }
        center = new VBox {
          children = Seq(
            new HBox {alignment = Pos.CenterLeft; children = Seq(projLbl  , projPath)},
            new BorderPane {prefHeight = 15},
            new HBox {alignment = Pos.CenterLeft; children = Seq(verLbl  , verFld)},
            new HBox {alignment = Pos.CenterLeft; children = Seq(anaLbl   , anaFld)},
          )
        }
        bottom = new HBox {
          alignment = Pos.CenterRight
          children = Seq(cncl,crt)
        }
      }
    }
    width = 500
    height = 200
  }
}

object StartPage {
  def handleOpenProject(fPath: String): Any = {
    val vrs = OpenProject.getWorkingVersions(fPath)
    val workingVrs = vrs._1
    val verToAn = vrs._2
    val currentVrIndx = OpenProject.getCurrentVersionIndx(workingVrs)
    if (currentVrIndx >= 0 && verToAn(workingVrs(currentVrIndx)).length == 1) {
      val currentVr = workingVrs(currentVrIndx)
      val currentVrAn = verToAn(workingVrs(currentVrIndx)).head
      Project(OpenProject.getPath(fPath ++ File.separator ++ currentVr ++ File.separator ++ currentVrAn)) match {
        case Some(proj) =>
          val w = new P6derGui(proj)
          stage.hide()
          w.stage.show()
      }
    } else if (workingVrs.length <= 0) {
      new Alert(AlertType.Information) {
        initOwner(stage)
        title = "Probleem"
        headerText = "Projekti laadimine ebaõnnestus"
        contentText = "Etteantud asukohast polnud võimalik laadida 'conf' faili."
      }.showAndWait()
    } else {
      stage.hide()
      OpenProject.updateProjPath(fPath)
      OpenProject.stage.show()
    }
  }
  val stage: PrimaryStage = new PrimaryStage {
    private val stage = this
    title = "Põder"
    scene = new Scene {
      val newProj: Button = new Button("Uus projekt"){
        prefWidth = 200
        prefHeight = 60
        onAction = handle {
          stage.hide()
          CreateProject.stage.show()
        }
      }
      val directoryChooser: DirectoryChooser = new DirectoryChooser(){
        title = "Vali projekti kataloog ..."
      }
      val opnProj = new Button("Ava projekt"){
        prefWidth = 200
        prefHeight = 60
        onAction = handle {
          val f = directoryChooser.showDialog(stage)
          if (f != null && f.isDirectory) {
            Project(Path(f)) match {
              case Some(proj) =>
                val w = new P6derGui(proj)
                stage.hide()
                w.stage.show()
              case None =>
                handleOpenProject(f.getPath)
            }
          }
        }
      }
      val close   = new Button("Sulge"){
        prefWidth = 200
        prefHeight = 60
        onAction = handle {
          stage.close()
        }
      }
      val lbl = new HBox {
        alignment = Pos.BottomCenter
        val pdr = new Text("\uD83E\uDD8C") {
          font =  new Font(40)
        }
        children = Seq(new Label("©Tartu Ülikool  "), pdr)
      }
      root = new BorderPane {
        center = new HBox {
          alignment = Pos.Center
          children = new VBox {
            children = Seq(newProj, opnProj, close)
          }
        }
        bottom = lbl
      }
    }

    width = 250
    height = 300
  }
}

object P6derGuiApp extends JFXApp {
  stage = StartPage.stage
}