package põder.analyses

import põder.framework._
import põder.framework.cfg._
import põder.util.Project
import põder.util.json.JsonF
import põder.util.typ.MethodType

case class Method(val cls: String, val mthd: String)

object MSet extends TopSet[Method] {
  def TtoJson[jv](x: Method)(implicit j: JsonF[jv]): jv =
    j.Str(x.cls ++ "." ++ x.mthd)
}

object ESet extends TopSet[String] {
  def TtoJson[jv](x: String)(implicit j: JsonF[jv]): jv =
    j.Str(x)
}

object SPrd extends Product(MSet ,ESet)
  with WidenOpF[(Option[Set[Method]], Option[Set[String]])]
  with NarrowOpF[(Option[Set[Method]], Option[Set[String]])]
{
  override def widen(oldv: (Option[Set[Method]], Option[Set[String]]), newv: (Option[Set[Method]], Option[Set[String]])): (Option[Set[Method]], Option[Set[String]]) =
    join(oldv, newv)

  override def toJson[jv](x: (Option[Set[Method]], Option[Set[String]]))(implicit j: JsonF[jv]): jv = {
    import j._
    Obj("methods" -> MSet.toJson(x._1), "exceptions" -> ESet.toJson(x._2))
  }
}

object DepD extends BotLift(SPrd)

class Dep(project: ProgramCFG)  extends SimpleCfgBcSem[Node]{
  override type dl = DepD.t
  override implicit val L: Lattice[DepD.t] with WidenOpF[DepD.t] with NarrowOpF[DepD.t] = DepD

  val empty:dl = Some((Some(Set()),Some(Set())))

  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = {
    if (nd.is_start) empty else L.bot
  }

  override def isBuiltinFunction(instr: MethodInstr, typ:MethodType, static:Boolean): Boolean = {
    val (cls, mthd, _) = instr.a.x.get
    cls == "P6der" && mthd == "check"
  }

  // algseisund
  override def sStart(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = st

  // hargnemise üleminekufunktsioon
  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: JumpInstr, thn: Boolean): dl = st

  def printDep(node:Node, d:dl): Unit = {
    if (d.isDefined && d.get._1.isDefined) {
      printf("%s: %s\n", node.methodInfo.cls ++ "." ++ node.methodInfo.mthd, d.get._1.get.map(x=>x.cls++"."++x.mthd).mkString(", "))
    }
  }

  // "tavaline" üleminekufunktsioon
  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: Instr): dl = instr match {
    case RETURN  => if(inVerification) printDep(to,d) ; d
    case xRETURN(_) => printDep(to,d) ; d
    case i => d
  }

  // erindi üleminekufunktsioon
  override def sThrows(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl = ex match {
    case Left(exception) =>
      st match {
        case Some((methods, exceptions)) => {
          Some((methods, ESet.join(exceptions, Some(Set(exception)))))
        }
        case _ => st
      }

    case Right(exceptionSet) => st
  }

  // annotatsiooni üleminekufunktsioon
  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = st

  // need on meetodiväljakutsete jaoks
  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] =
    Seq(empty)

  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = caller match {
    case None => None
    case Some((d1m, d1t)) => st match {
      case None => None
      case Some((dm, dt)) =>
        val mn = instr match {
          case INVOKEVIRTUAL(a) => a.x.get
          case INVOKESPECIAL(a) => a.x.get
          case INVOKESTATIC(a) => a.x.get
          case INVOKEINTERFACE(a) => a.x.get
          case INVOKEDYNAMIC(a,_) => a.x.get
        }
        val mmn = Method(mn._1, mn._2)
        Some(MSet.join(MSet.join(d1m, dm),Some(Set(mmn))), ESet.join(d1t, dt))
    }
  }
}
