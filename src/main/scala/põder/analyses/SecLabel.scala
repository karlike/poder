package põder.analyses

import põder.dom.LogicHelper._
import põder.dom.{BinIntProp, IProp, LVar, LogLat, Logic, LogicHelper, SProp, SVar, Val}
import põder.framework.{Lattice, NarrowOpF, SimpleCfgBcSem, WidenOpF}
import põder.framework.cfg.{Annot, ArithInstr, ArrayInstr, CastInstr, ConstInstr, DEPRECATED, FieldInstr, GETSTATIC, IF_ACMPx, IF_ICMPx, IFx, INVOKEDYNAMIC, INVOKESTATIC, Instr, JumpInstr, MethodInstr, Node, ObjInstr, ProgramCFG, RetInstr, StackInstr, UNUSED, xLOAD, xSTORE}
import põder.log.Log
import põder.util.typ.{MethodType, Parser}

class SecLabel(project: ProgramCFG)  extends SimpleCfgBcSem[Node]{
  override type dl = Logic
  override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = LogLat

  private val builtins = Map("P6der" -> List("getDirtyInput", "sanitize", "runQuery", "check","grantPermission","revokePermission"))
  override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean = {
    val (pkg,mthd,_) = instr.a.x.get
    builtins.contains(pkg) && builtins(pkg).contains(mthd)
  }

  def local(n:Node, start:Boolean = false)(i:Int): Val = {
    val vl =  LVar(i, start)
    if (n.varNames contains i)
      vl.alias = n.varNames(i)
    vl
  }

  private val dirty : IProp = IProp("dirty")

  override def sNode(get: Node => Logic, set: Node => Logic => Unit)(nd: Node): Logic = {
    if (nd.is_start) L.top else L.bot
  }

  override def sStart(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(locals: Int, stack: Int, access: Int): Logic = st
  override def senter(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[Logic] = Seq(st)
  override def scombine(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: Logic): Logic = st

  override def sBranch(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(instr: JumpInstr, thn: Boolean): Logic = {
    instr match {
      case IFx(x, a) => st.popStack()
      case IF_ICMPx(x, a) => st.popStack(2)
      case IF_ACMPx(x, a) => st.popStack(2)
      case _ => st
    }
  }

  override def sNormal(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(instr: Instr): Logic = {
    instr match {
      case xSTORE(_, n) =>
        val v = local(to)(n.x.get)
        st.eqForget(v).popStack(1,Seq(v())).simplify
      case xLOAD(_, n) =>
        val v = local(to)(n.x.get)
        if (st.query(dirty(v)) contains false)
          st.pushStack
        else
          st.pushStack && dirty(SVar(0))
      case GETSTATIC(_) =>
        st.pushStack
      case INVOKESTATIC(a) if a.x.get._1=="P6der" && a.x.get._2=="getDirtyInput" =>
        st.pushStack && dirty(SVar(0))
      case INVOKESTATIC(a) if a.x.get._1=="P6der" && a.x.get._2=="sanitize" =>
        st.popStack().pushStack
      case INVOKESTATIC(a) if a.x.get._1=="P6der" && a.x.get._2=="runQuery" =>
        if (st.query(dirty(SVar(0))) contains true)
          Log.logCommon("Doom!")
        else
          Log.logCommon("All is well!")
        st.popStack()
      case INVOKEDYNAMIC(a,b) if b.x.get._2=="makeConcatWithConstants" =>
        val typ = Parser.mpar(a.x.get._3)
        if (typ.args.indices.exists(i => st.query(dirty(SVar(i))) contains true))
          st.popStack(typ.args.length).pushStack && dirty(SVar(0))
        else
          st.popStack(typ.args.length).pushStack
      case _ =>
        st
    }
  }

  override def sThrows(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(instr: Instr, ex: Either[String, Set[String]]): Logic = st

  override def sAnnot(get: Node => Logic, set: Node => Logic => Unit, to: Node, to_final: Boolean, st: Logic)(ant: Annot): Logic = st


}
