package põder.analyses

import põder.dom._
import põder.dom.Logic._
import põder.framework.cfg._
import põder.framework._
import põder.util.Project
import põder.util.typ.{MethodType, VoidT}
import smtlib.trees.Terms
import smtlib.trees.Terms.Term




class WP(project: ProgramCFG) extends SimpleCfgBcSem[Node](false) {
  val D: LogLat.type = LogLat
  type node = Node

  override type dl = D.t
  override implicit val L: Lattice[D.t] with WidenOpF[D.t] with NarrowOpF[D.t] = D

  def simplify(d:dl): dl = {
    def not_op(s:String): String = s match {
      case "!=" => "="
      case "=" => "!="
      case ">=" => "<"
      case "<=" => ">"
      case "<" => ">="
      case ">" => "<="
    }
    def is_top(x:dl): Boolean = D.leq(D.top, x)
    def is_bot(x:dl): Boolean = D.leq(x, D.bot)
    if (is_top(d)) D.top else
    if (is_bot(d)) D.bot else {
      d match {
        case And(xs@_*) =>
          val ys = xs.map(simplify).filterNot(is_top)
          if (ys.isEmpty) D.top else And(ys:_*)
        case Or(xs@_*) =>
          val ys = xs.map(simplify).filterNot(is_bot)
          if (ys.isEmpty) D.bot else Or(ys:_*)
        case Not(App(Fun(op,t), x,y)) => App(Fun(not_op(op),t), x, y)
        case Not(x) => Not(simplify(x))
        case ForallL(t, x) => ForallL(t,simplify(x))
        case ExistsL(t, x) => ExistsL(t,simplify(x))
        case _ => d
      }
    }
  }

  override def isBuiltinFunction(i: MethodInstr, t:MethodType, b:Boolean): Boolean = {
    val (klass, mthd, _) = i.a.x.get
    klass == "P6der" && mthd == "check"
  }

  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = nd match {
    case n if n.is_return => L.top
    case _ => D.bot
  }

  // hargnemise üleminekufunktsioon
  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: JumpInstr, b: Boolean): dl = instr match {
    case IF_ICMPx(x, _) =>
      val a = x match {
        case EQ => app(BinIntProp("="), SVar(1), SVar(0))
        case NE => app(BinIntProp("!="), SVar(1), SVar(0))
        case LT => app(BinIntProp("<"), SVar(1), SVar(0))
        case GE => app(BinIntProp(">="), SVar(1), SVar(0))
        case GT => app(BinIntProp(">"), SVar(1), SVar(0))
        case LE => app(BinIntProp("<="), SVar(1), SVar(0))
      }

      val aa = if (b) {
        a
      } else {
        not(a)
      }

      simplify(and(
        aa,
        vS(vS(st))
      ))

    case _ => st
  }


  // tavaline "üleminekurelatsioon", põhitegevuse saadab aNormal_2-e
  // vajadusel laseb invariant funktsioonil invariantidega tegelda
  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr): dl = {
    val a = to.annot
    val dprim = sNormal_2(instr)(st)

    a match {
      case List(s: String) if to_final => invariant(s, dprim, to.varNames)
      case _ => dprim
    }
  }

  // tsükliinvariandid
  def invariant(s: String, dPrim: dl, varNames : Map[Int, String]): dl = {
    val term = parseTerm(s)
    val d = term2avaldis(term, varNames)

    if (!D.leq(dPrim, d)) {
      println("Hoiatus: Ei suuda tõestada invariandi kehtimist.")
    }

    d
  }

  // "tavaline" üleminekufunktsioon
  val sNormal_2: PartialFunction[Instr, dl => dl] = {
    case xLOAD(_, i) => d: dl => {
      vS(repl(Map[Val, dl](SVar(0) -> app(LVar(i.x.get))))(d))
    }
    case xSTORE(_, i) => d => {
      repl(Map[Val, dl](LVar(i.x.get) -> app(SVar(0))))(sS(d))
    }

    case INVOKESTATIC(a) if a.x.get._1 == "P6der" && a.x.get._2 == "check" => d => {
      and(app(BinIntProp("="), SVar(0), IVal(1)), sS(d))
    }

    case xRETURN(_) => d => {
      or(app(BinIntProp("="), RetVar, SVar(0)), d)
    }

    case RETURN => d => {
      d
    }

    case i: ICONST => d => {
      simplify(vS(repl(Map[Val, dl](SVar(0) -> app(IVal(i.n))))(d)))
    }

    case BIPUSH(n) => d => {
      simplify(vS(repl(Map[Val, dl](SVar(0) -> app(IVal(n.x.get))))(d)))
    }
    case SIPUSH(n) => d => {
      simplify(vS(repl(Map[Val, dl](SVar(0) -> app(IVal(n.x.get))))(d)))
    }
    case LDC(s) => d => {
      vS(repl(Map[Val, dl](SVar(0) -> app(IVal(3))))(d))
    }

    case IINC(la) => d => {
      val l = la.x.get._1
      val a = la.x.get._2
      repl(Map[Val, dl](LVar(l) -> app(BinIntOp("-"), LVar(l), IVal(a))))(d)
    }

    // aritmeetika
    case xADD(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("+"), SVar(1), SVar(0))))(sS(d))
    }
    case xSUB(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("-"), SVar(1), SVar(0))))(sS(d))
    }
    case xMUL(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("*"), SVar(1), SVar(0))))(sS(d))
    }
    case xDIV(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("/"), SVar(1), SVar(0))))(sS(d))
    }
    case xREM(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("%"), SVar(1), SVar(0))))(sS(d))
    }
    case xNEG(x) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("-"), SVar(0))))(sS(d))
    }


    // loogika
    case xSHL(_) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("<<"), SVar(1))))(sS(d))
    }
    case xSHR(_) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp(">>"), SVar(1))))(sS(d))
    }
    case xUSHR(_) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("U"), SVar(1), SVar(0))))(sS(d))
    }
    case xAND(_) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("&"), SVar(1), SVar(0))))(sS(d))
    }
    case xOR(_) => d => {
      repl(Map[Val, dl](SVar(1) -> app(BinIntOp("|"), SVar(1), SVar(0))))(sS(d))
    }

    case i => d => {
      print("tundmatu: ")
      println(i)
      d
    }
  }

  // annotatsiooni ülemunekufunktsioon
  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = st


  // meetodiväljakutsesse sisenemine "altpoolt"
  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = {
      Seq(if (getAtoms(st).contains(SVar(0))) {
        False
      } else {
        True
      })
  }

  // meetodisisu ja väliste reeglite ühendamine
  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: MethodInstr, t: MethodType, static: Boolean, vana_d: dl): dl = {
    val argumendiVahetused: Map[Val, dl] = Stream.from(0).take(t.args.length).map(LVar(_)).zip(Stream.from(0).map(a => app(SVar(a)))).toMap
    val vahetatud_args = repl(argumendiVahetused)(d)

    val vana_d_suurendatud_argumentideArv = argumendiVahetused.foldLeft(vana_d)((reeglid, _) => sS(reeglid))
    val vana_d_suurendatud_argumentideArv_ja_vahendatud_ret = if (t.ret == VoidT) {
      vana_d_suurendatud_argumentideArv
    } else {
      vS(vana_d_suurendatud_argumentideArv)
    }

    abiCombine(vahetatud_args, vana_d_suurendatud_argumentideArv_ja_vahendatud_ret)
  }


  def abiCombine(funReeglid: dl, enneReeglid: dl): dl = {
    funReeglid match {

      case Or(xs@_*) => Or(xs.map(abiCombine(_, enneReeglid)):_*)
      case And(xs@_*) => And(xs.map(abiCombine(_, enneReeglid)):_*)
      case Not(x) => Not(abiCombine(x, enneReeglid))
      case App(f, xs@_*) if f == BinIntOp("=") && xs.head == app(RetVar) => {
        repl(Map[Val, dl](SVar(0) -> xs(1)))(enneReeglid)
      }
      case App(f, xs@_*) if f == BinIntOp("=") && xs(1) == app(RetVar) => {
        repl(Map[Val, dl](SVar(0) -> xs.head))(enneReeglid)
      }
      case App(f, xs@_*) => App(f, xs.map(abiCombine(_, enneReeglid)):_*)
      case a => {
        println("Midagi muud")
        println(a)
        a
      }
    }
  }


  // pinuindeksite vähendaja
  def vS(a: dl): dl = {

    def vs(s: Seq[Val]): List[(Val, dl)] = s match {
      case Seq() => List()
      case SVar(n,t) +: xs => (SVar(n,t), app(SVar(n - 1,t))) :: vs(xs)
      case x +: xs => vs(xs)
    }

    repl(vs(getAtoms(a).toSeq).toMap)(a)
  }

  // pinuindeksite suurendaja
  def sS(a: dl): dl = {

    def ss(s: Seq[Val]): List[(Val, dl)] = s match {
      case Seq() => List()
      case SVar(n,t) +: xs => (SVar(n), app(SVar(n + 1, t))) :: ss(xs)
      case x +: xs => ss(xs)
    }

    repl(ss(getAtoms(a).toSeq).toMap)(a)
  }


  def parseTerm(s: String): Term = {
    val is = new java.io.StringReader(s)
    val lexer = new smtlib.lexer.Lexer(is)
    val parser = new smtlib.parser.Parser(lexer)
    parser.parseTerm
  }

  def term2avaldis(t : Term, varNames : Map[Int, String]) : dl = {

    val m : Map[String, Int] = varNames.toList.foldLeft(Nil: List[(String, Int)])((list, p) => (p._2, p._1) :: list).toMap
    t match {
      case Terms.FunctionApplication(fun, terms) =>
        val tehe = Fun(fun.id.symbol.name,AnyType)
        val vp = terms.head
        val pp = terms(1)

        val vVal : Val = vp match {
          case n : Terms.SNumeral               => IVal(n.value.bigInteger.intValue())
          case Terms.QualifiedIdentifier(id, _) => LVar(m(id.symbol.name))
        }
        val pVal : Val = pp match {
          case n : Terms.SNumeral               => IVal(n.value.bigInteger.intValue())
          case Terms.QualifiedIdentifier(id, _) => LVar(m(id.symbol.name))
        }

        app(tehe, vVal, pVal)
      case a =>
        print("Tundmatu invariant: ")
        println(a)
        True
    }
  }

  override def sThrows(get: node => D.t, set: node => D.t => Unit, to: node, to_final: Boolean, st: D.t)(instr: Instr, ex: Either[String, Set[String]]): D.t = L.top

  override def sStart(get: node => D.t, set: node => D.t => Unit, to: node, to_final: Boolean, st: D.t)(locals: Int, stack: Int, access: Int): D.t = st
}

