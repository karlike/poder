package põder.analyses

import põder.dom.Intervals.{IntLat, IntervalDomain}
import põder.framework._
import põder.framework.cfg._
import põder.util.typ.MethodType


class Value(project: ProgramCFG)  extends SimpleCfgBcSem[Node]{
  override type dl = IntervalDomain  // intervalli asemel peaks olema siin näiteks intervallide stackid vms
  override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = IntLat

  // siin on üldine koht programmipunktide algväärtustamise jaoks -- võmalusel ära seda kasuta
  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = L.bot

  // start --- meetodi kohalike muutujate, stack-i suuruse ja nähtavuse andmed
  override def sStart(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = st

  // hargnemise üleminekufunktsioon
  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: JumpInstr, thn: Boolean): dl = ???

  // "tavaline" üleminekufunktsioon
  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr): dl = ???

  // erindi üleminekufunktsioon
  override def sThrows(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl =
    L.top

  // annotatsiooni ülemunekufunktsioon
  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = ???

  // need on meetodiväljakutsete jaoks
  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = ???
  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = ???
}
