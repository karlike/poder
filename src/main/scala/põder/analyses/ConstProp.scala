package põder.analyses

import põder.analyses
import põder.analyses.ConstProp.{Known, Unknown}
import põder.framework.{EqualOpF, JoinOpF, JsonOpF, Lattice, NarrowOpF, SimpleCfgBcSem, TopOpF, WidenOpF}
import põder.framework.cfg._
import põder.log.Log
import põder.util.json.JsonF
import põder.util.typ.{MethodType, VoidT}

/*
  This is an exercises to help understand Põder and Static Program Analysis in general.
  Do not check in the solution!
*/

/* Test program (save as CPA.java in directory test) below, run using arguments "--analysis cpa --gui test CPA"
class P6der {
  public static void check(boolean b) {}
}

class CPA {
  static int viis(){
    return 5;
  }

  public static void main(String[] args) {
    P6der.check(false); // test
    int x = 1;
    int y = 2;
    int z = x + y;
    P6der.check(z==3);
    int q = viis();
    P6der.check(q==5);
    if (x < z)
      z = 100;
    else
      z = 200;
    P6der.check(z==100);
  }
}
*/

/*
   * First, add case classes to CPValue to implement the domain. Implement top, toJson, leq, and join.
     ** CPValue is meant to be either an integer constant or unknown value. You may add constants of other types.
   * In the end you need to add code to all places marked with ???.
   * Run the program and implement instructions as needed.
     ** At first write default cases that keeps the (abstract) state the same.
     ** Implement your own helper functions as needed.
 */


object ConstProp {
  sealed abstract class CPValue {
    def + (other: CPValue): CPValue
  }
  case class Known (n: Int) extends CPValue {
    override def + (other: CPValue): CPValue = other match {
      case Known(n1) => Known(n + n1)
      case _ => Unknown
    }
  }
  case object Unknown extends CPValue {
    override def + (other: CPValue): CPValue = Unknown
  }

  type vl = CPValue
  val VL: TopOpF[vl] with JoinOpF[vl] with EqualOpF[vl] with JsonOpF[vl] with WidenOpF[vl] with NarrowOpF[vl] =
    new TopOpF[vl] with JoinOpF[vl] with EqualOpF[vl] with JsonOpF[vl] with WidenOpF[vl] with NarrowOpF[vl] {
      override def top: vl = Unknown

      override def toJson[jv](x: vl)(implicit j: JsonF[jv]): jv = x match {
        case Known(n) => j.Num(n)
        case Unknown => j.Str("unknown")
      }


      override def leq(x: vl, y: vl): Boolean = (x, y) match {
        case (_, Unknown) => true
        case (Known(n1), Known(n2)) => n1 == n2
        case _ => false
      }
      override def join(x: vl, y: vl): vl = (x, y) match {
        case (Known(n1), Known(n2)) if (n1 == n2) => x
        case _ => Unknown
      }
      override def widen(oldv: vl, newv: vl): vl = join(oldv,newv)

      override def equal(x: vl, y: vl): Boolean = x==y
  }

  type l = Option[(List[vl],List[vl])]
  val L: Lattice[l] with WidenOpF[l] with NarrowOpF[l] = new Lattice[l] with WidenOpF[l] with NarrowOpF[l] {
    override def bot: l = None
    override def top: l = Some((List(),List()))

    override def toJson[jv](x: l)(implicit j: JsonF[jv]): jv = x match {
      case Some((st,ar)) => j.Obj("stack" -> j.Arr(st.map(VL.toJson(_))), "locals" -> j.Arr(ar.map(VL.toJson(_))))
      case None => j.Str("unreachable")
    }

    override def leq(x: l, y: l): Boolean = {
      if (x.isEmpty)
        true
      else if (y.isEmpty)
        false
      else {
        val (xs, xa) = x.get
        val (ys, ya) = y.get
        (xs,ys).zipped.forall(VL.leq) && (xa,ya).zipped.forall(VL.leq)
      }
    }

    override def join(x: l, y: l): l = {
      if (x.isEmpty)
        y
      else if (y.isEmpty)
        x
      else {
        val (xs, xa) = x.get
        val (ys, ya) = y.get
        Some((xs,ys).zipped.map(VL.join), (xa,ya).zipped.map(VL.join))
      }
    }

    override def widen(oldv: l, newv: l): l = newv

    @scala.annotation.tailrec
    def eqList(xs:List[vl], ys:List[vl]): Boolean = (xs,ys) match {
      case (Nil, Nil)     => true
      case (Nil, x::xs)   => x==VL.top && eqList(Nil,xs)
      case (x::xs, Nil)   => x==VL.top && eqList(Nil,xs)
      case (x::xs, y::ys) => x==y && eqList(xs,ys)
    }

    override def equal(x: l, y: l): Boolean = (x,y) match {
      case (None, None) => true
      case (None, _) => false
      case (_, None) => false
      case (Some((xs,xa)),Some((ys,ya))) => eqList(xs,ys) && eqList(xa,ya)
    }
  }
}

class ConstProp(project: ProgramCFG)  extends SimpleCfgBcSem[Node]{
  override type dl = ConstProp.l
  override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = ConstProp.L

  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = {
    if (nd.is_start) L.top else L.bot
  }

  override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean = {
    val (pkg,mthd,_) = instr.a.x.get
    pkg=="P6der"&&mthd=="check"
  }

  override def sStart(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = {
    if (st.isEmpty)
      return L.bot
    val (stack, locals_) = st.get
    val newLocals = List.fill(locals)(Unknown)
    Some(stack, newLocals)
  }

  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = {
    if (st.isEmpty)
      return Seq()
    Seq(st)
  }

  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = {
    if (st.isEmpty || caller.isEmpty)
      return L.bot
    val (stack, locals) = caller.get
    val (newStack, _) = st.get
    Some(newStack ++ stack, locals)
  }

  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: JumpInstr, thn: Boolean): dl = {
    if (st.isEmpty)
      return L.bot

    val (stack, locals) = st.get
    instr match {
      case IF_ICMPx(NE, a) =>
        val tipp1 :: tipp2 :: saba = stack
        (tipp1, tipp2) match {
          case (Known(n), Known(n1)) => if ((n == n1) == !thn) Some(saba, locals) else L.bot
          case _ => Some(saba, locals)
        }
      case _ => st
    }
  }

  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr): dl = {
    if (st.isEmpty)
      return L.bot
    val (stack, locals) = st.get
    instr match {
      case instr: ICONST => Some(Known(instr.n) :: stack, locals): dl
      case xSTORE(I, a) =>
        val tipp :: saba = stack
        val newLocals = locals.updated(a.x.get, tipp)
        Some(saba, newLocals)
      case xLOAD(I, a) =>
        val newStack = locals(a.x.get) :: stack
        Some(newStack, locals)
      case xADD(I) =>
        val tipp1 :: tipp2 :: saba = stack
        val newStack = tipp1 + tipp2 :: saba
        Some(newStack, locals)
      case INVOKESTATIC(a) if a.x.get._1 == "P6der" && a.x.get._2 == "check" =>
        val tipp :: saba = stack
        tipp match {
          case Known(0) => L.bot
          case _ => Some(saba, locals)
        }
      case d => st
    }
  }

  override def sThrows(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl = st

  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = st
}
