package põder.analyses

import põder.framework.{MapAnalysis, SimpleCfgBcSem, SimpleCfgBcSem2MapAnalysis}
import põder.framework.cfg.{Node, ProgramCFG}
import põder.util.Project

object Registry {
  private var analyses: List[(String, ProgramCFG => MapAnalysis[Node])] = List()

  def register(name: String, sem: ProgramCFG => MapAnalysis[Node]): Unit =
    if (analyses.forall(_._1 != name))
      analyses = (name, sem) :: analyses

  def registerS(name: String, sem: ProgramCFG => SimpleCfgBcSem[Node]): Unit =
    if (analyses.forall(_._1 != name))
      analyses = (name, (p:ProgramCFG) => SimpleCfgBcSem2MapAnalysis(sem(p))) :: analyses

  registerS("dep", p => new Dep(p))
  registerS("modular-sum", p => new ModularSummary(p))
  registerS("value", p => new Value(p))
  registerS("interval", p => new IntervalValue(p))
  registerS("wp", p => new WP(p))
  registerS("modular", p => new Modular(p))
  registerS("sec", p => new SecLabel(p))
  registerS("cpa", p => new ConstProp(p))
  register("classloader", p => new ClassLoader(p,SimpleCfgBcSem2MapAnalysis(new Modular(p))))
  register("optc", p => new OptConstr(p))
//  register("opti", p => new OptInterval(p))

  def getNames: Seq[String] = analyses.map(_._1)

  def getAnalysis(name: String): ProgramCFG => MapAnalysis[Node] = {
    analyses.find(_._1 == name) match {
      case Some(value) => value._2
      case None =>
        println(s"No analysis named: '$name'")
        sys.exit(-1)
    }
  }
}
