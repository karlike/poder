package põder.analyses

import põder.framework.cfg._
import põder.framework.{Lattice, NarrowOpF, SimpleCfgBcSem, WidenOpF}
import põder.log.Log
import põder.util.typ.{BoolT, IntT, MethodType, Parser}
import põder.dom.Intervals.Comparison._
import põder.dom.Intervals.Nums.{IntNum, MInf, PInf}
import põder.dom.Intervals._
import põder.dom.{LVar, Val}
import põder.util.Project

class IntervalValue(project: ProgramCFG) extends SimpleCfgBcSem[Node] {
  override type dl = IntervalDomain
  override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = IntLat


  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = {
    if (nd.is_start) {
      val args = nd.methodInfo.mthdTyp.args
      val d = args.zipWithIndex.foldRight(L.top)((x, y) => {
        x._1 match {
          case IntT => (y._1, Some(y._2.getOrElse(Map()) + (local(nd)(x._2).str -> Interval(MInf, PInf))), y._3)
          case _ => y
        }
      })
      d
    } else L.bot
  }


  override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean = {
    val (klass, mthd, typ) = instr.a.x.get
    (klass == "Util" && mthd == "check") || (klass == "java/lang/Math" && mthd == "random")
  }

  // algseisund
  override def sStart(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = st

  // hargnemise üleminekufunktsioon
  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: JumpInstr, b: Boolean): dl = {
    d match {
      case (None, None, None) => d
      case _ =>
        instr match {
          case IF_ICMPx(GT, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_GT(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() => d2
            }
          case IF_ICMPx(LT, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_LT(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() => d2
            }
          case IF_ICMPx(GE, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_GE(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() =>
                i2 match {
                  case Var(i) =>
                    val localVarName = local(to)(i).str
                    if (b)
                      (d2._1, Some(d2._2.getOrElse(Map()) + (localVarName -> Interval(v1.lb, d2._2.get(localVarName).ub))), d2._3)
                    else
                      (d2._1, Some(d2._2.getOrElse(Map()) + (localVarName -> Interval(d2._2.get(localVarName).lb, Nums.add(v1.ub, IntNum(-1))))), d2._3)
                  case _ => d2
                }
            }
          case IF_ICMPx(EQ, a) =>
            val (d1, v1, i1) = popStack(d)
            var (d2, v2, i2) = popStack(d1)
            compare_EQ(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() => d2
            }
          case IF_ICMPx(NE, a) =>
            val (d1, v1, i1) = popStack(d)
            var (d2, v2, i2) = popStack(d1)
            compare_NE(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() => d2
            }
          case IF_ICMPx(LE, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_LE(v2, v1) match {
              case True() => if (b) d2 else L.bot
              case False() => if (!b) d2 else L.bot
              case Uncertain() => d2
            }
          case IFx(order, a) =>
            val (d1, v1, i1) = popStack(d)
            val result = order match {
              case GT => compare_GT(v1, intToInterval(0))
              case LT => compare_LT(v1, intToInterval(0))
              case NE => compare_NE(v1, intToInterval(0))
              case _ => Uncertain()
            }
            result match {
              case True() => if (b) d1 else L.bot
              case False() => if (!b) d1 else L.bot
              case Uncertain() => d1
            }
          //      case IF_ACMPx(x, a) =>
          case GOTO(a) => d
          //      case IFNULL(a) =>
          //      case IFNONNULL(a) =>
          //      case TABLESWITCH(a) =>
          //      case LOOKUPSWITCH(a) =>
          case _ => d
        }
    }

  }

  // "tavaline" üleminekufunktsioon
  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(i: Instr): dl = {
    Log.log(s"instruction: $i")
    Log.log(s"start state: $d")
    var r = L.bot
    d match {
      case (None, None, None) => r = L.bot
      case _ => r = trans(get, set, to, to_final, d)(i)
    }
    Log.log(s"end state: $r")
    r

  }

  // erindi üleminekufunktsioon
  override def sThrows(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl = L.top

  // annotatsiooni ülemunekufunktsioon
  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = st

  // need on meetodiväljakutsete jaoks
  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = {
    Log.log(s"enter: $d")
    var dt = d
    val x = typ.args.flatMap({
      case IntT | BoolT =>
        val (ds, v, i) = popStack(dt)
        dt = ds
        Seq(v)
      case _ => Seq()
    }).reverse.zipWithIndex.map(x => local(to)(x._2).str -> x._1).toMap
    val domain = (dt._1, Some(x), dt._3)
    Log.log(s"enter new domain: $domain")
    Seq(domain)
  }


  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = {
    val res = typ.ret match {
      case IntT =>
        val (_, v1, i) = popStack(st)
        (Some(v1 :: caller._1.getOrElse(List())), caller._2, Some(i :: caller._3.getOrElse(List())))
      case _ => caller
    }
    Log.log(s"combine:\n\td_prev: $caller\n\td_fun:  $st\n\td_res:  $res\n")
    res
  }


  def pushStack(d: dl, n: Int): dl = {
    pushStack(d, intToInterval(n), Const())
  }

  def pushStack(d: dl, n: Interval, lvar: Expr = Unknown()): dl = {
    (Some(n :: d._1.getOrElse(List())), d._2, Some(lvar :: d._3.getOrElse(List())))
  }

  def popStack(d: dl): (dl, Interval, Expr) = {
    ((Some(d._1.getOrElse(List()).tail), d._2, Some(d._3.getOrElse(List()).tail)), d._1.getOrElse(List()).head, d._3.getOrElse(List()).head)
  }

  def local(n: Node, start: Boolean = false)(i: Int): Val = {
    val vl = LVar(i, start)
    if (n.varNames contains i)
      vl.alias = n.varNames(i)
    vl
  }

  def trans(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl): Function[Instr, dl] = {
    case INVOKESTATIC(a) if a.x.get._1 == "Util" && a.x.get._2 == "check" =>
      val (d1, v1, i1) = popStack(d)
      Log.log(s"push state: $d1")
      if (inVerification) v1 match {
        case Interval(IntNum(0), IntNum(0)) => println("Verification failed! Argument false.")
        case Interval(IntNum(1), IntNum(1)) => println("Verification passed! Argument true.")
        case _ => println(s"Verification unknown! Argument: $v1")
      }
      d1
    case INVOKESTATIC(a) if a.x.get._1 == "java/lang/Math" && a.x.get._2 == "random" =>
      val d1 = pushStack(d, Interval(IntNum(0), IntNum(1)))
      Log.log(s"push state: $d1")
      d1
    case i: ICONST =>
      val d1 = pushStack(d, i.n)
      Log.log(s"push state: $d1")
      d1
    case i: DCONST =>
      val d1 = pushStack(d, i.n.toInt)
      Log.log(s"push state: $d1")
      d1
    case i: BIPUSH =>
      val d1 = pushStack(d, i.a.x.get)
      Log.log(s"push state: $d1")
      d1
    case i: SIPUSH =>
      val d1 = pushStack(d, i.a.x.get)
      Log.log(s"push state: $d1")
      d1
    case xSTORE(t, n) =>
      var (d1, v, _) = popStack(d)
      Log.log(local(to)(n.x.get).str + "=" + v)
      (d1._1, Some(d1._2.getOrElse(Map()) + (local(to)(n.x.get).str -> v)), d1._3)
    case xLOAD(t, a) =>
      val d1 = pushStack(d, d._2.get(local(to)(a.x.get).str), Var(a.x.get))
      Log.log(s"push state: $d1")
      d1
    case xADD(t) =>
      val (d1, v1, i1) = popStack(d)
      val (d2, v2, i2) = popStack(d1)
      val d3 = pushStack(d2, add(v2, v1), Arith("+", i2, i1))
      d3
    case xMUL(t) =>
      val (d1, v1, i1) = popStack(d)
      val (d2, v2, i2) = popStack(d1)
      val d3 = pushStack(d2, mul(v2, v1), Arith("*", i2, i1))
      d3
    case xDIV(t) =>
      val (d1, v1, i1) = popStack(d)
      val (d2, v2, i2) = popStack(d1)
      val d3 = pushStack(d2, div(v2, v1), Arith("/", i2, i1))
      d3
    case i: LDC =>
      val d1 = pushStack(d, i.a.x.get.toString.toDouble.toInt)
      Log.log(s"push state: $d1")
      d1
    case xCMPG(t) =>
      val (d1, v1, i1) = popStack(d)
      val (d2, v2, i2) = popStack(d1)
      val d3 = pushStack(d2, cmp(v2, v1), Arith("=?", i2, i1))
      d3
    case IINC(a) =>
      (d._1, Some(d._2.get + (local(to)(a.x.get._1).str -> add(d._2.get.get(local(to)(a.x.get._1).str).orNull, intToInterval(a.x.get._2)))), d._3)
    case a => d
  }
}
