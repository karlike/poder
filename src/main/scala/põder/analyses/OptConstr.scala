package põder.analyses

import põder.dom.Intervals
import põder.dom.Intervals.Nums.{IntNum, MInf, Num, PInf}
import põder.dom.Intervals.{Interval, IntervalDomain, Nums}
import põder.framework.cfg._
import põder.framework._
import põder.util.json.JsonF
import põder.util.typ.MethodType

import scala.collection.mutable


object NFA {
  def two[T,E](f:T,t:T,any:E): NFA[T,E] = NFA(Set(f,t),Map.empty,Set(f),Set(t),any)

  def pset_epsilon[T,E](a:NFA[T,E], q:Set[T]): Set[T] = {
    var c : Set[T] = q
    var wl: Set[T] = q
    while (wl.nonEmpty){
      val h = wl.head; wl = wl.tail
      val s = a.d.getOrElse(h,List.empty).withFilter(_._1.isEmpty).map(_._2).toSet -- c
      wl ++= s
      c  ++= s
    }
    c
  }

  def pset_d[T,E](a:NFA[T,E], q:Set[T]): Map[E,Set[T]] = {
    val w = q.flatMap(a.d.getOrElse(_,List.empty)).withFilter(_._1.nonEmpty).map{case (e,t) => e.get -> t}.groupBy(_._1)
    val r = w.mapValues(x => pset_epsilon(a,x.map(_._2)))
    r
  }

  def pset_eps[T,E](a:NFA[T,E], q: Set[T]):Boolean = {
    q.exists(a.F.contains)
  }

  def handle_top[T,E](a:NFA[T,E], m:Map[E,Set[T]]): Map[E,(E,Set[T])] = {
    if (m contains a.any) {
      val topval = m(a.any)
      m.foldLeft(Map.empty[E,(E,Set[T])]) {
        case (m1, (e, s)) if e==a.any => m1 + (e -> (e, s))
        case (m1, (e, s)) => m1 + (e -> (a.any, s union topval))
      }
    } else
      m.map{ case (e,s) => e -> (e,s) }
  }



  /*\ The main algorithm is a fusion of Thompsons powerset construction and
   *  automaton multiplication.
   *
   *  The automaton contain ? edges, that represent any element of Σ.
   *  For all a ∈ Σ, a ≤ ?
   *
   *  Input: automaton X, automaton Y
   *
   *  Returns pair (may_leq, may_geq):
   *    may_leq is false iff in the product automaton, X reaches a final state first
   *                         or step in X is bigger than the corresponding step in Y.
   *
   *   may_geq -- symmetric to may_leq
   *
   *
  \*/
  def HKe_cmp[T,E](a:NFA[T,E], b:NFA[T,E]): (Boolean,Boolean) = {
    var may_leq = true
    var may_geq = true
    def step_pairs(e:Set[E], f:Set[E]): Set[(E,E)] = {
      var r: Set[(E, E)] = Set.empty
      for (q <- e union f)
        r += ((q, q))
      r
    }
    def edgeVal(a:NFA[T,E], e:E): Int = if (e==a.any) 1 else 0
    def constrain(e:E,r:E): Unit = {
      val ev = edgeVal(a,e)
      val rv = edgeVal(b,r)
      if (ev>rv) may_leq = false
      else if (ev<rv) may_geq = false
    }
    def iterStep(a:NFA[T,E], aq: Set[T], b:NFA[T,E], bq: Set[T], f: (Set[T], Set[T]) => Unit): Unit = {
      val as1 = handle_top(a, pset_d(a, aq))
      val bs1 = handle_top(b, pset_d(b, bq))

      for ((q1,w1) <- step_pairs(as1.keySet, bs1.keySet)) {
        val (q2, d: Set[T]) = {
            as1.getOrElse(q1, (q1, Set.empty))
        }
        val (w2, g: Set[T]) = {
            bs1.getOrElse(w1, (w1, Set.empty))
        }

        constrain(q2, w2)
        f(d, g)
      }
    }
    var S: List[(Set[T],Set[T])] = List((pset_epsilon(a,a.I),pset_epsilon(b,b.I)))
    val H: mutable.HashSet[(Set[T],Set[T])] = mutable.HashSet.empty
    while (S.nonEmpty){
      val (p,q) = S.head; S = S.tail
      (pset_eps(a,p),pset_eps(b,q)) match {
        case (true, false) => may_leq = false
        case (false, true) => may_geq = false
        case _ => ()
      }
      def f(pn:Set[T],qn:Set[T]):Unit = {
        if (!H.contains(pn, qn)) {
          S = (pn, qn) +: S
          H += ((pn,qn))
        }
      }
      iterStep(a,p,b,q,f)
    }
    (may_leq,may_geq)
  }
}

final case class NFA[T,E](Q: Set[T], d: Map[T,List[(Option[E],T)]], I: Set[T], F: Set[T], any:E) {
  def addNodes(t:T*): NFA[T,E] = {
    NFA(Q++t,d,I,F,any)
  }
  def addEdge(t:T,e:Option[E],u:T): NFA[T,E] = {
    NFA(Q,d+(t -> ((e,u) :: d.getOrElse(t,List()))), I, F,any)
  }
}

class Heap[T] {
  private var m: Map[T,T] = Map.empty
  def find(t:T): T =
    m.get(t) match {
      case Some(value) => find(value)
      case None => t
    }
  def union(t:T, u:T): Unit =
    m += t -> u
}


class OptConstr(p:ProgramCFG) extends MapAnalysis[Node]{
  sealed abstract class Var {
    def str: String
  }
  case class LVar(i:Int) extends Var {
    var alias: String = ""

    def str: String =
      if (alias.nonEmpty) alias else s"l$i"
  }
  case class SVar(i:Int) extends Var {
    override def str: String = s"s$i"
  }

  sealed abstract class LatticeExp {
    def long(p: (Node,Var)=>String): String = toString
    def short(p: (Node,Var)=>String): String = long(p)
  }
  case object Top extends LatticeExp
  case object Bot extends LatticeExp
  case class Val(i:Int) extends LatticeExp {
    override def toString: String = i.toString
  }
  case class Label(n:Node, v:Var) extends LatticeExp{
    override def short(p: (Node,Var)=>String): String = p(n,v)
    override def long(p: (Node,Var)=>String): String = s"L(${n.id},${v.str},${p(n,v)})"
  }
  case class Fun(s:String, as:LatticeExp*) extends LatticeExp {
    private val binops = Set("+","-","*","/","=",">=","<=",">","<","!=","->")
    override def short(p: (Node,Var)=>String): String = {
      if (as.length==2 && (binops contains s))
        s"${as(0).short(p)} $s ${as(1).short(p)}"
      else
        s"$s(${as.map(_.short(p)).mkString(", ")})"
    }
    override def long(p: (Node,Var)=>String): String = {
      if (as.length==2 && (binops contains s))
        s"${as(0).long(p)} $s ${as(1).long(p)}"
      else
        s"$s(${as.map(_.long(p)).mkString(", ")})"
    }
  }
  case class LFPVar(i:Int) extends LatticeExp {
    override def short(p: (Node,Var)=>String): String = s"#$i"
    override def long(p: (Node,Var)=>String): String = s"#$i"
  }
  case class LFP(b:LatticeExp) extends LatticeExp {
    override def short(p: (Node,Var)=>String): String = s"lfp(${b.short(p)})"
    override def long(p: (Node,Var)=>String): String = s"lfp(${b.long(p)})"
  }
  case class Par(v:LVar) extends LatticeExp {
    override def short(p: (Node,Var)=>String): String = s"${v.str}"
    override def long(p: (Node,Var)=>String): String = s"${v.str}"
  }
  type ExpGraph = Map[Either[(Node,Var),Var],LatticeExp]
//  implicit class EGtoString(g:ExpGraph) {
//    def short: String = {
//      def f(n: Node, v: Var): String = g(Some((n, v))).short(f)
//      g(None).short(f)
//    }
//    override def toString: String = {
//      def f(n: Node, v: Var): String = g(Some((n, v))).long(f)
//      g(None).long(f)
//    }
//
//  }


  def idFunction(g:ExpGraph,e:LatticeExp): Boolean = {
    e match {
      case LFPVar(0) => true
      case Label(n, v) => idFunction(g,g(Left((n,v))))
      case Fun("->", _, b) => idFunction(g,b)
      case Fun("join", bs@_*) => bs.forall(idFunction(g,_))
      case _ => false
    }
  }

  private def bin2int(b:Boolean): Int = if (b) 1 else 0
  val operations: Map[String, (Int,Int) => Int] =
    Map("+" -> ((x:Int,y:Int) => x+y),
      "-" -> ((x:Int,y:Int) => x-y),
      "*" -> ((x:Int,y:Int) => x*y),
      "/" -> ((x:Int,y:Int) => x/y),
      "=" -> ((x:Int,y:Int) => x*y),
      "<" -> ((x:Int,y:Int) => bin2int(x<y)),
      ">" -> ((x:Int,y:Int) => bin2int(x>y)),
      "<=" -> ((x:Int,y:Int) => bin2int(x<=y)),
      ">=" -> ((x:Int,y:Int) => bin2int(x>=y)),
      "!=" -> ((x:Int,y:Int) => bin2int(x!=y)),
      "==" -> ((x:Int,y:Int) => bin2int(x==y)))

  def optimize(g:ExpGraph,e:LatticeExp): LatticeExp = {
    e match {
      case Label(_, _) => e
      case Fun("->", a,b) =>
        optimize(g,a) match {
          case Val(0) => Bot
          case Val(1) => optimize(g,b)
          case a1 => Fun("->", a1, optimize(g,b))
        }
      case Fun(s, as@_*) =>
        val as1 = as.map(optimize(g,_))
        if ((as1.length==2) && as1.head.isInstanceOf[Val] && as1(1).isInstanceOf[Val] && (operations contains s)) {
          Val(operations(s)(as1.head.asInstanceOf[Val].i, as1(1).asInstanceOf[Val].i))
        } else {
          Fun(s,as1:_*)
        }
      case LFP(b) =>
        optimize(g,b) match {
          case Fun("join", as@_*) =>
            val as1 = as.filterNot(idFunction(g,_))
            if (as1.length==1)
              as.head
            else LFP(Fun("join",as1:_*))
          case x => LFP(x)
        }
      case _ => e
    }
  }

  object IntLat extends Lattice[Option[Interval]] with MeetOpF[Option[Interval]] with WidenOpF[Option[Interval]] with NarrowOpF[Option[Interval]] {
    override def bot: Option[Interval] = None
    override def top: Option[Interval] = Some(Interval(MInf,PInf))

    override def toJson[jv](x: Option[Interval])(implicit j: JsonF[jv]): jv = {
      def num2String(n:Num):String = {
        n match {
          case IntNum(i) => i.toString
          case Nums.MInf => "-∞"
          case Nums.PInf => "∞"
        }
      }
      x match {
        case Some(Interval(lb,ub)) => j.Str(s"[${num2String(lb)},${num2String(ub)}]")
        case None => j.Str("error")
      }
    }

    def leqNum(x:Num, y:Num): Boolean = {
      (x,y) match {
        case (MInf,_) | (_,PInf) => true
        case (IntNum(i),IntNum(j)) => i<=j
        case _ => false
      }
    }
    override def leq(x: Option[Interval], y: Option[Interval]): Boolean = {
      (x,y) match {
        case (None, _) => true
        case (_, None) => false
        case (Some(Interval(a,b)),Some(Interval(c,d))) =>
          leqNum(c,a) && leqNum(b,d)
      }
    }

    def minNum(x:Num, y:Num): Num = {
      (x,y) match {
        case (MInf,_) | (_,MInf) => MInf
        case (PInf,b) => b
        case (a,PInf) => a
        case (IntNum(i),IntNum(j)) => IntNum(math.min(i,j))
      }
    }

    def maxNum(x:Num, y:Num): Num = {
      (x,y) match {
        case (PInf,_) | (_,PInf) => PInf
        case (MInf,b) => b
        case (a,MInf) => a
        case (IntNum(i),IntNum(j)) => IntNum(math.max(i,j))
      }
    }

    def incNum(x:Num, y:Int = 1): Num = {
      x match {
        case PInf => PInf
        case MInf => MInf
        case IntNum(i) => IntNum(i+y)
      }
    }

    def liftOp(f:(Interval, Interval) => Interval)
              (x: Option[Interval], y: Option[Interval]): Option[Interval] = {
      (x,y) match {
        case (None,_) | (_,None) => None
        case (Some(i), Some(j)) => Some(f(i,j))
      }
    }

    def liftm(f: (Int,Int) => Int, x:Num, y:Num, c: Num): Num = (x,y) match {
      case (IntNum(i), IntNum(j)) => IntNum(f(i,j))
      case _ => c
    }

    def simpOp(f: (Int,Int) => Int, a: Interval, b: Interval): Interval = {
      Interval(liftm(f,a.lb,b.lb,MInf), liftm(f,a.ub,b.ub,PInf))
    }

    def cmplxOp(f: (Num,Num) => Num, a: Interval, b: Interval): Interval = {
      val values = Set(f(a.lb,b.lb), f(a.lb,b.ub), f(a.ub,b.lb), f(a.ub, b.ub))
      Interval(values.foldLeft[Num](PInf)(minNum), values.foldLeft[Num](MInf)(maxNum))
    }

    override def join(x: Option[Interval], y: Option[Interval]): Option[Interval] = {
      (x,y) match {
        case (None, b) => b
        case (a, None) => a
        case (Some(Interval(a,b)),Some(Interval(c,d))) =>
          Some(Interval(minNum(a,c), maxNum(b,d)))
      }
    }

    override def meet(x: Option[Interval], y: Option[Interval]): Option[Interval] = {
      (x,y) match {
        case (None, _) | (_, None) => None
        case (Some(Interval(a,b)),Some(Interval(c,d))) =>
          val lb = maxNum(a,c)
          val ub = minNum(b,d)
          if (leqNum(lb,ub))
            Some(Interval(lb,ub))
          else
            None
      }
    }

    def eqOrVal(a:Num, b:Num, c: =>Num): Num = {
      (a,b) match {
        case (IntNum(i), IntNum(j)) => if (i==j) a else c
        case _ => a
      }
    }

    override def widen(oldv: Option[Interval], newv: Option[Interval]): Option[Interval] = {
      (oldv,newv) match {
        case (Some(Interval(a,b)),Some(Interval(c,d))) =>
          Some(Interval(eqOrVal(c,a,MInf), eqOrVal(d,b,PInf)))
        case (_,b) => b
      }
    }

    override def narrow(oldv: Option[Interval], newv: Option[Interval]): Option[Interval] = {
      (oldv,newv) match {
        case (Some(Interval(a,b)),Some(Interval(c,d))) =>
          val lb = if (a == MInf) c else a
          val ub = if (b == PInf) d else b
          Some(Interval(lb,ub))
        case (_,b) => b
      }
    }

    def mul(a:Num, b:Num): Num = {
      (a,b) match {
        case (MInf,MInf) => PInf
        case (MInf,PInf) => MInf
        case (PInf,MInf) => MInf
        case (PInf,PInf) => PInf
        case (PInf,IntNum(j)) => if (j<0) MInf else PInf
        case (MInf,IntNum(j)) => if (j<0) PInf else MInf
        case (IntNum(i),PInf) => if (i<0) MInf else PInf
        case (IntNum(i),MInf) => if (i<0) PInf else MInf
        case (IntNum(i),IntNum(j)) => IntNum(i*j)
      }
    }


    def containsZero(a:Interval): Boolean = {
      a match {
        case Interval(MInf,MInf) => false
        case Interval(MInf,PInf) => true
        case Interval(PInf,MInf) => false
        case Interval(PInf,PInf) => false
        case Interval(PInf,IntNum(_)) => false
        case Interval(MInf,IntNum(j)) => j>=0
        case Interval(IntNum(i),PInf) => i<=0
        case Interval(IntNum(_),MInf) => false
        case Interval(IntNum(i),IntNum(j)) => i<=0 && j>=0
      }
    }

    def inv(a:Num): Num = {
      a match {
        case IntNum(i) => IntNum(1/i)
        case _ => IntNum(0)
      }
    }

    def inv(a:Interval): Interval = {
      a match {
        case Interval(IntNum(0), m) => Interval(inv(m), PInf)
        case Interval(n,IntNum(0)) => Interval(MInf, inv(n))
        case _ if containsZero(a) => Interval(MInf, PInf)
        case Interval(n,m) => Interval(inv(m),inv(n))
      }
    }

    def div(a:Interval, b:Interval): Interval = {
      cmplxOp(mul, a, inv(b))
    }

    def leq(a:Num, b:Num): Num = {
      (a,b) match {
        case (MInf,MInf) => IntNum(1)
        case (MInf,PInf) => IntNum(1)
        case (PInf,MInf) => IntNum(0)
        case (PInf,PInf) => IntNum(1)
        case (PInf,IntNum(_)) => IntNum(0)
        case (MInf,IntNum(_)) => IntNum(1)
        case (IntNum(_),PInf) => IntNum(1)
        case (IntNum(_),MInf) => IntNum(0)
        case (IntNum(i),IntNum(j)) => if (i<=j) IntNum(1) else IntNum(0)
      }
    }

    def lt(a:Num, b:Num): Num = {
      (a,b) match {
        case (MInf,MInf) => IntNum(0)
        case (MInf,PInf) => IntNum(1)
        case (PInf,MInf) => IntNum(0)
        case (PInf,PInf) => IntNum(0)
        case (PInf,IntNum(_)) => IntNum(0)
        case (MInf,IntNum(_)) => IntNum(1)
        case (IntNum(_),PInf) => IntNum(1)
        case (IntNum(_),MInf) => IntNum(0)
        case (IntNum(i),IntNum(j)) => if (i<j) IntNum(1) else IntNum(0)
      }
    }

    def eql(a:Num, b:Num): Num = {
      (a,b) match {
        case (MInf,MInf) => IntNum(1)
        case (MInf,PInf) => IntNum(0)
        case (PInf,MInf) => IntNum(0)
        case (PInf,PInf) => IntNum(1)
        case (PInf,IntNum(_)) => IntNum(0)
        case (MInf,IntNum(_)) => IntNum(0)
        case (IntNum(_),PInf) => IntNum(0)
        case (IntNum(_),MInf) => IntNum(0)
        case (IntNum(i),IntNum(j)) => if (i==j) IntNum(1) else IntNum(0)
      }
    }

    def neg(f:(Num,Num) => Num)(a:Num,b:Num): Num = {
      f(a, b) match {
        case IntNum(0) => IntNum(1)
        case IntNum(_) => IntNum(0)
        case MInf => PInf
        case PInf => MInf
      }
    }
  }

  val cmpSet = Set("<", "<=", "=", "!=", ">", ">=")
  def guardExp(g:ExpGraph, e: LatticeExp, v:Map[LVar,Option[Interval]], s:List[Option[Interval]]): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
    def lift(e:LatticeExp, f: Interval => (Map[LVar,Option[Interval]],List[Option[Interval]])): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
      evalExp(g,e,v,s) match {
        case Some(iv) => f(iv)
        case None => (v,s)
      }
    }
    def lt(i:Either[LFPVar,LVar], j:LatticeExp): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
      lift(j, iv => guardLFP(i, Interval(MInf, IntLat.incNum(iv.ub, -1))))
    }
    def leq(i:Either[LFPVar,LVar], j:LatticeExp): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
      lift(j, iv => guardLFP(i, Interval(MInf, iv.ub)))
    }
    def gt(i:Either[LFPVar,LVar], j:LatticeExp): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
      lift(j, iv => guardLFP(i, Interval(IntLat.incNum(iv.lb), PInf)))
    }
    def geq(i:Either[LFPVar,LVar], j:LatticeExp): (Map[LVar,Option[Interval]],List[Option[Interval]]) = {
      lift(j, iv => guardLFP(i, Interval(iv.lb, PInf)))
    }
    def guardLFP(i:Either[LFPVar,LVar], e:Interval): (Map[LVar,Option[Interval]],List[Option[Interval]])  = {
      i match {
        case Left(o) =>
          if (s.length < o.i)
            (v,s)
          else {
            s.splitAt(o.i) match {
              case (a,h::b) => (v, a ++ (IntLat.meet(h,Some(e)) :: b))
            }
          }
        case Right(o) =>
          if (v contains o)
            (v.updated(o,IntLat.meet(v(o),Some(e))), s)
          else
            (v,s)
      }
    }

    e match {
      case Fun(o, l, r) if cmpSet contains o =>
        (o,optimize(g,l), optimize(g,r)) match {
          case ("<", LFPVar(i), j) => lt(Left(LFPVar(i)), j)
          case (">=", j, LFPVar(i)) => lt(Left(LFPVar(i)), j)
          case ("<=", LFPVar(i), j) => leq(Left(LFPVar(i)), j)
          case (">", j, LFPVar(i)) => leq(Left(LFPVar(i)), j)
          case ("=", LFPVar(i), j) => lift(j, guardLFP(Left(LFPVar(i)), _))
          case (">", LFPVar(i), j) => gt(Left(LFPVar(i)), j)
          case ("<=", j, LFPVar(i)) => gt(Left(LFPVar(i)), j)
          case (">=", LFPVar(i), j) => geq(Left(LFPVar(i)), j)
          case ("<", j, LFPVar(i)) => geq(Left(LFPVar(i)), j)
          case ("<", Par(i), j) => lt(Right(i), j)
          case (">=", j, Par(i)) => lt(Right(i), j)
          case ("<=", Par(i), j) => leq(Right(i), j)
          case (">", j, Par(i)) => leq(Right(i), j)
          case ("=", Par(i), j) => lift(j, guardLFP(Right(i), _))
          case (">", Par(i), j) => gt(Right(i), j)
          case ("<=", j, Par(i)) => gt(Right(i), j)
          case (">=", Par(i), j) => geq(Right(i), j)
          case ("<", j, Par(i)) => geq(Right(i), j)
          case _ => (v,s)
        }
      case Label(n,v1) => guardExp(g,optimize(g,g(Left((n,v1)))),v,s)
      case _ => (v,s)
    }
  }


  def evalExp(g:ExpGraph, e:LatticeExp, v:Map[LVar,Option[Interval]] = Map(), s:List[Option[Interval]] = List.empty): Option[Interval] = {
    var visited = Set.empty[(Node,Var)]
    def evalExp1(e: LatticeExp, v: Map[LVar, Option[Interval]], s: List[Option[Interval]]): Option[Interval] = {
      import IntLat._
      e match {
        case Top => top
        case Bot => None
        case Val(i) => Some(Interval(IntNum(i), IntNum(i)))
        case Label(n, v1) =>
          if (visited contains ((n,v1)))
            IntLat.top
          else {
            visited += ((n,v1))
            evalExp1(g.getOrElse(Left((n, v1)),Bot), v, s)
          }
        case Fun("+", a, b) =>
          val qq = liftOp(simpOp(_ + _, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
          qq
        case Fun("-", a, b) => liftOp(simpOp(_ - _, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("*", a, b) => liftOp(cmplxOp(mul, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("=", a, b) => liftOp(cmplxOp(eql, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("!=", a, b) => liftOp(cmplxOp(neg(eql), _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("<", a, b) => liftOp(cmplxOp(lt, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("<=", a, b) => liftOp(cmplxOp(leq, _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun(">", a, b) => liftOp(cmplxOp(neg(lt), _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun(">=", a, b) => liftOp(cmplxOp(neg(leq), _, _))(evalExp1(a, v, s), evalExp1(b, v, s))
        case Fun("->", a, b) =>
          val av = evalExp1(a, v, s)
          if (av.isEmpty || av.get == Interval(IntNum(0), IntNum(0)))
            None
          else {
            val (v1, s1) = guardExp(g, a, v, s)
            evalExp1(b, v1, s1)
          }
        case Fun("join", as@_*) => as.map(evalExp1(_, v, s)).foldLeft(bot)(join)
        case Fun(_, _*) => top
        case LFPVar(i) => s.applyOrElse(i, (_: Int) => top)
        case LFP(b) =>
          var c = bot
          var n = bot
          do {
            c = n
            val r = evalExp1(b, v, c :: s)
            n = widen(c, join(c, r))
            //          println(s"widening - ${c} ${r} = ${n}")
          } while (!leq(n, c))
          do {
            c = n
            val r = evalExp1(b, v, c :: s)
            n = narrow(c, r)
            //          println(s"narrowing - ${c} ${r} = ${n}")
          } while (!leq(c, n))
          //        println("done")
          c
        case Par(x) => v.getOrElse(x, top)
      }
    }
    val r = evalExp1(e, v, s)
    //    printf("evalExp(%s,%s,%s) = %s\n", e.short, v.mkString("[",",","]"),s.mkString("[",",","]"), IntLat.toJson(r)(Json).toText)
    r
  }

  sealed abstract class LatticeExpLabel
  final case class ValLabel(i:Int) extends LatticeExpLabel
  final case class LabelLabel(n:Node, v:Var) extends LatticeExpLabel
  final case class ParLabel(v:LVar) extends LatticeExpLabel
  final case class FunLabel(s:String) extends LatticeExpLabel
  final object AnyLabel extends LatticeExpLabel
  final case class LFPVarLabel(i: Int) extends LatticeExpLabel

  var counter: Int = 0
  def nextCounter(): Int = {counter += 1; counter}

  def toNFA(x:LatticeExp): NFA[Int,LatticeExpLabel] = {
    def addToNFA(s:Int,x:LatticeExp,t:Int,a: NFA[Int,LatticeExpLabel],st:List[(Int,Int)]): NFA[Int,LatticeExpLabel] = {
      x match {
        case Bot =>
          a
        case Top =>
          val m1 = nextCounter()
          val m2 = nextCounter()
          a.addNodes(m1,m2).addEdge(s,None,m1).addEdge(m2,None,t).addEdge(m2,None,m1).addEdge(m1,Some(AnyLabel),m2)
        case Val(i) => a.addEdge(s,Some(ValLabel(i)),t)
        case Label(n, v) =>
          a.addEdge(s,Some(LabelLabel(n,v)),t)
        case Fun("join", as@_*) =>
          as.foldLeft(a){case (na,d) => addToNFA(s,d,t,na,st)}
        case Fun(f, as@_*) =>
          @scala.annotation.tailrec
          def helper(q:Int, xs:List[LatticeExp], na:NFA[Int,LatticeExpLabel]):NFA[Int,LatticeExpLabel] = {
            xs match {
              case Nil =>
                na.addEdge(q,Some(FunLabel(f)),t)
              case List(x) =>
                val m = nextCounter()
                addToNFA(q,x,m,na.addNodes(m).addEdge(m,Some(FunLabel(f)),t),st)
              case x::xs =>
                val m = nextCounter()
                helper(m,xs,addToNFA(q,x,m,na.addNodes(m),st))
            }
          }
          helper(s,as.toList,a)
        case LFPVar(i) =>
          if (i < st.length) {
            val (m1, m2) = st(i)
            a.addEdge(s, None, m1).addEdge(m2, None, t)
          } else {
            a.addEdge(s, Some(LFPVarLabel(i)), t)
          }
        case LFP(b) => addToNFA(s,b,t,a,(s,t)::st)
        case Par(v) => a.addEdge(s,Some(ParLabel(v)),t)
      }
    }
    val s = nextCounter()
    val t = nextCounter()
    addToNFA(s,x,t,NFA.two(s,t,AnyLabel),List())
  }

  def structEq(x:LatticeExp, y:LatticeExp): Boolean = {
    (x,y) match {
      case (Top,Top) => true
      case (Val(i), Val(j)) => i==j
      case (Fun(o,is@_*), Fun(p,js@_*)) => o==p && is.length==js.length && (is,js).zipped.forall(structEq)
      case (Label(n,v), Label(m,u)) => n==m && v==u
      case (LFPVar(i),LFPVar(j)) => i==j
      case (LFP(i), LFP(j)) => structEq(i,j)
      case (Par(i), Par(j)) => i==j
      case _ => false
    }
  }


  def findLFPs(g:ExpGraph, x:LatticeExp): Seq[LatticeExp] = {
    x match {
      case Label(n,v) => findLFPs(g, g(Left((n,v))))
      case Fun(_, as@_*) => as.flatMap(findLFPs(g,_))
      case LFP(b) => findLFPs(g,b) :+ b
      case _ => Seq.empty
    }
  }

  def replace(x:LatticeExp, m:Int, s:LatticeExp):LatticeExp = {
    x match {
      case Label(n, v) => Label(n, v)
      case Fun(o, as@_*) => Fun(o, as.map(replace(_,m,s)):_*)
      case LFP(b) => LFP(replace(b, m+1, s))
      case LFPVar(n) if n==m => s
      case _ => x
    }
  }


  def reduceLFP(g:ExpGraph, x:LatticeExp):LatticeExp = {
    val zs = findLFPs(g,x).toSet
    for (z <- zs) {
      val x1 = replace(z,0,LFP(z))
      if (structEq(x,x1)) {
//        println(s"reduceLFP(${x}) = ${LFP(z)}")
        return LFP(z)
      }
    }
    x
  }



  def mycompare(x: LatticeExp, y: LatticeExp): (Boolean,Boolean) =  {
    printf("compare(%s,%s)\n",x,y)
    if (x==y) {
      printf("compare(%s,%s)=EQ\n",x,y)
      return (true, true)
    }
    val xa = toNFA(x)
    val ya = toNFA(y)
    val p = NFA.HKe_cmp(xa, ya)
    printf("compare(%s,%s)=%s\n",x,y,p)
    p
  }


  def myjoin(x: LatticeExp, y: LatticeExp): LatticeExp = {
    val (l,g) = mycompare(x,y)
    if (l)
      y
    else if (g)
      x
    else
      Fun("join",x,y)
  }

//  def splitLFP(g:ExpGraph, n: Node, v:Var, e:LatticeExp):(LatticeExp, Set[LatticeExp]) = {
//    e match {
//      case Label(n1, v1) if (n1==n) && (v1==v) =>
//        splitLFP(g,n,v,g(Left((n1,v1)))) match {
//          case (q,s) =>
//            (Label(n1, v1, LFPVar(0)),s+q)
//        }
//      case Label(n1, v1, d) =>
//        splitLFP(n,v,d) match {
//          case (g,s) =>
//            (Label(n1,v1,g),s)
//        }
//      case Fun(n1, as@_*) =>
//        val fs = as.map(splitLFP(n,v,_))
//        (Fun(n1,fs.map(_._1):_*), fs.foldLeft(Set.empty[LatticeExp]){case (s,(_,s1)) => s union s1 })
//      case LFP(d) => (LFP(d), Set.empty) // its already part of lfp
//      case c => (c, Set.empty)
//    }
//  }

//  def toLFP(n:Node, v:Var, d:LatticeExp): LatticeExp = {
//    def explodeJoin(d:LatticeExp):Set[LatticeExp] = {
//      d match {
//        case Fun("join",bs@_*) => bs.toSet.flatMap(explodeJoin)
//        case x => Set(x)
//      }
//    }
//    def explodeLFP(d:LatticeExp):Set[LatticeExp] = {
//      d match {
//        case LFP(b) => explodeJoin(b)
//        case x => explodeJoin(x)
//      }
//    }
//    splitLFP(n, v, d) match {
//      case (g,s) if s.nonEmpty =>
//        val s1 = s.flatMap(explodeLFP)+g
//        val s2 = s1.foldLeft(s1) {
//          case (s,e) =>
//            val s3 = s1-e
//            if (mycompare(Fun("join", (s3).toSeq:_*),e)._2) s3 else s
//        }
//        val r = LFP(Fun("join", s2.toSeq:_*))
////        println(s"toLFP($n,${v.str},${d.short}) = ${r.short}")
//        r
//      case (g,_) =>
//        g
//    }
//  }

  type key = Unit
  override val Key: JsonOpF[Unit] = new JsonOpF[key] {
    override def toJson[jv](x: key)(implicit j: JsonF[jv]): jv =
      j.Null
  }
  override val startKeys = Seq(())

  sealed trait ExpMapLat
  case object TopMap extends ExpMapLat
  case object BotMap extends ExpMapLat
  case class MapObj(m:ExpGraph) extends ExpMapLat

  override type dl = ExpMapLat
  override implicit val L: Lattice[ExpMapLat] with WidenOpF[ExpMapLat] with NarrowOpF[ExpMapLat] = new Lattice[ExpMapLat] with WidenOpF[ExpMapLat] with NarrowOpF[ExpMapLat] {
    override def bot: ExpMapLat = BotMap
    override def top: ExpMapLat = TopMap

    override def toJson[jv](x: ExpMapLat)(implicit j: JsonF[jv]): jv = {
      x match {
        case TopMap => j.Str("unknown state")
        case BotMap => j.Str("statically unreachable")
        case MapObj(m) =>
          var s = Set.empty[(Node,Var)]
          def f(n:Node, v:Var): String = {
            if (s contains ((n,v))) "@" else {
              s += ((n,v))
              val r = m.getOrElse(Left((n,v)),Bot).short(f)
              s -= ((n,v))
              r
            }
          }
          def g(n:Node, v:Var): String = {
            if (s contains ((n,v))) "@" else {
              s += ((n,v))
              val r = m.getOrElse(Left((n,v)),Bot).long(f)
              s -= ((n,v))
              r
            }
          }
          j.Obj(m.flatMap{case (Right(v),d) =>
            Map(s"${v.str}" ->
                j.Arr(j.Str("data: " ++ d.long(g))
                  ,   j.Str("short: " ++ d.short(f))
                  ,   j.Str("opt: " ++ optimize(m,d).short(f))
                  ,   IntLat.toJson(evalExp(m,d))
                ))
          case (Left(_),_) => Map.empty[String,jv]
          })
      }
    }

    override def equal(x: ExpMapLat, y: ExpMapLat): Boolean = {
      (x, y) match {
        case (TopMap, TopMap) | (BotMap, BotMap) =>
          true
        case (MapObj(a), MapObj(b)) if a.keySet == b.keySet =>
          a.forall { case (v, d) =>
            val (l,g) = mycompare(d, b(v))
            l&&g
          }
        case _ =>
          false
      }
    }

    override def leq(x: ExpMapLat, y: ExpMapLat): Boolean =  {
      (x,y) match {
        case (_, TopMap) | (BotMap, _) =>
          true
        case (TopMap,_) | (_, BotMap)  =>
          false
        case (MapObj(a), MapObj(b)) if a.keySet subsetOf b.keySet =>
          a.forall{case (v,d) => mycompare(d,b(v))._1}
        case _ => false
      }
    }

    override def join(x: ExpMapLat, y: ExpMapLat): ExpMapLat =  {
      (x,y) match {
        case (a, BotMap) => a
        case  (BotMap,a) => a
        case (MapObj(a), MapObj(b))  =>
          var r = a.map {
            case (k,v) if b contains k => k -> myjoin(v,b(k))
            case (k,v)  => k -> v
          }
          r = (b.keySet -- a.keySet).foldLeft(r){
            case (m,k) => m + (k -> b(k))
          }
          MapObj(r)
        case _ => TopMap
      }
    }


    override def widen(oldv: ExpMapLat, newv: ExpMapLat): ExpMapLat = join(oldv,newv)

    override def narrow(oldv: ExpMapLat, newv: ExpMapLat): ExpMapLat = oldv
  }
  def local(n:Node, i:Int): LVar = {
    val vl =  LVar(i)
    if (n.varNames contains i)
      vl.alias = n.varNames(i)
    vl
  }


  override def sNode(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit)(k: Unit, nd: Node): ExpMapLat = {
    if (nd.is_start && nd.methodInfo.cls == p.mainClass && p.startMethods.contains((nd.methodInfo.mthd,nd.methodInfo.mthdTyp))) {
      val ln = nd.methodInfo.mthdTyp.args.length + (if (nd.methodInfo.is_static) 0 else 1)
      MapObj(Map(
        (0 until ln).map(i => {
          val p = local(nd, i)
          Right(p) -> Par(p)
        }) :_*
      ))
    } else
      BotMap
  }


  def labAll(fr: Node, fr_s: Boolean, m: ExpGraph):ExpGraph = {
    if (fr_s) {
      m.flatMap {
        case (Left(k), v) => Seq(Left(k) -> v)
        case (Right(k), v) =>
          if (m contains (Left((fr,k))))
            Seq(Left((fr, k)) -> myjoin(m(Left((fr,k))),v), Right(k) -> Label(fr, k))
          else
            Seq(Left((fr, k)) -> v, Right(k) -> Label(fr, k))
      }
    } else
      m
  }
  override def sBranch(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, t: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, instr: JumpInstr, thn: Boolean): ExpMapLat = {
    implicit val to: Node = t
    val nst = st(()) match {
      case TopMap => return TopMap
      case BotMap => return BotMap
      case MapObj(m) => labAll(fr,fr_s,m)
    }
    def negate(o:Order):Order = {
      o match {
        case EQ => NE
        case NE => EQ
        case LT => GE
        case GE => LT
        case GT => LE
        case LE => GT
      }
    }
    val r = instr match {
      case IF_ICMPx(o,a) =>
        val ord = if (thn) o else negate(o)
        def guard(x:LatticeExp):LatticeExp = Fun("->", Fun(ord.str, nst(Right(SVar(1))), nst(Right(SVar(0)))), x)
        MapObj(pop(nst,2).map{
          case (Left(k),v)  => Left(k) -> v
          case (Right(k),v) => Right(k) -> guard(v)
        })
      case _ => MapObj(nst)
    }
    r
  }

  def push(m:ExpGraph, step:Int=1)(implicit to:Node): ExpGraph = {
    m.map{
      case (Right(SVar(i)), exp) => Right(SVar(i+step)) -> exp
      case (Right(LVar(i)), exp) => Right(local(to,i)) -> exp
      case (Left(v),exp) => Left(v) -> exp
    }
  }

  def pop(m:ExpGraph, step:Int=1)(implicit to:Node): ExpGraph = {
    m.flatMap{
      case (Right(SVar(i)), _) if i<step => Seq()
      case (Right(SVar(i)), exp) => Seq(Right(SVar(i-step)) -> exp)
      case (Right(LVar(i)), exp) => Seq(Right(local(to,i)) -> exp)
      case (Left(v),exp) => Seq(Left(v) -> exp)
    }
  }

  override def sNormal(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit,fr:Node, fr_s:Boolean,  t: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, instr: Instr): ExpMapLat = {
    implicit val to: Node = t
    val nst = st(()) match {
      case TopMap => return TopMap
      case BotMap => return BotMap
      case MapObj(m) => labAll(fr,fr_s,m)
    }
    def get(v:Var): LatticeExp = {
      nst.getOrElse(Right(v),Label(fr,v))
    }
    val r = instr match {
      case BIPUSH(a) =>
        MapObj(push(nst) + (Right(SVar(0)) -> Val(a.x.get)))
      case a:ICONST =>
        MapObj(push(nst) + (Right(SVar(0)) -> Val(a.n)))
      case xSTORE(_,a) =>
        MapObj(pop(nst) + (Right(local(to,a.x.get)) -> get(SVar(0))))
      case xLOAD(_,a) =>
        MapObj(push(nst) + (Right(SVar(0)) -> get(local(to,a.x.get))))
      case xADD(_) =>
        MapObj(push(pop(nst,2)) + (Right(SVar(0)) ->
          Fun("+", get(SVar(1)), get(SVar(0)))
          ))
      case xSUB(_) =>
        MapObj(push(pop(nst,2)) + (Right(SVar(0)) ->
          Fun("-", get(SVar(1)), get(SVar(0)))
          ))
      case RETURN =>
        MapObj(Map.empty)
      case xRETURN(_) =>
        MapObj(Map(Right(SVar(0)) -> get(SVar(0))))

      case _ => st(k)
    }
    printf("inst %s = %s\n",instr,r)
    r
  }

  override def sStart(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, to: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, locals: Int, stack: Int, access: Int): ExpMapLat =  {
    st(())
  }

  override def sThrows(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, to: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, instr: Instr, ex: Either[String, Set[String]]): ExpMapLat = st(k)

  override def sAnnot(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, to: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, ant: Annot): ExpMapLat = st(k)

  override def senter(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, to: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, instr: MethodInstr, typ: MethodType, static: Boolean): Seq[ExpMapLat] = Seq(TopMap)

  override def scombine(get: Node => Unit => ExpMapLat, set: Node => Unit => ExpMapLat => Unit, fr:Node, fr_s:Boolean, to: Node, to_final: Boolean, st: Unit=>ExpMapLat)(k: Unit, instr: MethodInstr, typ: MethodType, static: Boolean, caller: Unit=>ExpMapLat): ExpMapLat = {
    caller(k)
    //    val staticOff = if (static) 0 else 1
    //    k match {
    //      case LVar(i) => st(local(to,i))
    //      case SVar(0) =>
    //        val (c,m,t) = instr.a.x.get
    //        val tp = Parser.mpar(t)
    //        val nd = p.returnVarsFun(c,m,tp)
    //        val res = get(nd)(SVar(0))
    //        val v = tp.args.zipWithIndex.foldLeft(res) {
    //          case (s, (_, i)) =>
    //            replacePar(s, i, label(fr, fr_s, k, caller(SVar(i + staticOff))))
    //        }
    //        if (containsLabel(to,k,res)) {
    //          val vq = simplifyJoin(replaceLabeled(to,k,v,Bot))
    //          val vs = findPars(vq)
    //          val nv = vs.zipWithIndex.foldLeft(vq){
    //            case (s,(v,i)) => replacePar(s,v.i,LFPVar(i))
    //          }
    //          reduce(Label(nd, k, vs.foldLeft(simplifyJoin(Fun("join",vq,nv)))((q,_) => LFP(q))))
    //        } else {
    //          Label(nd, k, v)
    //        }
    //      case SVar(i) => st(SVar(i-1-staticOff))
    //    }
  }
}
