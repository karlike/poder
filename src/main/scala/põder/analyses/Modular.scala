package põder.analyses

import põder.util.typ.{MethodType, ObjectT, Parser, Type, VoidT}
import põder.dom.LogicHelper._
import põder.dom.Logic._
import põder.dom.{LogLat, _}
import põder.framework._
import põder.framework.cfg.{ARRAYLENGTH, _}
import põder.log.Log
import põder.util.Project
import põder.util.json.JsonF

import scala.language.postfixOps


object D extends Product(LogLat,LogLat) with WidenOpF[(LogLat.t,LogLat.t)] with NarrowOpF[(LogLat.t,LogLat.t)] {
  override def widen(oldv: (LogLat.t, LogLat.t), newv: (LogLat.t, LogLat.t)): (LogLat.t, LogLat.t) =
    (LogLat.widen(oldv._1,newv._1), LogLat.widen(oldv._2,newv._2))

  override def toJson[jv](x: (LogLat.t, LogLat.t))(implicit j: JsonF[jv]): jv = {
    import j._
    Obj("kokkuvõte" -> LogLat.toJson(x._1), "saavutatav" -> LogLat.toJson(x._2))
  }
}
case class FieldVar(cls: String, fld: String, override val typ:SimpType) extends Var(typ) {
  override def str: String = cls++"."++fld
}

abstract class ModularLogic extends SimpleCfgBcSem[Node] {
  type node = Node

  type l = LogLat.t
  type el = (node => l) => (node => l => Unit) => (node,Boolean) => l => l



  def havoc(x:Val, o:Boolean = true): Logic => Logic = inForall{
    case Not(y) => not(havoc(x,!o)(y))
    case Or(xs@_*) =>  or(xs.map(havoc(x,o)) :_*)
    case And(xs@_*) =>
      def eqProp: Logic => Seq[Logic] = {
        case App(BinIntProp("="), App(y), App(z)) if x == y && x == z => Seq()
        case App(BinIntProp("="), App(y), e) if x == y && !getAtoms(e).contains(y) => Seq(e)
        case App(BinIntProp("="), e, App(y)) if x == y && !getAtoms(e).contains(y) => Seq(e)
        case And(xs@_*) => xs.flatMap(eqProp)
        case _ => Seq()
      }
      val v: Option[Logic] = xs.flatMap(eqProp).headOption
      v match {
        case Some(value) => and(xs.map(rewriteExp(_,x,value)).map(simplify):_*)
        case _ => and(xs.map(havoc(x,o)):_*)
      }
    case True => True
    case False => False
    case App(f,y@_*) => forgetE(x,o)(App(f,y:_*))
  }

  def havoc1(x:Val): Logic => Logic = z => {
    val r = havoc(x)(z)
//    printf("havoc(%s,%s):\n%s\n", x.str, clearString(z), clearString(r))
    r
  }


  def args(n:Node,t:Seq[Type], static:Boolean): Seq[(Int,LVar)] = {
    val m = (if (static) 0 else 1) + t.length - 1
    (0 to m).map(i => (i,local(n,start = true)(i)))
  }

  def ent(n:Node)(t:Seq[Type],static: Boolean): LogLat.t = {
    val as = args(n,t,static)
    val bs = as.foldRight(LogLat.top) {
      case ((i,v), b) => and(b,app(BinIntProp("="),local(n)(i),v))
    }
    bs
  }

  def local(n:Node, start:Boolean = false)(i:Int): LVar = {
    val vl =  LVar(i, start)
    if (n.varNames contains i)
      vl.alias = n.varNames(i)
    vl
  }

  override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean = {
    val (klass, mthd, typ) = instr.a.x.get
    (klass == "P6der" && mthd == "check") || (klass == "java/lang/Math" && mthd == "random")
  }

  def forallInForall(f: LogLat.t => LogLat.t): LogLat.t => LogLat.t = {
    case ForallL(t, x) => ForallL(t, forallInForall(f)(x))
    case x => f(x)
  }

  // compare with zero
  def branchIFx(x:Order, d: LogLat.t, b: Boolean): LogLat.t = {
    def ordTrue = x match {
      case EQ => app(BinIntProp("="), SVar(0), IVal(0))
      case NE => not(app(BinIntProp("="), SVar(0), IVal(0)))
      case LT => app(BinIntProp("<"), SVar(0), IVal(0))
      case GE => app(BinIntProp(">="), SVar(0), IVal(0))
      case GT => app(BinIntProp(">"), SVar(0), IVal(0))
      case LE => app(BinIntProp("<="), SVar(0), IVal(0))
    }
    def ordFalse = x match {
      case EQ => not(app(BinIntProp("="), SVar(0), IVal(0)))
      case NE => app(BinIntProp("="), SVar(0), IVal(0))
      case LT => app(BinIntProp(">="), SVar(0), IVal(0))
      case GE => app(BinIntProp("<"), SVar(0), IVal(0))
      case GT => app(BinIntProp("<="), SVar(0), IVal(0))
      case LE => app(BinIntProp(">"), SVar(0), IVal(0))
    }
    inForall{ q =>
      val p = if (b) ordTrue else ordFalse
      val r = popStack(and(q, p), 1)
      Log.log(s"result: ${clearString(r)}")
      r
    }(d)
  }

  // compare two numbers
  def branchIF_ICMPx(x:Order, d: LogLat.t, b: Boolean): LogLat.t = {
    def filterExp(b: Boolean)(e: l): l = if (b) e else not(e)
    def ord = x match {
      case EQ => "="
      case NE => "="
      case LT => "<"
      case GE => ">="
      case GT => ">"
      case LE => "<="
    }
    inForall { q =>
      val a =
        if (x==NE)
          filterExp(b)(not(app(BinIntProp(ord), SVar(1), SVar(0))  ))
        else
          filterExp(b)(app(BinIntProp(ord), SVar(1), SVar(0)))
      Log.log(s"filter: ${clearString(a)}")
      val c = and(a, q)
      Log.log(s"cut: ${clearString(c)}")
      //      val d = dnf(c)
      //      Log.log(s"normalization: ${clearString(d)}")
      val e = simplify(c)
      //      Log.log(s"filter: ${clearString(e)}")
      val r = simplify(popStack(e, 2))
      r
    }(d)
  }


  def sBranchS(get: node => l, set: node => l => Unit, to: node, to_final: Boolean, st: l)(instr: JumpInstr, thn: Boolean): l = {
    instr match {
      case IF_ICMPx(x, a) => branchIF_ICMPx(x, st, thn)
      case IFx(x, a) => branchIFx(x, st, thn)
      //      case IF_ACMPx(x, a) =>
      case GOTO(a) => st
      //      case IFNULL(a) =>
      //      case IFNONNULL(a) =>
      //      case TABLESWITCH(a) =>
      //      case LOOKUPSWITCH(a) =>
      case _ => st
    }
  }

  // keeps the top element
  def clearStack(d: l): l = {
    val vs = getAtoms(d).filter(_.isInstanceOf[SVar]).map(_.asInstanceOf[SVar])
    if (vs nonEmpty) {
      val n = vs.map(_.n).max
      val d1 = (2 to n).foldLeft(d) { (v, i) => eqForget(SVar(i - 1))(v) }
      def vs2 = vs.map {
        case SVar(m,t) => SVar(m,t) -> app(SVar(m - n,t))
      }.toMap[Val, l]

      simplify(repl(vs2)(d1))
    } else {
      d
    }
  }

  def clearLocals(d: l): l = {
    def isNormalLocal(v:Val):Boolean = v.isInstanceOf[LVar] && !v.asInstanceOf[LVar].start
    val vs = getAtoms(d).filter(isNormalLocal)
    val qq1 = vs.foldLeft(d){ case (d,v) =>
      val r = havoc1(v)(d)
//      Log.logCommon(s"havoc(${v.str},${clearString(d)}):\n${clearString(r)}");
      r
    }
    if(LogLat.leq(LogLat.top, qq1))
      LogLat.top
    else if(LogLat.leq(qq1,LogLat.bot))
      LogLat.bot
    else
      qq1
  }

  def forgetLocals(d: l): l = {
    val vs = getAtoms(d).filter(_.isInstanceOf[LVar]).map(_.asInstanceOf[LVar]).filter(_.n>=0)
    vs.foldLeft(d)((d1,v) => eqForget(v)(d1))
  }

  def trans(get: node => l, set: node => l => Unit, to: node, to_final: Boolean)(instr: Instr): l => l = instr match {
    case xCMPG(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xCMPL(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case a: FCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case a: LCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case a: DCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case DUP_X1 => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case ANEWARRAY(a) => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case INSTANCEOF(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case CHECKCAST(a) => inForall { d =>
      // not implemented
      d
    }
    case xXOR(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case LCMP => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case ACONST_NULL => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case xUSHR(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xOR(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xAND(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case LDC(a) => inForall { d =>
      // not implemented
      a.x match {
        case Some(x: Int) =>
          val d1 = pushStack(d)
          Log.log(s"push state: ${clearString(d1)}")
          val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(x)))
          and(d1, s0eqi)
        case _ => pushStack(d)
      }
    }
    case ATHROW => _ => False
    case MONITORENTER => inForall { d =>
      // not implemented
      popStack(d, 1)
    }
    case MONITOREXIT => inForall { d =>
      // not implemented
      popStack(d, 1)
    }
    case xSHL(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xSHR(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case ARRAYLENGTH => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case x2y(a, b) => inForall { d =>
      // not implemented
      d
    }
    case NEWARRAY(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case GETFIELD(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case xASTORE(t) => inForall { d =>
      // not implemented
      popStack(d, 3)
    }
    case xALOAD(t) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case SIPUSH(i) => inForall { d =>
      val d1 = pushStack(d)
      val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.x.get)))
      and(d1, s0eqi)
    }
    case INVOKESTATIC(c) if c.x.get == ("P6der", "check", "(Z)V") => inForall { d =>
      val mthd = s"(${to.methodInfo.cls}.${to.methodInfo.mthd}:${to.linenr})"
      if (LogLat.leq(d, app(BinIntProp("="), SVar(0), IVal(1)))) {
        Log.log("Check true")
        Log.logCommon("Check true", mthd)
      } else if (LogLat.leq(d, app(BinIntProp("="), app(SVar(0)), app(IVal(0))))) {
        Log.log("Check false")
        Log.logCommon("Check false", mthd)
      } else {
        Log.log("Check inconclusive")
        Log.logCommon("Check inconclusive", mthd)
      }
      popStack(d, 1)
    }
    case INVOKEDYNAMIC(a,b) if b.x.get._2=="makeConcatWithConstants" => st =>
      val typ = Parser.mpar(a.x.get._3)
      st.popStack(typ.args.length).pushStack
    case IINC(a) => inForall { d =>
      val r = repl(Map[Val, l](local(to)(a.x.get._1) -> app(TempVar)))(d)
      Log.log(s"replace state: ${clearString(r)}")
      val q = and(app(BinIntProp("="), app(local(to)(a.x.get._1)), app(BinIntOp("+"), app(TempVar), app(IVal(a.x.get._2)))), r)
      simplify(eqForget(TempVar)(q))
    }
    case xADD(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("+"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xSUB(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("-"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xMUL(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("*"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xDIV(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("*"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xNEG(t) => inForall { d =>
      val r = repl(Map[Val, l](SVar(0) -> app(BinIntOp("-"), SVar(0))))(d)
      r
    }
    case xLOAD(t, a) => inForall { d =>
      val d1 = pushStack(d)
      Log.log(s"push state: ${clearString(d1)}")
      val s0eqi = app(BinIntProp("="), app(SVar(0)), app(local(to)(a.x.get)))
      and(d1, s0eqi)
    }
    case POP => inForall { d =>
      simplify(popStack(d, 1))
    }
    case i: ICONST => inForall {
      d =>
        val d1 = pushStack(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.n)))
        and(d1, s0eqi)
    }
    case i: BIPUSH => inForall {
      d =>
        val d1 = pushStack(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.a.x.get)))
        val r = and(d1, s0eqi)
        r
    }
    case xSTORE(t, n) => inForall {
      d =>
        val d1 = eqForget(local(to)(n.x.get))(d)
        Log.log(s"forget state: ${clearString(d1)}")
        simplify(popStack(d1, 1, Seq(app(local(to)(n.x.get)))))
    }
    case NEW(s) => inForall {
      d =>
        val d1 = pushStack(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(Fun("type", FunType(ClassType, FunType(StringType, PropType))), SVar(0), SVal(s.x.get))
        and(d1, s0eqi)
    }
    case DUP => inForall {
      d =>
        val d1 = pushStack(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(SVar(1)))
        and(d1, s0eqi)
    }
    case PUTFIELD(a) => inForall {
      d =>
        val d1 = and(app(Fun("field", FunType(ClassType, FunType(StringType, FunType(AnyType, PropType)))), app(SVar(0)), SVal(a.x.get._2), SVar(1)), d)
        Log.log(s"before simplify: ${clearString(d1)}")
        simplify(popStack(d1, 2))
    }
    case PUTSTATIC(a) => inForall {
      d =>
        val fld = FieldVar(a.x.get._1,a.x.get._2,IntType)
        d.eqForget(fld).popStack(1,Seq(app(fld))).simplify
    }
    case GETSTATIC(a) => inForall {
      d =>
        val fld = FieldVar(a.x.get._1,a.x.get._2,IntType)
        val eq = BinIntProp("=")
        d.pushStack && eq(SVar(0),fld)
    }
    case xRETURN(t: SimpleType) => inForall {
      d =>
        //        Log.logCommon("before return:\n"++clearString(d))
        simplify(clearLocals(clearStack(d)))
    }
    case RETURN => inForall {
      d => simplify(clearLocals(popStack(clearStack(d), 1)))
    }
    case a => st =>
      Log.logCommon(s"Ignoring unknown instruction: $a")
      st
  }

  def sThrowsS(get: node => l, set: node => l => Unit, to: node, to_final: Boolean)(instr: Instr, ex: Either[String, Set[String]]): l => l = instr match {
    case ATHROW => inForall {
      d => clearStack(d)
    }
    case _ => st => st
  }

  def sAnnotS(get: node => l, set: node => l => Unit, to: node, to_final: Boolean, st: l)(ant: Annot): l = ant match {
    case LocalVar(i,n,t) => st
  }


  def compose(args:Seq[(Int,LVar)], f:l): l => l = inForall{
    x =>
      Log.log(s"compose:\n\td_prev: ${clearString(x)}\n\td_fun: ${clearString(f)}\n")
      val retVar =  RetVar
      val f1 = repl(Map[Val,l](SVar(0) -> and(retVar)))(f)
      def r(x: l): l = {
        args.foldRight(x) {
          case ((i,v),b) => repl(Map[Val,l](v -> and(SVar(i))))(b)
        }
      }
      val q = popStack(and(x,forgetLocals(r(f1))),args.length)
      val w = simplify(repl(Map[Val,l](retVar -> and(SVar(0))))(pushStack(q)))
      Log.log(s"combine result: ${clearString(w)}")
      w
  }
}

class Modular(proj: ProgramCFG) extends ModularLogic {

  override type dl = D.t
  override implicit val L: Lattice[D.t] with WidenOpF[D.t] with NarrowOpF[D.t] = D

  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = {
    if (nd.is_start)
      (ent(nd)(nd.methodInfo.mthdTyp.args, nd.methodInfo.is_static), LogLat.top)
    else
      D.bot
  }

  override def sStart(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = st

  override def sBranch(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: JumpInstr, b: Boolean): dl = {
    def getS(v: node): l = get(v)._1
    def setS(v: node)(d: l): Unit = set(v)((d, LogLat.bot))
    def getR(v: node): l = get(v)._2
    def setR(v: node)(d: l): Unit = set(v)((LogLat.bot,d))

    (sBranchS(getS,setS,to,to_final,d._1)(instr, b), sBranchS(getR,setR,to,to_final,d._2)(instr, b))
  }

  override def sNormal(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(i: Instr): dl = {
    val (ds: LogLat.t, dr: LogLat.t) = st
    Log.log(s"instruction: $i")
    Log.log(s"start summary state: ${clearString(ds)}")
    Log.log(s"start state: ${clearString(dr)}")

    def getS(v: node): l = get(v)._1
    def setS(v: node)(d: l): Unit = set(v)((d, LogLat.bot))
    def getR(v: node): l = get(v)._2
    def setR(v: node)(d: l): Unit = set(v)((LogLat.bot, d))

    val rs = trans(getS,setS,to,to_final)(i)(ds)
    val rr = trans(getR,setR,to,to_final)(i)(dr)
    Log.log(s"end summary state: ${clearString(rs)}")
    Log.log(s"end state: ${clearString(rr)}")
    (rs, rr)

  }


  override def sThrows(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl = {
    def getS(v: node): l = get(v)._1
    def setS(v: node)(d: l): Unit = set(v)((d, LogLat.bot))
    def getR(v: node): l = get(v)._2
    def setR(v: node)(d: l): Unit = set(v)((LogLat.bot, d))

    (sThrowsS(getS,setS,to, to_final)(instr, ex)(st._1), sThrowsS(getR,setR,to, to_final)(instr, ex)(st._2))
  }

  override def sAnnot(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(ant: Annot): dl = {
    def getS(v: node): l = get(v)._1
    def setS(v: node)(d: l): Unit = set(v)((d, LogLat.bot))
    def getR(v: node): l = get(v)._2
    def setR(v: node)(d: l): Unit = set(v)((LogLat.bot, d))

    (sAnnotS(getS,setS,to, to_final,st._1)(ant), sAnnotS(getR,setR,to, to_final,st._2)(ant))
  }

  override def senter(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, d: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = {
    val (cls, mthd, atyp) = instr.a.x.get
    val startn = proj.startVarsFun(cls, mthd, Parser.mpar(atyp))
    val as = args(startn,typ.args,static)
    Log.log(s"senter reachable start: ${clearString(d._2)}")
    val r = as.foldRight(forgetLocals(d._2)) {
      case ((i, x), b) =>
        repl(Map[Val, l](SVar(i) -> and(local(startn)(i))))(b)
    }
    val q = popStack(clearStack(r), 1)
    Log.log(s"senter reachable result: ${clearString(q)}")
    Seq((ent(to)(typ.args, static), q))
  }


  //  def intros: ds => (Seq[(Val,String)],ds) = {
  //    case Forall(x,t,p) =>
  //      val (xs, r) = intros(p)
  //      ((x,t) +: xs, r)
  //    case y => (Seq.empty, y)
  //  }


  override def scombine(get: Node => dl, set: Node => dl => Unit, to: Node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = {
    if (typ.ret == VoidT) {
      val n = typ.args.length
      ((caller._1.popStack(n) && st._1.popStack()).simplify, (caller._2.popStack(n) && st._2.popStack()).simplify)
    } else {
      val (cls, mthd, atyp) = instr.a.x.get
      val startn = proj.startVarsFun(cls, mthd, Parser.mpar(atyp))
      (compose(args(startn,typ.args,static),st._1)(caller._1).simplify, compose(args(startn,typ.args,static),st._1)(caller._2).simplify)
    }
  }
}

class ModularSummary(project: ProgramCFG) extends ModularLogic {
  override type dl = LogLat.t
  override implicit val L: Lattice[LogLat.t] with WidenOpF[LogLat.t] with NarrowOpF[LogLat.t] = LogLat

  override def sNode(get: Node => dl, set: Node => dl => Unit)(nd: Node): dl = {
    if (nd.is_start)
      ent(nd)(nd.methodInfo.mthdTyp.args, nd.methodInfo.is_static)
    else
      LogLat.bot
  }

  override def sStart(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(locals: Int, stack: Int, access: Int): dl = st
  override def sBranch(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(instr: JumpInstr, thn: Boolean): dl = sBranchS(get,set,to,to_final,st)(instr,thn)
  override def sNormal(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(instr: Instr): dl = trans(get,set,to,to_final)(instr)(st)
  override def sThrows(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(instr: Instr, ex: Either[String, Set[String]]): dl = sThrowsS(get,set,to,to_final)(instr,ex)(st)
  override def sAnnot(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(ant: Annot): dl = sAnnotS(get,set,to,to_final,st)(ant)

  override def senter(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean): Seq[LogLat.t] = {
    Seq(ent(to)(typ.args, static))
  }

  override def scombine(get: node => dl, set: node => dl => Unit, to: node, to_final: Boolean, st: dl)(instr: MethodInstr, typ: MethodType, static: Boolean, caller: dl): dl = {
    //  override val scombine: PartialFunction[(MethodInstr, MethodType, Boolean, dl), ed] = {
    if (typ.ret == VoidT) {
      def r(x: l): Int = {
        x match {
          case ForallL(t, y) => 1 + r(y)
          case y => 0
        }
      }

      popStack(caller, r(st))
    } else {
      val (cls, mthd, atyp) = instr.a.x.get
      val startn = project.startVarsFun(cls, mthd, Parser.mpar(atyp))
      compose(args(startn,typ.args,static),st)(caller).simplify
    }
  }
}
