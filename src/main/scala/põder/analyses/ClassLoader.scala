package põder.analyses

import põder.framework.{EqualOpF, JoinOpF, JsonOpF, Lattice, MapAnalysis, NarrowOpF, SimpleCfgBcSem, TopOpF, WidenOpF}
import põder.framework.cfg._
import põder.util.Util.Cell
import põder.util.json.JsonF
import põder.util.typ.{MethodType, VoidT}

object ClassLoader {
  type d = Option[Set[String]]
  object CL extends  Lattice[d] with WidenOpF[d] with NarrowOpF[d]{
    override def widen(oldv: Option[Set[String]], newv: Option[Set[String]]): Option[Set[String]] = join(oldv,newv)
    override def bot: Option[Set[String]] = None
    override def top: Option[Set[String]] = Some(Set.empty)
    override def leq(x: Option[Set[String]], y: Option[Set[String]]): Boolean = {
      if (x==bot) true
      else if (y==top) true
      else if (y==bot) false
      else y.get.subsetOf(x.get)
    }

    override def toJson[jv](x: Option[Set[String]])(implicit j: JsonF[jv]): jv = {
      x match {
        case Some(value) =>  j.Obj("loaded classes" -> j.Arr(value.map(j.Str)))
        case None => j.Str("unreachable")
      }
    }

    override def join(x: Option[Set[String]], y: Option[Set[String]]): Option[Set[String]] = (x,y) match {
      case (None, x) => x
      case (x,None) => x
      case (Some(a), Some(b)) => Some(a intersect b)
    }
  }
}

class ClassLoader(project: ProgramCFG, val c: MapAnalysis[Node])  extends MapAnalysis[Node]{
  import ClassLoader._
  import c.{L => C}

  override type key = c.key
  override val Key: JsonOpF[c.key] = c.Key
  override val startKeys: Seq[c.key] = c.startKeys

  override type dl = (d, c.dl)
  override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = new Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] {
    override def widen(oldv: dl, newv: dl): dl =  (CL.widen(oldv._1,newv._1), C.widen(oldv._2,newv._2))
    override def bot: dl = (CL.bot, C.bot)
    override def top: dl = (CL.top, C.top)

    override def leq(x: dl, y: dl): Boolean =
      CL.leq(x._1,y._1) && C.leq(x._2,y._2)

    override def toJson[jv](x: dl)(implicit j: JsonF[jv]): jv =
      j.Arr(CL.toJson(x._1),C.toJson(x._2))

    override def join(x: dl, y: dl): dl =
      (CL.join(x._1,y._1),C.join(x._2,y._2))
  }

  override def sNode(get: Node => key => dl, set: Node => key => dl => Unit)(k:key,nd: Node): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((CL.bot,d))
    val cn = c.sNode(get2,set2)(k,nd)
    if (project.mainClass==nd.methodInfo.cls&&nd.is_start&&project.startMethods.contains((nd.methodInfo.mthd,nd.methodInfo.mthdTyp))) (CL.top,cn) else (CL.bot,cn)
  }

  override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    c.isBuiltinFunction(instr,typ,static)

  def loadClassTF(get: Node => key => dl, set: Node => key => dl => Unit, from:Node, to:Node,cls: String, st: key => dl)(k:key): dl = {
    if (st(k)._1.isEmpty)
      return st(k)

    val s = st(k)._1
    if (s contains cls){
      st(k)
    } else {
      def get2(n:Node)(k:key): c.dl = get(n)(k)._2
      def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((Some(s.get + cls),d))
      val mthType = MethodType(List(),VoidT)
      val instr = INVOKESPECIAL(Cell((cls,"<clinit>","()V")))
      val es = c.senter(get2,set2,from,from_start = false,to,to_final = false, k => st(k)._2)(k,instr,mthType,static = true)
      val snode = project.startVarsFun(cls,"<clinit>",MethodType(List(),VoidT))
      val rnode = project.returnVarsFun(cls,"<clinit>",MethodType(List(),VoidT))
      es.foreach(set2(snode)(k))
      val newc = c.scombine(get2,set2,from, from_start = false, to,to_final = false, k=>get(rnode)(k)._2)(k,instr,mthType,static = true, k=>st(k)._2)
      (get(rnode)(k)._1, newc)
    }
  }

  def makeBot(st: dl): dl = st match {
    case (s,d) if C.leq(d,C.bot) => (s,C.bot)
    case (s,d) => (s,d)
  }

  override def sStart(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean, to: Node, to_final: Boolean, st: key => dl)(k:key, locals: Int, stack: Int, access: Int): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val cs = c.sStart(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,locals,stack,access)
    def nst(k:key):dl = (st(k)._1,cs)
    makeBot(loadClassTF(get,set,fr,to,to.methodInfo.cls,nst)(k))
  }


  override def senter(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean, to: Node, to_final: Boolean, st: key => dl)(k:key, instr: MethodInstr, typ: MethodType, static: Boolean): Seq[dl] = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val ce = c.senter(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,typ,static)
    def nst(ce:c.dl):dl = (st(k)._1,ce)
    ce.map(ce => makeBot(nst(ce)))
  }

  override def scombine(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: key => dl)(k:key, instr: MethodInstr, typ: MethodType, static: Boolean, caller: key => dl): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val cc = c.scombine(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,typ,static,k=>caller(k)._2)
    makeBot((st(k)._1,cc))
  }

  override def sBranch(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: key => dl)(k:key, instr: JumpInstr, thn: Boolean): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val cb = c.sBranch(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,thn)
    makeBot((st(k)._1,cb))
  }

  override def sNormal(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: key => dl)(k:key, instr: Instr): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val cn = c.sNormal(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr)
    instr match {
      case GETSTATIC(a) =>
        makeBot(loadClassTF(get,set,fr,to,a.x.get._1,k=>(st(k)._1,cn))(k))
      case _ =>
        makeBot((st(k)._1,cn))
    }
  }

  override def sThrows(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: key => dl)(k:key, instr: Instr, ex: Either[String, Set[String]]): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val ct = c.sThrows(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,ex)
    makeBot((st(k)._1,ct))
  }

  override def sAnnot(get: Node => key => dl, set: Node => key => dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: key => dl)(k:key, ant: Annot): dl = {
    def get2(n:Node)(k:key): c.dl = get(n)(k)._2
    def set2(n:Node)(k:key)(d: c.dl): Unit = set(n)(k)((st(k)._1,d))
    val ca = c.sAnnot(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,ant)
    makeBot((st(k)._1,ca))
  }
}
