package põder.dom

import põder.dom.LogicHelper._
import põder.framework.{Lattice, NarrowOpF, WidenOpF}
import põder.util.SMTHelper
import põder.util.json.JsonF
import smtlib.trees.Terms
import smtlib.trees.Terms.{FunctionApplication, Identifier, QualifiedIdentifier, SNumeral, SString, SSymbol, Sort, SortedVar, Term}

import scala.math.BigInt

object LogLat extends Lattice[Logic] with WidenOpF[Logic] with NarrowOpF[Logic] {
  import Logic._

  override def bot: Logic = False
  override def top: Logic = True

  override def toJson[jv](x: Logic)(implicit j: JsonF[jv]): jv = x match {
    case True   => j.Str("True")
    case False   => j.Str("False")
    case And(xs@_*) => j.Obj(("And",j.Arr(xs.map(toJson(_)))))
    case Or(xs@_*)  => j.Obj(("Or",j.Arr(xs.map(toJson(_)))))
    case Not(q)  => j.Obj(("Not",toJson(q)))
    case App(f, xs@_*) => j.fromVal(clearString(App(f, xs:_*)))
    case ForallL(t,q) => j.Obj(("Forall "+t.str,  toJson(q)))
    case ExistsL(t,q) => j.Obj(("Exists "+t.str,  toJson(q)))
  }


  override def join(x: Logic, y: Logic): Logic = {
    if (x == top || y == top) top
    else if (x == bot) y
    else if (y == bot) x
    else if (x == y) x
    else (x,y) match {
      case (ForallL(t1,x1), ForallL(t2,x2)) if t1==t2 =>
        ForallL(t1, join(x1,x2))
      case _ => Logic.or(x,y) // simplify here?
    }
  }


  override def leq(x: Logic, y: Logic): Boolean = {
    if (y==top || x==bot || x==y) true
    else {
      x.query(y).contains(true)
    }
  }

  //  override def equal(x: Logic, y: Logic): Boolean = {
  //    if (x==y) true
  //    else {
  //      val q = inForall2{ case (a, b) => or(and(not(b), a),and(not(a), b)) }(x,y)
  //      !sat(q)
  //    }
  //  }


  case class MVar(s:String) extends Var(IntType) {
    override def str: String = s"${s}_m"
  }

  private val testTemps: Seq[(Seq[Var], Seq[Var], Logic)] =
    Seq(
      (Seq(MVar("x")),Seq(MVar("a"),MVar("b")),and(app(BinIntProp("<="), MVar("x"), MVar("b")),app(BinIntProp("<="), MVar("a"), MVar("x")),app(BinIntProp("<="), MVar("a"), MVar("b")))),
      (Seq(MVar("x"), MVar("y")),Seq(MVar("a")),app(BinIntProp("<="), MVar("x"), app(BinIntOp("+"), MVar("y"), MVar("a")))))
  //      (Seq(MVar("x"),MVar("y")),Seq(),app(BinPropOp("<="), MVar("x"), MVar("y"))))

  override def widen(oldv: Logic, newv: Logic): Logic = {
    if (oldv == bot)
      newv
    else {
      if (oldv==newv)
        oldv
      else
        top
      //      val nd = generalize(testTemps, newv)
      //      nd
    }
  }
}
