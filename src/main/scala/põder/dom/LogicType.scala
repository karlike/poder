package põder.dom

// Every variable and value has a tyoe
sealed trait Type {
  val prec: Int
  val assoc: Int
  def toSimpleString:String
}

// We can do forall and exists over SimpTypes
sealed trait SimpType extends Type

case object IntType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "Int"
}
case object DoubleType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "Double"
}
case object StringType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "String"
}
case object ClassType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "Int"
}
case object AnyType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "Int"
}
case object PropType extends SimpType {
  val prec  = 5
  val assoc = 0
  override def toSimpleString: String = "Bool"
}
case class FunType(fr: Type, to:Type) extends Type {
  val prec = 4
  val assoc = 1
  lazy val frs: String =
    if (fr.prec < prec || fr.prec == prec) s"(${fr.toSimpleString})" else fr.toSimpleString
  lazy val tos: String =
    if (to.prec <= prec) s"(${to.toSimpleString})" else to.toSimpleString
  override def toSimpleString: String = {
    def gather(x:Type):List[String] = {
      x match {
        case FunType(f, t) => f.toSimpleString +: gather(t)
        case _ => List(x.toSimpleString)
      }
    }
    gather(this).mkString("("," ",")")
  }
}

trait Val extends Serializable
{
  val typ: Type
  def str: String
  val sym: Boolean = false
  val refl: Boolean = false
}

case class Fun(override val str:String, override val typ: Type) extends Val {}

class BinIntOp(s:String)
  extends Fun(s,FunType(IntType,FunType(IntType, IntType)))
{
  override val sym: Boolean = false
  override val refl: Boolean = false
}
object BinIntOp {
  def apply(s: String): BinIntOp = new BinIntOp(s)
  def unapply(s: BinIntOp): Option[String] = Some(s.str)
}

class BinIntProp(s:String)
  extends Fun(s, FunType(IntType,FunType(IntType, PropType))) {
  val reflOps: Seq[String] = Seq("=", "<=", ">=")
  override val refl: Boolean = reflOps contains s
}

object BinIntProp {
  def apply(s: String): BinIntProp = new BinIntProp(s)
  def unapply(s: BinIntProp): Option[String] = Some(s.str)
}

class IProp(s:String)
  extends Fun(s,FunType(IntType, PropType))

object IProp {
  def apply(s: String): IProp = new IProp(s)
  def unapply(s: IProp): Option[String] = Some(s.str)
}

class SProp(s:String)
  extends Fun(s,FunType(StringType, PropType))

object SProp {
  def apply(s: String): SProp = new SProp(s)
  def unapply(s: SProp): Option[String] = Some(s.str)
}


abstract class Var(override val typ:SimpType) extends Val
object  Var {
  def unapply(s: Var): Option[Type] = Some(s.typ)
}
case class SVar(n:Int, override val typ:SimpType = IntType) extends Var(typ) {
  override def str: String = "s"+n.toString
}
case class LVar(n:Int, start: Boolean = false, override val typ:SimpType = IntType) extends Var(typ) {
  var alias: String = ""

  override def str: String =
    (if (alias.nonEmpty) alias else "l" + n.toString) ++ (if (start) "'" else "")
}

case class SVal(s:String) extends Val {
  override def str: String = "\"" ++ s.toString ++ "\""
  override val typ: Type = StringType
}

case class IVal(n:Int) extends Val {
  override def str: String = n.toString
  override val typ: Type = IntType
}

case class DVal(n:Double) extends Val {
  override def str: String = n.toString
  override val typ: Type = DoubleType
}

case class XIVar(v:Val) extends Val {
  override def str: String = 'x' + v.toString
  override val typ: Type = v.typ
}

case object TempVar extends Val {
  override def str: String = "temp"
  override val typ: Type = IntType
}

case object RetVar extends Val {
  override def str: String = "Ret"
  override val typ: Type = IntType
}


sealed trait Logic  {
  def toSimpleString: String
}
case object True extends Logic {
  override def toSimpleString = "True"
}
case object False extends Logic {
  override def toSimpleString = "False"
}
case class And(xs: Logic*) extends Logic {
  override def toSimpleString: String =
    s"And(${xs.map(_.toSimpleString).mkString(", ")})"
}
case class Or(xs: Logic*) extends Logic {
  override def toSimpleString: String =
    s"Or(${xs.map(_.toSimpleString).mkString(", ")})"
}
case class Not(x: Logic) extends Logic {
  override def toSimpleString: String =
    s"Not(${x.toSimpleString})"
}
case class App(f: Val, xs: Logic*) extends Logic {
  override def toSimpleString: String =
    if (xs.isEmpty)
      f.str
    else
      s"${f.str}(${xs.map(_.toSimpleString).mkString(", ")})"
}
case class ForallL(t: Val, x: Logic) extends Logic {
  override def toSimpleString =
    s"(forall ${t.str}: ${t.typ.toSimpleString}. ${x.toSimpleString})"
}
case class ExistsL(t: Val, x: Logic) extends Logic {
  override def toSimpleString =
    s"(exists ${t.str}: ${t.typ.toSimpleString}. ${x.toSimpleString})"
}

object Logic {
  def fromBool(b:Boolean): Logic =
    if (b) True else False

  def convert: Any => Logic = {
    case x:Val    => App(x)
    case x:Int    => App(IVal(x))
    case x:Double => App(DVal(x))
    case x:String => App(SVal(x))
    case x:Logic  => x
    case _ => throw new IllegalArgumentException
  }

  def andE(xs:Logic*): Logic = {
    lazy val ys = xs.filter(_ != True).distinct
    if (ys.contains(False))
      False
    else {
      if (ys.isEmpty) True
      else if (ys.size == 1) ys.head
      else {
        And(ys.flatMap{
          case And(ws@_*) => ws
          case z => Seq(z)
        }:_*)
      }
    }
  }

  def and(xs:Any*): Logic = {
    andE(xs.map(convert):_*)
  }

  def orE(xs:Logic*): Logic = {
    lazy val ys = xs.filter(!_.isInstanceOf[False.type]).distinct
    if (ys.contains(True))
      True
    else {
      if (ys.isEmpty) False
      else if (ys.size == 1) ys.head
      else {
        Or(ys.flatMap{
          case Or(ws@_*) => ws
          case z => Seq(z)
        }:_*)
      }
    }
  }

  def or(xs:Any*): Logic = {
    orE(xs.map(convert):_*)
  }

  def not(x:Logic): Logic = x match {
    case True  => False
    case False  => True
    case Not(y) => y
    case y      => Not(y)
  }

  def appE(f:Val, xs:Logic*): Logic = {
    if (xs.length == 2 && f.sym) {
      val q = xs.sortBy(_.toSimpleString)
      if (q.length == 1) {
        if (f.refl) True else {
          App(f, q ++ q: _*)
        }
      } else App(f, q: _*)
    } else App(f, xs: _*)
  }


  def app(f:Val, xs:Any*): Logic = {
    appE(f,xs.map(convert):_*)
  }

  def iop(f:String, x:Any, y:Any): Logic = {
    app(BinIntOp(f), convert(x), convert(y))
  }

  def cop(f:String, x:Any, y:Any): Logic = {
    app(BinIntProp(f), convert(x), convert(y))
  }

  def eqL(x:Any, y:Any): Logic = {
    cop("=", x, y)
  }

  def forall(x:Val, y:Logic): Logic = {
    ForallL(x, y)
  }

  def exists(x:Val, y:Logic): Logic = {
    y match {
      case ExistsL(a, b) if x==a =>
        y
      case _ =>
        ExistsL(x, y)
    }
  }

  def svar(n:Int): Logic = App(SVar(n))

  def lvar(n:Int, start: Boolean=false, alias: String = ""): Logic = {
    val v = LVar(n, start)
    v.alias = alias
    App(v)
  }

  def ival(n:Int): Logic = App(IVal(n))

  def dval(n:Double): Logic = App(DVal(n))

  def inForall(f: Logic => Logic): Logic => Logic = {
    case ForallL(t, x) =>
      val q = inForall(f)(x)
      if (q == False)
        q
      else
        forall(t, q)
    case y => f(y)
  }

  def inExists(f: Logic => Logic): Logic => Logic = {
    case ExistsL(t, x) =>
      val q = inExists(f)(x)
      if (q == False)
        q
      else
        ExistsL(t, q)
    case y => f(y)
  }

  def inForall2(f: (Logic, Logic) => Logic)(a: Logic, b: Logic): Logic = (a,b) match {
    case (ForallL(t1, x1), ForallL(t2, x2)) if t1 == t2 =>
      val q = inForall2(f)(x1, x2)
      if (q == False)
        q
      else
        forall(t1, q)
    case (False, ForallL(t2, x2)) =>
      val q = inForall2(f)(False, x2)
      if (q == False)
        q
      else
        forall(t2, q)
    case (ForallL(t1, x1), False) =>
      val q = inForall2(f)(x1, False)
      if (q == False)
        q
      else
        forall(t1, q)
    case (x, y) => f(x, y)
  }

  def insertForall(f: Logic): Logic = {
    def detectIntVars: Val => Boolean = {
      case Var(_) => true
      case _ => false
    }

    val vars = Logic.getAtoms(f).filter(detectIntVars)
    vars.foldLeft(f) { case (b, v) => ForallL(v, b) }
  }

  def insertExists(f: Logic): Logic = {
    def detectIntVars: Val => Boolean = {
      case Var(_) => true
      case _ => false
    }

    val vars = Logic.getAtoms(f).filter(detectIntVars)
    vars.foldLeft(f) { case (b, v) => ExistsL(v, b) }
  }

  def appExp(f:Logic, xs: Seq[Logic]): Logic = {
    if (xs.isEmpty) f
    else {
      f match {
        case True => True
        case False => False
        case And(zs@_*) => and(zs.map(appExp(_, xs)):_*)
        case Or(zs@_*) => or(zs.map(appExp(_, xs)) :_*)
        case Not(x) => not(appExp(x, xs))
        case App(d, zs@_*) => app(d, zs ++ xs:_*)
        case ForallL(t, x) =>
          appExp(repl(Map(t -> xs.head))(x), xs.tail)
        case ExistsL(t, x) =>
          ExistsL(t, appExp(x, xs))
      }
    }
  }

  def repl(f:Map[Val,Logic]): Logic => Logic = {
    case True => True
    case False => False
    case Not(x)  => not(repl(f)(x))
    case And(xs@_*) => and(xs.map(repl(f)) :_*)
    case Or(xs@_*)  => or (xs.map(repl(f)) :_*)
    case App(x,xs@_*) if !f.contains(x) => app(x,xs.map(repl(f)):_*)
    case App(x,xs@_*)   => appExp(f(x),xs)
    case ForallL(t,x) => forall(t, repl(f-t)(x))
    case ExistsL(t,x) => exists(t, repl(f-t)(x))
  }

  def getAtoms(e:Logic): Set[Val] = {
    e match {
      case True => Set.empty
      case False => Set.empty
      case And(xs@_*) => xs.toSet.flatMap(getAtoms)
      case Or(xs@_*) => xs.toSet.flatMap(getAtoms)
      case Not(x) => getAtoms(x)
      case App(f, xs@_*) => Set(f) ++ xs.toSet.flatMap(getAtoms)
      case ForallL(t, x) => getAtoms(x) - t
      case ExistsL(t, x) => getAtoms(x) - t
    }
  }

  def proj(p:Val => Boolean): Logic => Logic = {
    case True   => True
    case False  => False
    case And(xs@_*) => and(xs.map(proj(p)) :_*)
    case Or(xs@_*)  => or(xs.map(proj(p)) :_*)
    case Not(y)  => not(proj(p)(y))
    case App(f, xs@_*) if p(f) => app(f, xs.map(proj(p)):_*)
    case App(f, xs@_*) => True
    case ForallL(t, q) => ForallL(t, proj(x => x==t || p(x))(q))
    case ExistsL(t, q) => ExistsL(t, proj(x => x==t || p(x))(q))
  }

  def forget(x:Val): Logic => Logic =
    forgetE(x, o = true)

  def forgetE(x:Val, o:Boolean): Logic => Logic = {
    case True  => True
    case False => False
    case And(xs@_*) =>
      and(xs.map(forgetE(x,o)):_*)
    case Or(xs@_*) =>
      or(xs.map(forgetE(x,o)):_*)
    case Not(y)  =>
      not(forgetE(x,!o)(y))
    case App(f, xs@_*) =>
      if (f==x || xs.flatMap(getAtoms).contains(x))
        fromBool(o)
      else
        app(f, xs:_*)
    case ForallL(t, y) =>
      if (x==t)
        ForallL(t, y)
      else
        ForallL(t, forgetE(x,o)(y))
    case ExistsL(t, y) =>
      if (x==t)
        ExistsL(t, y)
      else
        ExistsL(t, forgetE(x,o)(y))
  }
}



