package põder.dom

import põder.dom.Logic.{and, app, exists, forall, forget, getAtoms, inExists, inForall, insertForall, not, or, repl}
import põder.log.Log
import põder.util.SMTHelper
import smtlib.trees.Terms
import smtlib.trees.Terms.{FunctionApplication, Identifier, QualifiedIdentifier, SNumeral, SString, SSymbol, Sort, SortedVar, Term}

import scala.math.BigInt

object  LogicHelper {

  val maxLevel: Int = 12

  val compOper: Set[String] = Set("=", "<", "<=", ">", ">=")
  def getLevel(x:Val): Int = x.str match {
    case o if compOper.contains(o) => 6
    case "+" => 4
    case "-" => 4
    case "/" => 2
    case "*" => 2
    case _   => 0
  }
  def getLevel(x:Logic): Option[Int] = x match {
    case True  => Some(0)
    case False => Some(0)
    case And(xs@_*) if xs.size==2 => Some(8)
    case And(xs@_*) => Some(1)
    case Or(xs@_*) if xs.size==2 => Some(10)
    case Or(xs@_*) => Some(1)
    case Not(_) => Some(1)
    case App(f, xs@_*) => Some(getLevel(f))
    case ForallL(t, _) => Some(12)
    case ExistsL(t, _) => Some(12)
    case _ => None
  }

  def par(b:Boolean)(s: String): String =
    if(b) s"($s)" else s

  def clearString(i:Int): Logic => String = {
    case True  => "true"
    case False => "false"
    case And(xs@_*) if xs.size==2 =>
      val v = clearString(8)(xs.head)
      val p = clearString(8+1)(xs.tail.head)
      par(i<=8)(s"$v ∧ $p")
    case And(xs@_*) => s"And(${xs.map(clearString(maxLevel)).mkString(", ")})"
    case Or(xs@_*) if xs.size==2 =>
      val v = clearString(10)(xs.head)
      val p = clearString(10+1)(xs.tail.head)
      par(i<=10)(s"$v ∨ $p")
    case Or(xs@_*) => s"Or(${xs.map(clearString(maxLevel)).mkString(", ")})"
    case Not(x) => s"Not(${clearString(maxLevel)(x)})"
    case App(f) => f.str
    case App(f, xs@_*) => (getLevel(f), xs.size) match {
      case (l,2) =>
        val v = clearString(l)(xs.head)
        val p = clearString(l+1)(xs.tail.head)
        par(i<=l)(s"$v ${f.str} $p")
      case _ =>
        s"${f.str}(${xs.map(clearString(maxLevel)).mkString(", ")})"
    }
    case ForallL(t, x) => s"Forall ${t.str}:${t.typ.toSimpleString}. ${clearString(12)(x)}"
    case ExistsL(t, x) => s"Exists ${t.str}:${t.typ.toSimpleString}. ${clearString(12)(x)}"
  }

  def clearString(d: Logic): String = clearString(maxLevel)(d)


  def satCheck(q:Logic):Option[Boolean] = {
    def detectFunVars(v: Val): Boolean = {
      !(List("+","-","*","/","=","!=","<=",">=","<",">") contains  v.str) && (v.typ match {
        case FunType(_,_) => true
        case _ => false
      } )
    }

    @scala.annotation.tailrec
    def varToType(q:Type, a:Seq[String] = Seq()): (Seq[String], String) = {
      q match {
        case FunType(fr, to) => varToType(to, fr.toSimpleString +: a)
        case x => (a.reverse, x.toSimpleString)
      }
    }

    val vars = Logic.getAtoms(q).filter(detectFunVars)

    SMTHelper.reset()
    val b1 = SMTHelper.push()
    val p = tr(q)
    for (v <- vars) {
      val (as,r) = varToType(v.typ)
      SMTHelper.declareFun(v.str, as, r)
    }
    val b2 = SMTHelper.assert(p)
    val r = SMTHelper.sat()
    SMTHelper.pop()
    r
  }

  def sat(q:Logic):Boolean = {
    satCheck(q).contains(true)
  }

  def unsat(q:Logic):Boolean = {
    satCheck(q).contains(false)
  }

  def tr(e:Logic): Term = {
    def q(s:Val) =  QualifiedIdentifier(Identifier(SSymbol(s.str)))

    def mkFa(n:String, typ:String, x:Term): Term =
      Terms.Forall(SortedVar(SSymbol(n),Sort(Identifier(SSymbol(typ)))), Seq(), x)

    def mkEx(n:String, typ:String, x:Term): Term =
      Terms.Exists(SortedVar(SSymbol(n),Sort(Identifier(SSymbol(typ)))), Seq(), x)

    e match {
      case True => smtlib.theories.Core.True()
      case False => smtlib.theories.Core.False()
      case And(xs@_*) =>
        if (xs.isEmpty) smtlib.theories.Core.True()
        else if (xs.size == 1) tr(xs.head)
        else {
          xs.tail.foldLeft(tr(xs.head)) {
            case (y, x) => smtlib.theories.Core.And(tr(x),y)
          }
        }
      case Or(xs@_*) =>
        if (xs.isEmpty) smtlib.theories.Core.False()
        else if (xs.size == 1) tr(xs.head)
        else {
          xs.tail.foldLeft(tr(xs.head)) {
            case (y, x) => smtlib.theories.Core.Or(tr(x),y)
          }
        }
      case Not(x) => smtlib.theories.Core.Not(tr(x))
      case App(IVal(n)) => smtlib.theories.Ints.NumeralLit(BigInt(n))
      case App(DVal(n)) => smtlib.theories.Reals.DecimalLit(BigDecimal(n))
      case App(SVal(s)) => SString(s)
      case App(f) => q(f)
      case App(BinIntOp("="), x, y) => smtlib.theories.Core.Equals(tr(x),tr(y))
      case App(BinIntOp("+"),x, y) => smtlib.theories.Ints.Add(tr(x),tr(y))
      case App(BinIntOp("-"),x, y) => smtlib.theories.Ints.Sub(tr(x),tr(y))
      case App(BinIntOp("*"),x, y) => smtlib.theories.Ints.Mul(tr(x),tr(y))
      case App(BinIntOp("/"),x, y) => smtlib.theories.Ints.Div(tr(x),tr(y))
      case App(f, xs@_*) => FunctionApplication(q(f),xs.map(tr))
      case ForallL(t, x) => mkFa(t.str, t.typ.toSimpleString, tr(x))
      case ExistsL(t, x) => mkEx(t.str, t.typ.toSimpleString, tr(x))
    }
  }

  def filterSat: Logic => Logic = {
    case True => True
    case False => False
    case t@And(xs@_*) =>
      if (sat(t)) t else False
    case Or(xs@_*) =>
      def g(x: Logic): Logic = {
        if (sat(x))
          x
        else {
          False
        }
      }
      or(xs.map(g):_*)
    case x => x
  }


  def optForall(d: Logic): Logic = d match {
    case ForallL(t,x) if Logic.getAtoms(x).contains(t) => ForallL(t,optForall(x))
    case ForallL(t,x) => optForall(x)
    case y => y
  }

  def decBool(b:Boolean): Logic =
    if(b) True else False

  def simplify(l: Logic): Logic = {
    def dealWithApp: App => Logic = {
      case App(f, xs@_*) =>
        App(f, xs.map(simplify): _*) match {
          case App(_, x, y) if f.refl && x == y => True
          case App(BinIntProp("="), App(IVal(y)), App(IVal(z))) => decBool(y == z)
          case App(BinIntProp(">="), App(IVal(y)), App(IVal(z))) => decBool(y >= z)
          case App(BinIntProp(">"), App(IVal(y)), App(IVal(z))) => decBool(y > z)
          case App(BinIntProp("<="), App(IVal(y)), App(IVal(z))) => decBool(y <= z)
          case App(BinIntProp("<"), App(IVal(y)), App(IVal(z))) => decBool(y < z)
          case App(BinIntOp("+"), App(IVal(y)), App(IVal(z))) => App(IVal(y + z))
          case App(BinIntOp("+"), x, App(IVal(0))) => x
          case App(BinIntOp("+"), App(IVal(0)), x) => x
          case App(BinIntOp("-"), App(IVal(y)), App(IVal(z))) => App(IVal(y - z))
          case App(BinIntOp("-"), x, App(IVal(0))) => x
          case App(BinIntOp("*"), App(IVal(y)), App(IVal(z))) => App(IVal(y * z))
          case App(BinIntOp("*"), x, App(IVal(0))) => App(IVal(0))
          case App(BinIntOp("*"), x, App(IVal(1))) => x
          case App(BinIntOp("*"), App(IVal(0)), x) => App(IVal(0))
          case App(BinIntOp("*"), App(IVal(1)), x) => x
          case App(BinIntOp("/"), App(IVal(y)), App(IVal(z))) => App(IVal(y / z))
          case App(BinIntOp("/"), x, App(IVal(1))) => x
          case x => x
        }
    }
    //    Log.log(s"simplify(${l.toSimpleString})")
    l match {
      case True => True
      case False => False
      case And(xs@_*) => simplify_comm(simplify_contra(and(xs.map(simplify): _*)))
      case Or(xs@_*) => simplify_comm(or(xs.map(simplify): _*))
      case Not(y) => not(simplify(y))
      case App(f, xs@_*) => dealWithApp(App(f, xs.map(simplify): _*))
      case ForallL(t, z) => forall(t, simplify(z))
      case ExistsL(t, z) => exists(t, simplify(z))
    }
  }
  def simplify_contra: Logic => Logic =  {
    case And(xs@_*) =>
      if (xs.exists(q => xs.contains(Not(q))))
        False
      else
        And(xs:_*)
    case Or(xs@_*) =>
      if (xs.exists(q => xs.contains(Not(q))))
        True
      else
        Or(xs:_*)
    case x => x
  }

  /*???*/
  def simplify_comm: Logic => Logic =  {
    case Or(xs@_*) =>
      var s = Set.empty[Logic]
      xs.foreach {
        case And(ys@_*) => s = s.intersect(ys.toSet)
        case z => s = s.intersect(Set(z))
      }
      if (s.isEmpty) Or(xs:_*) else {
        def f: Logic => Logic = {
          case And(ys@_*) => and((ys.toSet -- s).toSeq :_*)
          case z => if (Set(z) == s) True else z
        }
        and(and(s.toSeq:_*),or(xs.map(f):_*))
      }
    case And(xs@_*) =>
      var s = Set.empty[Logic]
      xs.foreach {
        case Or(ys@_*) => s = s.intersect(ys.toSet)
        case z => s = s.intersect(Set(z))
      }
      if (s.isEmpty) And(xs:_*) else {
        def f: Logic => Logic = {
          case Or(ys@_*) => or((ys.toSet -- s).toSeq :_*)
          case z => if (Set(z) == s) False else z
        }
        or(or(s.toSeq:_*),and(xs.map(f).toSeq:_*))
      }
    case x => x
  }


  def mul[A,T](x:Seq[A],y:Seq[T]): Seq[Seq[(A,T)]] = {
    if (x.isEmpty) {
      Seq(Seq.empty[(A,T)])
    } else {
      val qs = mul(x.tail, y)
      y.flatMap(t => qs.map((x.head, t) +: _))
    }
  }

  def gen(vars:Seq[Var], l:Logic, d:Logic): Seq[Logic] = {
    val allvars = getAtoms(d).filter(_.isInstanceOf[Var]).toSeq
    //    printf("allvars: %s\n", allvars.map(_.str).mkString(", "))
    val xs = mul(vars,allvars.map(app(_)))
    //    printf("raw: %s\n", xs.map(x => x.map(x => x._1.str ++ " -> " ++ clearString(x._2)).mkString(", ")).mkString("; "))
    val r = xs.map{ ys => repl(ys.toMap)(l)}
    //    printf("gen: %s\n", r.map(clearString).mkString(", "))
    r
  }

  def testCandidate(l:Logic)(p: (Seq[Var], Logic)): Logic = {
    val q = p._1
    val valmap = q.groupBy(_.str)
    val d = p._2
    SMTHelper.push()
    q.foreach(v => SMTHelper.declareConst(v.str, v.typ.toSimpleString))
    val u = inForall(x => or(d,not(x)))(insertForall(l))
    SMTHelper.assert(tr(u)) //
    if (SMTHelper.sat().contains(true)){
      val vs = SMTHelper.getAssignment(q.map(_.str))
      SMTHelper.pop()
      vs.foldLeft(d){
        case (k,(s,t:SNumeral)) =>
          val iv = t.value.bigInteger.intValue()
          repl(Map(valmap(s).head -> App(IVal(iv))))(k)
        case _ => True
      }
    } else {
      True
    }
  }


  def generalize(temps:Seq[(Seq[Var], Seq[Var], Logic)], d:Logic): Logic = {
    //    println(clearString(d))
    //    temps.foreach(g => printf("temps: %s (vars: %s, vals: %s)\n", clearString(g._3), g._1.map(_.str).mkString(", "), g._2.map(_.str).mkString(", ")))
    val candidates = temps.flatMap(g => gen(g._1,g._3,d).map((g._2,_))).distinct
    //    candidates.foreach(g => printf("candidate: %s vals: %s\n", clearString(g._2), g._1.map(_.str).mkString(", ")))
    and(candidates.map(testCandidate(d)).map(simplify):_*)
  }


  def getEq(x:Val): Logic => Logic =  {
    case True   => True
    case False   => False
    case And(xs@_*) => and(xs.map(getEq(x)) : _*)
    case Or(xs@_*)  => or (xs.map(getEq(x)) : _*)
    case Not(y)  => True
    case App(BinIntProp("="), App(y), App(z)) if x==y && x==z => True
    case App(BinIntProp("="), App(y), e) if x==y => e
    case App(BinIntProp("="), e, App(y)) if x==y => e
    case App(f, xs@_*) => True
    case ForallL(t, z) => forall(t, getEq(x)(z))
  }

  def rewriteExp(e:Logic, v:Val, d: Logic): Logic = d match {
    case True  => forget(v)(e)
    case False => False // ???
    case And(xs@_*) => and(xs.map(rewriteExp(e,v,_)):_*)
    case Or(xs@_*)  => or (xs.map(rewriteExp(e,v,_)):_*)
    case Not(x)  => not(rewriteExp(e,v,x))
    case App(f, xs@_*) => repl(Map(v -> App(f, xs:_*)))(e)
    case ForallL(t, x) => ForallL(t, rewriteExp(e,v,x))
    case ExistsL(t, x) => ExistsL(t, rewriteExp(e,v,x))
  }

  def eqForget(x:Val): Logic => Logic = inForall {
    d =>
      //      Log.log(s"eqforget(${x.str}) of ${d.toSimpleString}: ")
      val eqx = getEq(x)(d)
      //      Log.log(s"eqs = ${eqx.toSimpleString}")
      val r =
        if (getAtoms(eqx) contains x)
          forget(x)(d)
        else
          forget(x)(rewriteExp(d,x,eqx))
      //      Log.log(s"result = ${r.toSimpleString}")
      r
  }

  def pushStack(d: Logic): Logic = {
    val vs = getAtoms(d).filter(_.isInstanceOf[SVar]).map(_.asInstanceOf[SVar])
    val vs1 = vs.map(x => (x, app(SVar(x.n + 1)))).toMap[Val, Logic]
    repl(vs1)(d)
  }

  // simple popStack where the value is transferred somewhere else
  def popStack(d: Logic, n:Int, r: Seq[Logic]): Logic = {
    val vs = getAtoms(d).filter(_.isInstanceOf[SVar]).map(_.asInstanceOf[SVar])
    def f(v: Val): (Val, LogLat.t) = v match {
      case SVar(m,t) if m<0 => (SVar(m,t), app(SVar(m,t)))
      case SVar(m,t) if m<n => (SVar(m,t), r(m))
      case SVar(m,t) => (SVar(m,t), app(SVar(m - n,t)))
    }
    val vs1 = vs.map(f).toMap[Val, Logic]
    repl(vs1)(d)
  }

  // popStack where the value is thrown away
  def popStack(d: Logic, n:Int): Logic = {
    val d1 = (1 to n).foldLeft(d){ (v,i) => eqForget(SVar(i-1))(v) }
    val vs = getAtoms(d).filter(_.isInstanceOf[SVar]).map(_.asInstanceOf[SVar])
    def vs2 = vs.map{
      case SVar(m,t) => SVar(m,t) -> app(SVar(m - n,t))
    }.toMap[Val, Logic]
    simplify(repl(vs2)(d1))
  }

  final implicit class LogicWrapper(x: Logic) {
    def &&(y:Logic): Logic = Logic.andE(x,y)
    def ||(y:Logic): Logic = Logic.orE(x,y)
    def not: Logic = Logic.not(x)
    def forall(y:Val): Logic = Logic.forall(y,x)
    def exists(y:Val): Logic = Logic.exists(y,x)
    def apply(y:Val, ys:Val*): Logic = Logic.appExp(x, (y +: ys).map(app(_)))
    def apply(y:Logic, ys:Logic*): Logic = Logic.appExp(x, y +: ys)
    def popStack(i:Int = 1): Logic = LogicHelper.popStack(x,i)
    def popStack(i:Int, xs:Seq[Logic]): Logic = LogicHelper.popStack(x,i,xs)
    def pushStack: Logic = LogicHelper.pushStack(x)
    def simplify: Logic = LogicHelper.simplify(x)
    def eqForget(v:Val): Logic = LogicHelper.eqForget(v)(x)
    def query(y:Logic): Option[Boolean] = {
      val trm = Logic.insertExists(Logic.andE(x, Logic.not(y)))
      val r = LogicHelper.satCheck(trm).map(!_)
//      Log.log(s"Query ${x.toSimpleString} ? ${y.toSimpleString} : $r")
      r
    }

//    def checkSat: Option[Boolean] = LogicHelper.satCheck(x)
  }
  final implicit class ValWrapper(x: Val) {
    def apply(): Logic = app(x)
    def apply(y:Logic, ys:Logic*): Logic = Logic.appE(x,y+:ys :_*)
    def apply(y:Val, ys:Val*): Logic = Logic.appE(x, (y+:ys).map(app(_)) :_*)
  }
}
