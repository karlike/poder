package põder.dom

import põder.dom.Intervals.Nums._
import põder.framework.{Lattice, NarrowOpF, WidenOpF}
import põder.util.json.JsonF

import scala.collection.immutable.Map

object Intervals {

  trait Expr

  case class Const() extends Expr

  case class Var(i: Int) extends Expr

  case class Arith(op: String, left: Expr, right: Expr) extends Expr

  case class Unknown() extends Expr

  case class Interval(lb: Num, ub: Num) {
    override def toString = s"[$lb, $ub]"
  }

  def intToInterval(i: Int): Interval = Interval(IntNum(i), IntNum(i))

  def add(a: Interval, b: Interval): Interval = {
    Interval(Nums.add(a.lb, b.lb), Nums.add(a.ub, b.ub))
  }

  def mul(a: Interval, b: Interval): Interval = (a.lb, a.ub, b.lb, b.ub) match {
    case (IntNum(lb1), IntNum(ub1), IntNum(lb2), IntNum(ub2)) =>
      val values = Set(lb1 * lb2, lb1 * ub2, ub1 * lb2, ub1 * ub2)
      Interval(IntNum(values.min), IntNum(values.max))
    case _ => throw new Exception(s"Undefined multiplication of $a and $b")
  }

  def div(a: Interval, b: Interval): Interval = (a.lb, a.ub, b.lb, b.ub) match {
    case (IntNum(lb1), IntNum(ub1), IntNum(lb2), IntNum(ub2)) =>
      val values = Set(lb1 / lb2, lb1 / ub2, ub1 / lb2, ub1 / ub2)
      Interval(IntNum(values.min), IntNum(values.max))
    case _ => throw new Exception(s"Undefined division of $a and $b")
  }

  type IntervalDomain = (Option[List[Interval]], Option[Map[String, Interval]], Option[List[Expr]])

  object IntLat extends Lattice[IntervalDomain] with WidenOpF[IntervalDomain] with NarrowOpF[IntervalDomain] {
    override def join(x: IntervalDomain, y: IntervalDomain): IntervalDomain = {
      if (x == top || y == top) return top
      else if (x == bot) return y
      else if (y == bot) return x
      else if (x == y) return x

      // Stack
      val stack = (x._1, y._1) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) =>
          Some(
            a.zipAll(b, Interval(PInf, MInf), Interval(PInf, MInf))
              .map(e => Lattice.join(e._1, e._2))
          )
      }

      // LVars
      val lVars: Option[Map[String, Interval]] = (x._2, y._2) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) =>
          Some(
            (a.toSeq ++ b.toSeq)
              .groupBy(_._1)
              .mapValues(_.map(_._2).reduce(Lattice.join))
          )
      }
      (stack, lVars, x._3)
    }

    override def leq(d1: IntervalDomain, d2: IntervalDomain): Boolean = {
      if (d1 == bot || d2 == top || d1 == d2) true
      else if (d1 == top || d2 == bot) false
      else {
        val d1_stack = d1._1.getOrElse(List())
        val d1_locals = d1._2.getOrElse(Map())
        val d2_stack = d2._1.getOrElse(List())
        val d2_locals = d2._2.getOrElse(Map())

        if (d1_stack.length != d2_stack.length || d1_locals.size != d2_locals.size)
          d1_stack.length <= d2_stack.length && d1_locals.size <= d2_locals.size
        else
          (d1_stack, d2_stack).zipped.forall(Lattice.leq) &&
            (d1_locals.keys ++ d2_locals.keys)
              .map(k => (d1_locals.get(k), d2_locals.get(k)))
              .forall({ case (Some(k1), Some(k2)) => Lattice.leq(k1, k2) })
      }
    }

    override def top: IntervalDomain = {
      (Some(List()), Some(Map()), Some(List()))
    }

    override def bot: IntervalDomain = {
      (None, None, None)
    }

    override def toJson[jv](x: IntervalDomain)(implicit j: JsonF[jv]): jv = {
      x match {
        case (None, None, None) => j.Str("bot")
        case (Some(List()), t, Some(List())) if t == Some(Map()) => j.Str("top")
        case _ => j.Str(s"2\nStack variables: ${x._1.getOrElse(List())}\nLocal variables: ${Map(x._2.getOrElse(Map()).toSeq.sortBy(_._1): _*)}\nStack details: ${x._3}\n")
      }
    }

    override def widen(oldv: IntervalDomain, newv: IntervalDomain): IntervalDomain = {
      // Stack
      val stack: Option[List[Interval]] = (oldv._1, newv._1) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) => Some((a, b).zipped.toList.map(e => Lattice.widen(e._1, e._2)))
      }

      // LVars
      val lVars: Option[Map[String, Interval]] = (oldv._2, newv._2) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) => Some((a.toSeq ++ b.toSeq).groupBy(_._1).mapValues(_.map(_._2).reduce(Lattice.widen)))
      }
      (stack, lVars, newv._3)
    }

    override def narrow(oldv: IntervalDomain, newv: IntervalDomain): IntervalDomain = {
      if (newv == bot) return bot

      // Stack
      val stack: Option[List[Interval]] = (oldv._1, newv._1) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) => Some((a, b).zipped.toList.map(e => Lattice.narrow(e._1, e._2)))
      }

      // LVars
      val lVars: Option[Map[String, Interval]] = (oldv._2, newv._2) match {
        case (None, None) => None
        case (Some(e), None) => Some(e)
        case (None, Some(e)) => Some(e)
        case (Some(a), Some(b)) => Some((a.toSeq ++ b.toSeq).groupBy(_._1).mapValues(_.map(_._2).reduce(Lattice.narrow)))
      }
      (stack, lVars, newv._3)
    }
  }

  object Lattice {
    def join(x: Interval, y: Interval): Interval = {
      Interval(min(Set(x.lb, y.lb)), max(Set(x.ub, y.ub)))
    }

    def leq(x: Interval, y: Interval): Boolean = {
      y.lb <= x.lb && x.ub <= y.ub
    }

    def widen(x: Interval, y: Interval): Interval = (x, y) match {
      case (Interval(PInf, MInf), i) => i
      case (i, Interval(PInf, MInf)) => i
      case _ =>
        val a: Num = if (x.lb <= y.lb) x.lb else MInf
        val b: Num = if (x.ub >= y.ub) x.ub else PInf
        Interval(a, b)
    }

    def narrow(x: Interval, y: Interval): Interval = (x, y) match {
      case (Interval(PInf, MInf), i) => i
      case (i, Interval(PInf, MInf)) => i
      case _ =>
        val a: Num = if (x.lb == MInf) y.lb else x.lb
        val b: Num = if (x.ub == PInf) y.ub else x.ub
        Interval(a, b)
    }
  }

  object Comparison {

    abstract class Result

    case class True() extends Result

    case class False() extends Result

    case class Uncertain() extends Result

    def compare_LE(a: Interval, b: Interval): Result = (a, b) match {
      case (x, y) if x.ub <= y.lb => True()
      case (x, y) if x.lb > y.ub => False()
      case _ => Uncertain()
    }

    def compare_GE(a: Interval, b: Interval): Result = (a, b) match {
      case (x, y) if x.lb >= y.ub => True()
      case (x, y) if x.ub < y.lb => False()
      case _ => Uncertain()
    }

    def compare_EQ(a: Interval, b: Interval): Result = (a, b) match {
      case (x, y) if x.lb == x.ub && x.ub == y.lb && y.lb == y.ub => True()
      case (x, y) if x.ub < y.lb || x.lb > y.ub => False()
      case _ => Uncertain()
    }

    def compare_NE(a: Interval, b: Interval): Result = Comparison.compare_EQ(a, b) match {
      case False() => True()
      case True() => False()
      case _ => Uncertain()
    }

    def compare_LT(a: Interval, b: Interval): Result = Comparison.compare_GE(a, b) match {
      case False() => True()
      case True() => False()
      case _ => Uncertain()
    }

    def compare_GT(a: Interval, b: Interval): Result = Comparison.compare_LE(a, b) match {
      case False() => True()
      case True() => False()
      case _ => Uncertain()
    }

    def cmp(a: Interval, b: Interval): Interval = {
      var results: List[Int] = List()
      Comparison.compare_EQ(a, b) match {
        case True() => return intToInterval(0)
        case Uncertain() => results = 0 :: results
        case _ =>
      }

      Comparison.compare_GT(a, b) match {
        case True() => return intToInterval(1)
        case Uncertain() => results = 1 :: results
        case _ =>
      }
      Comparison.compare_LT(a, b) match {
        case True() => return intToInterval(-1)
        case Uncertain() => results = -1 :: results
        case _ =>
      }
      Interval(IntNum(results.min), IntNum(results.max))
    }
  }

  object Nums {
    /**
      * A Num is an int, +infinity, or -infinity.
      */
    sealed trait Num extends Ordered[Num] {
      def compare(that: Num): Int =
        (this, that) match {
          case (x, y) if x == y => 0
          case (IntNum(a), IntNum(b)) => a - b
          case (MInf, _) => -1
          case (_, PInf) => -1
          case (PInf, _) => 1
          case (_, MInf) => 1
        }
    }

    case class IntNum(i: Int) extends Num {
      override def toString = s"$i"
    }

    case object MInf extends Num {
      override def toString = "-inf"
    }

    case object PInf extends Num {
      override def toString = "+inf"
    }

    /**
      * Finds the maximum of the given set of Num values.
      */
    def max(s: Set[Num]): Num =
      if (s.isEmpty) MInf
      else {
        s.reduce { (a, b) =>
          (a, b) match {
            case (PInf, _) | (_, PInf) => PInf
            case (x, MInf) => x
            case (MInf, x) => x
            case (IntNum(x), IntNum(y)) => IntNum(math.max(x, y))
          }
        }
      }

    /**
      * Finds the minimum of the given set of Num values.
      */
    def min(s: Set[Num]): Num =
      if (s.isEmpty) PInf
      else {
        s.reduce { (a, b) =>
          (a, b) match {
            case (PInf, x) => x
            case (x, PInf) => x
            case (MInf, _) | (_, MInf) => MInf
            case (IntNum(x), IntNum(y)) => IntNum(math.min(x, y))
          }
        }
      }

    def add(a: Num, b: Num): Num = (a, b) match {
      case (_, MInf) | (MInf, _) => MInf
      case (_, PInf) | (PInf, _) => PInf
      case (IntNum(x), IntNum(y)) => IntNum(x + y)
    }

  }

}