package põder.framework

import põder.util.json.JsonF

trait TypedF[T] {
  type t = T
}

trait TopOpF[T] extends TypedF[T] {
  def top: T
}
trait BotOpF[T] extends TypedF[T] {
  def bot: T
}
trait JoinOpF[T] extends TypedF[T] {
  def join(x:T, y:T): T
}
trait MeetOpF[T] extends TypedF[T] {
  def meet(x:T, y:T): T
}
trait LeqOpF[T] extends TypedF[T] {
  def leq(x:T, y:T): Boolean
}
trait EqualOpF[T] extends LeqOpF[T] {
  def equal(x:T, y:T): Boolean = leq(x,y) && leq(y, x)
}
trait JsonOpF[T] extends TypedF[T] {
  def toJson[jv](x: T)(implicit j: JsonF[jv]): jv
}
trait WidenOpF[T] extends TypedF[T] {
  def widen(oldv: T, newv:T): T
}
trait NarrowOpF[T] extends TypedF[T] {
  def narrow(oldv: T, newv:T): T = oldv
}


trait Lattice[T]
  extends TopOpF[T]
     with BotOpF[T]
     with JoinOpF[T]
     with EqualOpF[T]
     with JsonOpF[T]
{
  def Join(xs: Seq[T]): T =
    if (xs.isEmpty)
      bot
    else
      xs.tail.foldLeft(xs.head)(join)
}

case object BotException extends Exception

trait ExBotLattice[T] extends Lattice[T] {
  final override def bot = throw BotException
}

abstract class ReducedBotLattice[T] extends Lattice[T] {
  final def lift(x: => T): T =
    try {
      x
    } catch {
      case BotException => bot
    }
}

object MaxInt extends Lattice[Int] {
  def top = Int.MaxValue
  def bot = Int.MinValue

  def join(x:Int, y: Int): Int = math.max(x, y)

  override def leq(x: Int, y: Int): Boolean = x <= y

  override def toJson[jv](x: Int)(implicit j: JsonF[jv]): jv = j.Num(BigDecimal(x))
}

class Const[T](x: T) extends Lattice[T] {
  override def top: T = x
  override def bot: T = x
  override def join(x: T, y: T): T = x

  override def leq(x: T, y: T): Boolean = true

  override def toJson[jv](x: T)(implicit j: JsonF[jv]): jv =
    j.fromVal(x)
}

case object UnitLat extends Const[Unit](()) with Lattice[Unit]

class Product[T,R](To: Lattice[T], Ro: Lattice[R]) extends Lattice[(T,R)] {
  override def top: (T,R) = (To.top, Ro.top)
  override def bot: (T,R) = (To.bot, Ro.bot)
  override def join(x: (T,R), y: (T,R)): (T,R) = (x,y) match {
    case ((x1,x2),(y1,y2)) => (To.join(x1,y1), Ro.join(x2,y2))
  }

  override def leq(x: (T, R), y: (T, R)): Boolean =
   To.leq(x._1, y._1) && Ro.leq(x._2, y._2)

  override def equal(x: (T, R), y: (T, R)): Boolean =
    To.equal(x._1, y._1) && Ro.equal(x._2, y._2)

  override def toJson[jv](x: (T, R))(implicit j: JsonF[jv]): jv = {
    import j._
    Obj("left" -> To.toJson(x._1), "right" -> Ro.toJson(x._2))
  }
}

class ProductExBot[T,R](To: ExBotLattice[T], Ro: ExBotLattice[R]) extends ExBotLattice[(T,R)] {
  override def top: (T,R) = (To.top, Ro.top)
  override def join(x: (T,R), y: (T,R)): (T,R) = (x,y) match {
    case ((x1,x2),(y1,y2)) => (To.join(x1,y1), Ro.join(x2,y2))
  }

  override def leq(x: (T, R), y: (T, R)): Boolean =
    To.leq(x._1, y._1) && Ro.leq(x._2, y._2)

  override def toJson[jv](x: (T, R))(implicit j: JsonF[jv]): jv = {
    import j._
    Obj("left" -> To.toJson(x._1), "right" -> Ro.toJson(x._2))
  }
}

class ProductExtraExBot[T,R](To: ExBotLattice[T]) extends ExBotLattice[(T,Option[R])] {
  override def top: (T,Option[R]) = (To.top, None)
  override def join(x: (T,Option[R]), y: (T,Option[R])): (T,Option[R]) = (x,y) match {
    case ((x1,x2),(y1,y2)) =>
      val ei = if (x2.isEmpty) y2 else x2
      (To.join(x1,y1), ei)
  }

  override def leq(x: (T, Option[R]), y: (T, Option[R])): Boolean =
    To.leq(x._1, y._1)

  override def toJson[jv](x: (T, Option[R]))(implicit j: JsonF[jv]): jv = {
    import j._
    x match {
      case (a, Some(b)) =>
        Obj("data" -> To.toJson(a), "info" -> j.fromVal(b))
      case (a,_) =>
        To.toJson(a)
    }
  }
}


class Stacks[T](Ops: Lattice[T]) extends Lattice[Option[List[T]]] {
  override def top: Option[List[T]] = None
  override def bot: Option[List[T]] = Some(List())

  override def join(x: Option[List[T]], y: Option[List[T]]): Option[List[T]] = {
    for {xv <- x; yv <- y} yield (xv,yv).zipped.map(Ops.join)
  }

  override def leq(x: Option[List[T]], y: Option[List[T]]): Boolean =
    y.isEmpty || (x.isDefined && (x.get.length==y.get.length) && (x.get,y.get).zipped.forall(Ops.leq))

  override def toJson[jv](x: Option[List[T]])(implicit j: JsonF[jv]): jv =
    x match {
      case Some(y) =>
        j.Arr(y.map(Ops toJson _))
      case None =>
        j.Str("⏉")
    }
}

class ExBotStacks[T](Ops: ExBotLattice[T]) extends ExBotLattice[Option[List[T]]] {
  override def top: Option[List[T]] = None

  override def join(x: Option[List[T]], y: Option[List[T]]): Option[List[T]] = {
    for {xv <- x; yv <- y} yield (xv, yv).zipped.map(Ops.join)
  }

  override def leq(x: Option[List[T]], y: Option[List[T]]): Boolean =
    y.isEmpty || (x.isDefined && (x.get.length==y.get.length) && (x.get,y.get).zipped.forall(Ops.leq))

  override def toJson[jv](x: Option[List[T]])(implicit j: JsonF[jv]): jv = {
    import j._
    x match {
      case Some(y) =>
        Arr(y.map(Ops.toJson(_)))
      case None =>
        Str("⏉")
    }
  }
}

abstract class TopSet[T] extends Lattice[Option[Set[T]]] {
  override def top: Option[Set[T]] = None
  override def bot: Option[Set[T]] = Some(Set())

  def TtoJson[jv](x: T)(implicit j: JsonF[jv]): jv

  override def toJson[jv](x: Option[Set[T]])(implicit j: JsonF[jv]): jv =
    x match {
      case Some(value) => j.Arr(value.map(x => TtoJson(x)))
      case None =>
        j.Str("top")
    }

  override def leq(x: Option[Set[T]], y: Option[Set[T]]): Boolean =
    (x,y) match {
      case (Some(xs), Some(ys)) => xs subsetOf ys
      case (None, None)     => true
      case (None, Some(_)) => false
      case (Some(_), None) => true
    }
  override def join(x: Option[Set[T]], y: Option[Set[T]]): Option[Set[T]] =
    (x,y) match {
      case (Some(xs), Some(ys)) => Some(xs union ys)
      case _ => None
    }
}

class BotLift[T](l:Lattice[T] with WidenOpF[T] with NarrowOpF[T]) extends Lattice [Option[T]] with WidenOpF[Option[T]] with NarrowOpF[Option[T]] {
  override def top: Option[T] = Some(l.top)
  override def bot: Option[T] = None

  override def toJson[jv](x: Option[T])(implicit j: JsonF[jv]): jv = {
    x match {
      case Some(value) => l.toJson(value)
      case None =>j.Str("bot")
    }
  }

  override def leq(x: Option[T], y: Option[T]): Boolean = {
    (x,y) match {
      case (Some(_), None) => false
      case (None,_) => true
      case (Some(x),Some(y)) => l.leq(x,y)
    }
  }

  override def join(x: Option[T], y: Option[T]): Option[T] = {
    (x,y) match {
      case (None, None) => None
      case (Some(x), None) => Some(x)
      case (None,(Some(y))) => Some(y)
      case (Some(x),Some(y)) => Some(l.join(x,y))
    }
  }

  override def widen(oldv: Option[T], newv: Option[T]): Option[T] = {
    (oldv, newv) match {
      case (None, None) => None
      case (Some(x),None) => Some(x)
      case (None, Some(y)) => Some(y)
      case (Some(x),Some(y)) => Some(l.widen(x,y))
    }
  }
}
