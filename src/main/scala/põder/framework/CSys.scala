package põder.framework
import põder.util.json.JsonF

import scala.util.{Left, Right}

class BreakTransferFunction extends Throwable {
  override def toString: String = {
    val s = getClass.getSuperclass.getName
    val message = getLocalizedMessage
    if (message != null) s + ": " + message
    else s
  }
}

trait CSys[Var, D]
  extends TypedF[D]
{
  val L: Lattice[D]
  def tf(get: Var => D, set: Var => D => Unit, inVerification:Boolean = false)(v:Var): D
  def postProcess(): Unit
}

class EitherLattice[L,G]
  (L:Lattice[L],
   G:Lattice[G])
  extends Lattice[Either[L, G]]
{
  override def join(x: Either[L, G], y: Either[L, G]): Either[L, G] = (x,y) match {
    case (Right(a),  Left(b)) => if (a==G.bot || b==L.top) Left(b) else sys.error("joining data from local and global lattices")
    case (Left(a) , Right(b)) => if (b==G.bot || a==L.top) Left(a) else sys.error("joining data from local and global lattices")
    case (Left(a) ,  Left(b)) => Left(L.join(a, b))
    case (Right(a), Right(b)) => Right(G.join(a, b))
  }

  override def leq(x: Either[L, G], y: Either[L, G]): Boolean = (x,y) match {
    case (Right(_),  Left(_)) => true
    case (Left(_) , Right(_)) => false
    case (Left(a) ,  Left(b)) => L.leq(a, b)
    case (Right(a), Right(b)) => G.leq(a, b)
  }

  override def bot: Either[L, G] = Right(G.bot)

  override def top: Either[L, G] = Left(L.top)

  override def toJson[jv](x: Either[L, G])(implicit j: JsonF[jv]): jv =
    x match {
      case Left(a) => L.toJson(a)
      case Right(b) => G.toJson(b)
    }
}