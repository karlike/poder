package põder.framework.cfg

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout
import com.mxgraph.model.mxCell
import com.mxgraph.util.{mxConstants, mxEvent, mxEventObject}
import com.mxgraph.util.mxEventSource.mxIEventListener
import com.mxgraph.view.{mxGraph, mxGraphSelectionModel, mxPerimeter}
import põder.util.typ.MethodType

import scala.collection.mutable
import scala.util.matching.Regex
import scala.util.{Left, Right}

sealed abstract class TFType[Annot] extends Serializable

case class BasicBlock[Annot](var x: List[Either[Instr,Annot]]) extends TFType[Annot] {
  override def toString: String =
    x.map{
      case Left(y)  => y.toString
      case Right(y) => y.toString
    }.mkString("\n")
}
case class Jump[Annot](i: JumpInstr) extends TFType[Annot]
case class Fall[Annot](i: JumpInstr) extends TFType[Annot]
case class Start[Annot](locals: Int, stack: Int, access: Int) extends TFType[Annot] {
  def accStr: String = {
    var s = Set[String]()
    if ((access & 0x0001) == 0x0001)
      s += "public"
    if ((access & 0x0010) == 0x0010)
      s += "final"
    if ((access & 0x0020) == 0x0020)
      s += "synchronized"
    if ((access & 0x0200) == 0x0200)
      s += "interface"
    if ((access & 0x0400) == 0x0400)
      s += "abstract"
    if ((access & 0x1000) == 0x1000)
      s += "synthetic"
    if ((access & 0x2000) == 0x2000)
      s += "annotation"
    if ((access & 0x4000) == 0x4000)
      s += "enum"
    s.mkString(" ")
  }

  override def toString: String = {
    s"Start(locals = $locals, stack = $stack, $accStr)"
  }
}
case class Returns[Annot](i:MethodInstr) extends TFType[Annot] {
  override def toString: String = /*"Returns:\n" ++ */ i.toString
}
case class Throws[Annot](var i: List[Either[Instr,Annot]], e:Either[String,Set[String]]) extends TFType[Annot] {
  override def toString: String = {
    val b = i.map {
      case Left(y) => y.toString
      case Right(y) => y.toString
    }.mkString("\n")

    e match {
      case Left(value) => s"Throws($value):\n$b"
      case Right(value) => s"Throws(¬${value.mkString("{", ", ", "}")}):\n$b"
    }
  }
}

object CFG {
  def addStyle(s:String, n:String):String = {
    if (!s.eq(null) && s.endsWith(";"))
      s++n
    else if (s.eq(null))
      n
    else
      s++";"++n
  }
  def removeStyle(s:String, n:String*):String = {
    n.foldLeft(s){
      case (s1,n1) =>
        s1.replaceAll(Regex.quote(n1)++"([^;])*;","")
    }
  }
  def changeStyle(s: String, n: (String, String)*): String = {
    n.foldLeft(s) { case (s1,(n1,v1)) =>
      s1.replaceAll("("++Regex.quote(n1)++")=([^;])*;","$1="++v1++";")
    }
  }

  class E[Edge](val from: Node, val to: Node, var data: Edge) extends Serializable

  object Graph {
    val defaultEdgeStyle = "defaultVertex;autosize=true;fillColor=white;fontColor=black;strokeColor=white;"
    val defaultNodeStyle = "defaultVertex;autosize=true;fillColor=black;fontColor=white;"
    val selectNodeStyle = "strokeColor=orange;strokeWidth=6;"
  }

  class Graph[Edge]
    (val nodes: List[Node],
     val edges: List[E[Edge]],
     val start: Node,
     val retrn: Node,
     val exn:   Node,
     val throwingNodes: List[Node]
    )
  extends Serializable
  {
    def toMXG(select: Node => Unit, selecte: E[Edge] => Unit): (mxGraph, Map[Node, mxCell]) = {
      val nm = mutable.Map.empty[Node, mxCell]
      val em = mutable.Map.empty[E[Edge], mxCell]
      val mn = mutable.Map.empty[AnyRef,Node]
      val me = mutable.Map.empty[mxCell, E[Edge]]
      val graph = new mxGraph() {
        override def isCellSelectable(cell: Any): Boolean = {
          val state = view.getState(cell)
          val style = if (state != null) state.getStyle else this.getCellStyle(cell)

          super.isCellSelectable(cell) && !isCellLocked(cell) && !style.containsKey("nonselectable")
        }
      }


      graph.getSelectionModel.addListener(mxEvent.CHANGE, new mxIEventListener {
        var oldStyles = Set.empty[mxCell]
        override def invoke(sender: scala.Any, evt: mxEventObject): Unit = {
          def removeSelectedStyle(k:mxCell): Unit = {
            val r = removeStyle(k.getStyle, "strokeColor", "strokeWidth")
            k.setStyle(addStyle(r,"strokeColor=white;"))
          }
          sender match {
            case s: mxGraphSelectionModel =>
              for (cell <- s.getCells) {
                if (mn contains cell) {
                  oldStyles.foreach(removeSelectedStyle)
                  val c = cell.asInstanceOf[mxCell]
                  oldStyles = Set(c)
                  c.setStyle(addStyle(c.getStyle,Graph.selectNodeStyle))
                  select(mn(cell))
                  graph.refresh()
                } else cell match {
                  case c: mxCell if me contains c =>
                    oldStyles.foreach(removeSelectedStyle)
                    oldStyles = Set(c)
                    c.setStyle(addStyle(c.getStyle, Graph.selectNodeStyle))
                    selecte(me(c))
                    graph.refresh()
                  case _ =>
                }
              }
          }
        }
      })

      val estyle = graph.getStylesheet.getDefaultEdgeStyle
      val vstyle = graph.getStylesheet.getDefaultVertexStyle
      vstyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE)
      vstyle.put(mxConstants.STYLE_PERIMETER, mxPerimeter.RectanglePerimeter)
      vstyle.put(mxConstants.STYLE_FONTCOLOR, "#111")
      vstyle.put(mxConstants.STYLE_FONTSIZE, new Integer(14))
      vstyle.put(mxConstants.STYLE_FILLCOLOR, "#fff")
      vstyle.put(mxConstants.STYLE_ROUNDED, Boolean.box(true))
      vstyle.put(mxConstants.STYLE_RESIZABLE, Boolean.box(false))
      vstyle.put(mxConstants.STYLE_AUTOSIZE, Boolean.box(true))

      estyle.put("nonselectable", Boolean.box(true))
      estyle.put(mxConstants.STYLE_AUTOSIZE, Boolean.box(true))
      estyle.put(mxConstants.STYLE_ROUNDED, Boolean.box(true))
      estyle.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION)
      estyle.put(mxConstants.STYLE_STROKEWIDTH, new Integer(2))

      val parent = graph.getDefaultParent
      graph.getModel.beginUpdate()
      for (n <- nodes) {
        val dtext = n.toString
        val nwidth = dtext.length*12+20
        val nheight = if (dtext.isEmpty) 20.0 else 25.0
        val o = graph.insertVertex(parent, null, dtext, 10, 20, nwidth, nheight,Graph.defaultNodeStyle)
        nm += n -> o.asInstanceOf[mxCell]
        mn += o -> n
      }
      for (e <- edges) {
        val dtext = e.data.toString.split('\n')
        val nwidth = dtext.map(_.length).max*8+10
        val nheight = dtext.length*15+8
        val en = graph.insertVertex(parent, null, e.data.toString, 0, 0, nwidth, nheight, Graph.defaultEdgeStyle)

        em += e -> en.asInstanceOf[mxCell]
        me += en.asInstanceOf[mxCell] -> e
      }
      for (e <- edges) {
        graph.insertEdge(parent, null, "", nm(e.from), em(e),"endArrow=none")
        if (!e.to.is_exn)
          graph.insertEdge(parent, null, "", em(e), nm(e.to),"")
      }
      graph.getModel.endUpdate()
      val lo = new mxHierarchicalLayout(graph)
      lo.setIntraCellSpacing(10.0)
      lo.setInterRankCellSpacing(25.0)
      lo.setInterHierarchySpacing(10.0)
      lo.setParallelEdgeSpacing(6.0)
      lo.setResizeParent(true)
      lo.setFineTuning(true)
//      lo.setHorizontal(false)
      lo.execute(graph.getDefaultParent)
//      val lo2 = new mxFastOrganicLayout(graph)
//      lo2.execute(graph.getDefaultParent)

//      val lo2 = new mxEdgeLabelLayout(graph)
//      lo2.execute(graph.getDefaultParent)
      graph.setCellsEditable(false)
      graph.setCellsBendable(false)
      graph.setCellsMovable(false)
      graph.setCellsDeletable(false)
      graph.setAllowDanglingEdges(false)
      graph.setAllowLoops(false)
      graph.setCellsCloneable(false)
      graph.setCellsDisconnectable(false)
      graph.setDropEnabled(false)
      graph.setSplitEnabled(false)
      graph.setCellsBendable(false)
      (graph, nm.toMap)
    }
  }

}

abstract class ProgramCFG {
  var startMethods: Set[(String, MethodType)] = Set.empty
  val next: mutable.Map[Node, Seq[(TFType[Annot], Node)]]
  val prev: mutable.Map[Node, Seq[(TFType[Annot], Node)]]
  val usedMeths: mutable.Set[(String, String, MethodType)]

  var basePath: String
  var mainPath: String
  var mainClass: String

  def startVarsFun(c: String, m: String, t: MethodType): Node
  def returnVarsFun(c: String, m: String, t: MethodType): Node
  def getMethodThrowingNodes(c: String, m: String, t: MethodType): List[Node]

  def getMethod(c: String, m: String, t: MethodType): CFG.Graph[TFType[Annot]]
}