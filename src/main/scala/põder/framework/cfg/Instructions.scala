package põder.framework.cfg

import põder.util.Util.Cell

// Many instructions range over simple types.
// To collect those instructions together, lets define simple types.
sealed abstract class SimpleType extends Serializable
sealed abstract class SimpleValType        extends SimpleType
sealed abstract class SimpleWholeType      extends SimpleValType
sealed abstract class SimpleSmallWholeType extends SimpleWholeType
sealed abstract class SimpleBigWholeType   extends SimpleWholeType
sealed abstract class SimpleFloatType      extends SimpleValType
case object I extends SimpleBigWholeType    // Int
case object L extends SimpleBigWholeType    // Long
case object B extends SimpleSmallWholeType  // Byte
case object S extends SimpleSmallWholeType  // Short
case object C extends SimpleSmallWholeType  // Char
case object F extends SimpleFloatType       // Float
case object D extends SimpleFloatType       // Double
case object A extends SimpleType            // Object (or Array?)

// The class reader visitor is arranged based instruction arguments.
// For int argumented instructions, there is one callback that
// gives you the opcode and the integer argument.
// We, therefore, define traits to provide instructions with their arguments.
//
trait Arg extends Serializable

trait IntArg extends Arg {
  var a: Cell[Int]
}

trait FieldArg extends Arg {
  var a: Cell[(String,String,String)]
}

trait MethodArg extends Arg {
  var a: Cell[(String,String,String)]
  override def toString: String = {
    if(a.x.isDefined)
      s"${this.getClass.getSimpleName}(${a.x.get._1},${a.x.get._2})"
    else
      super.toString
  }
}

trait TypeArg extends Arg {
  var a: Cell[String]
}

trait VarArg extends Arg {
  var a: Cell[Byte]
}

trait LabelArg extends Arg {
  var a: Cell[String]
}

trait ArithType {
  val x: SimpleValType
  override def toString: String = {
    s"$x${this.getClass.getSimpleName.drop(1)}"
  }
}

trait SimpleTypeArg {
  val x: SimpleType
  override def toString: String = {
    s"$x${this.getClass.getSimpleName.drop(1)}"
  }
}

trait OrderArg {
  val x: Order
  override def toString: String = {
    s"${this.getClass.getSimpleName.dropRight(1)}$x"
  }
}

// The base-class for instructions (with or without instructions).
sealed abstract class Instr extends Serializable {}

// Categorization based on the ASM documentation
sealed abstract class StackInstr  extends Instr
sealed abstract class ConstInstr  extends Instr
sealed abstract class CastInstr   extends Instr
sealed abstract class ObjInstr    extends Instr
sealed abstract class FieldInstr  extends Instr with FieldArg
sealed abstract class MethodInstr extends Instr with MethodArg
sealed abstract class ArrayInstr  extends Instr
sealed abstract class JumpInstr   extends Instr
sealed abstract class RetInstr    extends Instr
sealed abstract class ArithInstr  extends Instr

// Instructions are typically case classes so that
// we get the toString method etc. for free.
case class  xLOAD(x:SimpleType, var a: Cell[Byte] = Cell())   extends StackInstr with VarArg {
  override def toString: String = {
    s"${x}LOAD(${a.x.get})"
  }
}
case class  xSTORE(x:SimpleType, var a: Cell[Byte] = Cell())  extends StackInstr with VarArg {
  override def toString: String = {
    s"${x}STORE(${a.x.get})"
  }
}
case class  xALOAD(x:SimpleType)                 extends StackInstr with SimpleTypeArg
case class  xASTORE(x:SimpleType)                extends StackInstr with SimpleTypeArg
case object POP                                  extends StackInstr
case object POP2                                 extends StackInstr
case object DUP                                  extends StackInstr
case object DUP_X1                               extends StackInstr
case object DUP_X2                               extends StackInstr
case object DUP2                                 extends StackInstr
case object DUP2_X1                              extends StackInstr
case object DUP2_X2                              extends StackInstr
case object SWAP                                 extends StackInstr
case class  IINC(a:Cell[(Short,Short)])          extends StackInstr
case object NOP                                  extends StackInstr

// xConst instructions push small values onto the operand stack
// We provide a couple of layers: based on type ...
sealed abstract class xCONST(x:SimpleType) extends ConstInstr

// ... and value
sealed abstract class ICONST(val n:Int)    extends xCONST(I)
sealed abstract class LCONST(val n:Long)   extends xCONST(L)
sealed abstract class FCONST(val n:Float)  extends xCONST(F)
sealed abstract class DCONST(val n:Double) extends xCONST(D)
case object           ACONST_NULL      extends xCONST(A)

case object ICONST_M1 extends ICONST(-1)
case object ICONST_0  extends ICONST(0)
case object ICONST_1  extends ICONST(1)
case object ICONST_2  extends ICONST(2)
case object ICONST_3  extends ICONST(3)
case object ICONST_4  extends ICONST(4)
case object ICONST_5  extends ICONST(5)
case object LCONST_0  extends LCONST(0)
case object LCONST_1  extends LCONST(1)
case object FCONST_0  extends FCONST(0.0f)
case object FCONST_1  extends FCONST(1.0f)
case object FCONST_2  extends FCONST(2.0f)
case object DCONST_0  extends DCONST(0.0)
case object DCONST_1  extends DCONST(1.0)

// For larger constants we have these instructions
case class BIPUSH(var a: Cell[Int] = Cell()) extends ConstInstr with IntArg
case class SIPUSH(var a: Cell[Int] = Cell()) extends ConstInstr with IntArg
case class LDC(a: Cell[Any] = Cell())        extends ConstInstr

case class xADD(x:SimpleValType) extends ArithInstr with ArithType
case class xSUB(x:SimpleValType) extends ArithInstr with ArithType
case class xMUL(x:SimpleValType) extends ArithInstr with ArithType
case class xDIV(x:SimpleValType) extends ArithInstr with ArithType
case class xREM(x:SimpleValType) extends ArithInstr with ArithType
case class xNEG(x:SimpleValType) extends ArithInstr with ArithType

case class xSHL(x:SimpleBigWholeType)  extends ArithInstr with ArithType
case class xSHR(x:SimpleBigWholeType)  extends ArithInstr with ArithType
case class xUSHR(x:SimpleBigWholeType) extends ArithInstr with ArithType
case class xAND(x:SimpleBigWholeType)  extends ArithInstr with ArithType
case class xOR(x:SimpleBigWholeType)   extends ArithInstr with ArithType
case class xXOR(x:SimpleBigWholeType)  extends ArithInstr with ArithType
case class xCMPL(x:SimpleFloatType)    extends ArithInstr with ArithType
case class xCMPG(x:SimpleFloatType)    extends ArithInstr with ArithType
case object LCMP                       extends ArithInstr with ArithType {val x=L}

case class  xRETURN(x:SimpleType) extends RetInstr  with SimpleTypeArg
case object RETURN                extends RetInstr
case object ATHROW                extends RetInstr

case class x2y(x: SimpleValType, y: SimpleValType)   extends CastInstr
case class CHECKCAST(var a:Cell[String]= Cell())    extends CastInstr with TypeArg

// Instructions like IFx vary on the branch condition
sealed abstract class Order { val str: String }
case object EQ extends Order { val str = "="  }
case object NE extends Order { val str = "!=" }
case object LT extends Order { val str = "<"  }
case object GE extends Order { val str = ">=" }
case object GT extends Order { val str = ">"  }
case object LE extends Order { val str = "<=" }


case class IFx(x: Order, var a:Cell[String]= Cell())                           extends JumpInstr with LabelArg with OrderArg
case class IF_ICMPx(x: Order, var a:Cell[String]= Cell())                      extends JumpInstr with LabelArg with OrderArg
case class IF_ACMPx(x: Order, var a:Cell[String]= Cell())                      extends JumpInstr with LabelArg with OrderArg
case class GOTO(var a:Cell[String]= Cell())                                    extends JumpInstr with LabelArg
case class IFNULL(var a:Cell[String]= Cell())                                  extends JumpInstr with LabelArg
case class IFNONNULL(var a:Cell[String]= Cell())                               extends JumpInstr with LabelArg
case class TABLESWITCH(var a:Cell[(Int,Int,String,Array[String])]= Cell())     extends JumpInstr
case class LOOKUPSWITCH(var a:Cell[(String,Array[Int],Array[String])]= Cell()) extends JumpInstr


case class GETSTATIC(var a: Cell[(String,String,String)]= Cell()) extends FieldInstr
case class PUTSTATIC(var a: Cell[(String,String,String)]= Cell()) extends FieldInstr
case class GETFIELD(var a: Cell[(String,String,String)]= Cell())  extends FieldInstr
case class PUTFIELD(var a: Cell[(String,String,String)]= Cell())  extends FieldInstr


case class INVOKEVIRTUAL(var a: Cell[(String,String,String)]= Cell())          extends MethodInstr
case class INVOKESPECIAL(var a: Cell[(String,String,String)]= Cell())          extends MethodInstr
case class INVOKESTATIC(var a: Cell[(String,String,String)]= Cell())           extends MethodInstr
case class INVOKEINTERFACE(var a: Cell[(String,String,String)]= Cell())        extends MethodInstr
case class INVOKEDYNAMIC(var a: Cell[(String,String,String)]= Cell(), var c:Cell[(String, String, String, Boolean,List[AnyRef])]=Cell()) extends MethodInstr{
  override def toString: String = {
    s"INVOKEDYNAMIC(${a.x.get._2},${a.x.get._3})"
  }
}

case class NEW(var a:Cell[(String)]= Cell())          extends ObjInstr with TypeArg
case class INSTANCEOF(var a: Cell[String]= Cell())    extends ObjInstr with TypeArg
case object MONITORENTER                  extends ObjInstr
case object MONITOREXIT                   extends ObjInstr

case class NEWARRAY(var a:Cell[Int]= Cell())              extends ArrayInstr with IntArg
case class ANEWARRAY(var a:Cell[String]= Cell())             extends ArrayInstr with TypeArg
case object ARRAYLENGTH                          extends ArrayInstr
case class MULTIANEWARRAY(a:Cell[(Short,Byte)]= Cell())  extends ArrayInstr

// RET and JSR are deprecated
case class DEPRECATED(n:String) extends Instr

// Some instruction codes are unused (or unused in ASM)
case object UNUSED extends Instr

object InstrUtil {
  val INSTR: Array[() => Instr] =
    Array(
        () => NOP
      , () => ACONST_NULL
      , () => ICONST_M1
      , () => ICONST_0
      , () => ICONST_1
      , () => ICONST_2
      , () => ICONST_3
      , () => ICONST_4
      , () => ICONST_5
      , () => LCONST_0
      , () => LCONST_1
      , () => FCONST_0
      , () => FCONST_1
      , () => FCONST_2
      , () => DCONST_0
      , () => DCONST_1
      , () => BIPUSH()
      , () => SIPUSH()
      , () => LDC()
      , () => UNUSED // ldc_w
      , () => UNUSED // ldc_w2
      , () => xLOAD(I)//ILOAD
      , () => xLOAD(L)//LLOAD
      , () => xLOAD(F)//FLOAD
      , () => xLOAD(D)//DLOAD
      , () => xLOAD(A)//ALOAD
      , () => UNUSED//ILOAD_0
      , () => UNUSED//ILOAD_1
      , () => UNUSED//ILOAD_2
      , () => UNUSED//ILOAD_3
      , () => UNUSED//LLOAD_0
      , () => UNUSED//LLOAD_1
      , () => UNUSED//LLOAD_2
      , () => UNUSED//LLOAD_3
      , () => UNUSED//FLOAD_0
      , () => UNUSED//FLOAD_1
      , () => UNUSED//FLOAD_2
      , () => UNUSED//FLOAD_3
      , () => UNUSED//DLOAD_0
      , () => UNUSED//DLOAD_1
      , () => UNUSED//DLOAD_2
      , () => UNUSED//DLOAD_3
      , () => UNUSED//ALOAD_0
      , () => UNUSED//ALOAD_1
      , () => UNUSED//ALOAD_2
      , () => UNUSED//ALOAD_3
      , () => xALOAD(I)//IALOAD
      , () => xALOAD(L)//LALOAD
      , () => xALOAD(F)//FALOAD
      , () => xALOAD(D)//DALOAD
      , () => xALOAD(A)//AALOAD
      , () => xALOAD(B)//BALOAD
      , () => xALOAD(C)//CALOAD
      , () => xALOAD(S)//SALOAD
      , () => xSTORE(I)//ISTORE
      , () => xSTORE(L)//LSTORE
      , () => xSTORE(F)//FSTORE
      , () => xSTORE(D)//DSTORE
      , () => xSTORE(A)//ASTORE
      , () => UNUSED//xSTORE_c
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => xASTORE(I)//IASTORE
      , () => xASTORE(L)//LASTORE
      , () => xASTORE(F)//FASTORE
      , () => xASTORE(D)//DASTORE
      , () => xASTORE(A)//AASTORE
      , () => xASTORE(B)//BASTORE
      , () => xASTORE(C)//CASTORE
      , () => xASTORE(S)//SASTORE
      , () => POP
      , () => POP2
      , () => DUP
      , () => DUP_X1
      , () => DUP_X2
      , () => DUP2
      , () => DUP2_X1
      , () => DUP2_X2
      , () => SWAP
      , () => xADD(I)//IADD
      , () => xADD(L)//LADD
      , () => xADD(F)//FADD
      , () => xADD(D)//DADD
      , () => xSUB(I)//ISUB
      , () => xSUB(L)//LSUB
      , () => xSUB(F)//FSUB
      , () => xSUB(D)//DSUB
      , () => xMUL(I)//IMUL
      , () => xMUL(L)//LMUL
      , () => xMUL(F)//FMUL
      , () => xMUL(D)//DMUL
      , () => xDIV(I)//IDIV
      , () => xDIV(L)//LDIV
      , () => xDIV(F)//FDIV
      , () => xDIV(D)//DDIV
      , () => xREM(I)//IREM
      , () => xREM(L)//LREM
      , () => xREM(F)//FREM
      , () => xREM(D)//DREM
      , () => xNEG(I)//INEG
      , () => xNEG(L)//LNEG
      , () => xNEG(F)//FNEG
      , () => xNEG(D)//DNEG
      , () => xSHL(I)//ISHL
      , () => xSHL(L)//LSHL
      , () => xSHR(I)//ISHR
      , () => xSHR(L)//LSHR
      , () => xUSHR(I)//IUSHR
      , () => xUSHR(L)//LUSHR
      , () => xAND(I)//IAND
      , () => xAND(L)//LAND
      , () => xOR(I)//IOR
      , () => xOR(L)//LOR
      , () => xXOR(I)//IXOR
      , () => xXOR(L)//LXOR
      , () => IINC(Cell())
      , () => x2y(I,L)//I2L
      , () => x2y(I,F)//I2F
      , () => x2y(I,D)//I2D
      , () => x2y(L,I)//L2I
      , () => x2y(L,F)//L2F
      , () => x2y(L,D)//L2D
      , () => x2y(F,I)//F2I
      , () => x2y(F,L)//F2L
      , () => x2y(F,D)//F2D
      , () => x2y(D,I)//D2I
      , () => x2y(D,L)//D2L
      , () => x2y(D,F)//D2F
      , () => x2y(I,B)//I2B
      , () => x2y(I,C)//I2C
      , () => x2y(I,S)//I2S
      , () => LCMP
      , () => xCMPL(F)//FCMPL
      , () => xCMPG(F)//FCMPG
      , () => xCMPL(D)//DCMPL
      , () => xCMPG(D)//DCMPG
      , () => IFx(EQ)//IFEQ
      , () => IFx(NE)//IFNE
      , () => IFx(LT)//IFLT
      , () => IFx(GE)//IFGE
      , () => IFx(GT)//IFGT
      , () => IFx(LE)//IFLE
      , () => IF_ICMPx(EQ)//IF_ICMPEQ
      , () => IF_ICMPx(NE)//IF_ICMPNE
      , () => IF_ICMPx(LT)//IF_ICMPLT
      , () => IF_ICMPx(GE)//IF_ICMPGE
      , () => IF_ICMPx(GT)//IF_ICMPGT
      , () => IF_ICMPx(LE)//IF_ICMPLE
      , () => IF_ACMPx(EQ)//IF_ACMPEQ
      , () => IF_ACMPx(NE)//IF_ACMPNE
      , () => GOTO()
      , () => DEPRECATED("JSR")
      , () => DEPRECATED("RET")
      , () => TABLESWITCH()
      , () => LOOKUPSWITCH()
      , () => xRETURN(I)//IRETURN
      , () => xRETURN(L)//LRETURN
      , () => xRETURN(F)//FRETURN
      , () => xRETURN(D)//DRETURN
      , () => xRETURN(A)//ARETURN
      , () => RETURN
      , () => GETSTATIC()
      , () => PUTSTATIC()
      , () => GETFIELD()
      , () => PUTFIELD()
      , () => INVOKEVIRTUAL()
      , () => INVOKESPECIAL()
      , () => INVOKESTATIC()
      , () => INVOKEINTERFACE()
      , () => INVOKEDYNAMIC()
      , () => NEW()
      , () => NEWARRAY()
      , () => ANEWARRAY()
      , () => ARRAYLENGTH
      , () => ATHROW
      , () => CHECKCAST()
      , () => INSTANCEOF()
      , () => MONITORENTER
      , () => MONITOREXIT
      , () => UNUSED//wide(load/store/ret)
      , () => MULTIANEWARRAY()
      , () => IFNULL()
      , () => IFNONNULL()
    )
}
