package põder.framework.cfg

import põder.util.Util.Cell
import põder.util.typ.{MethodType, Parser}
import org.objectweb.asm._

import scala.collection.mutable
import scala.language.postfixOps
import scala.util.{Left, Right}

//object Node {
//  private val enter_map = mutable.Map.empty[(String,String,String),Node]
//  private val return_map = mutable.Map.empty[(String,String,String),Node]
//  def enterNode(cls:String, mthd: String, typ:String): Option[Node] = {
//    enter_map.get(cls,mthd,typ)
//  }
//  def returnNode(cls:String, mthd: String, typ:String): Option[Node] = {
//    return_map.get(cls,mthd,typ)
//  }
//  def registerMethod(cls:String, mthd: String, typ:String, enter:Node, ret:Node): Unit = {
//    enter_map += (cls,mthd,typ) -> enter
//    return_map += (cls,mthd,typ) -> ret
//  }
//}

case class MethodInfo(
  var is_static: Boolean = false,
  var cls: String = "not set",
  var mthd: String = "not set",
  var mthdTyp: MethodType = null,
  var access: Int = -1,
  var locals: Int = -1,
  var stackSize: Int = -1)
  extends Serializable


class Node(val methodInfo: MethodInfo, val id: Int)
  extends Serializable
{
  var str: String = ""
  val labels: mutable.Set[String] = mutable.Set.empty
  var lastLabel: Option[String] = None
  var is_start: Boolean = false
  var is_return: Boolean = false
  var is_exn: Boolean = false
  var offset: Int = -1
  var linenr: Int = -1
  var varNames: Map[Int,String] = Map()
  var annot: List[AnyRef] = List()

  override def toString: String =
    id.toString
//    str++(if (annot.isEmpty) "" else annot.mkString("[",",","]"))
}

// Here, we read in the method `name` and return it to the class-visitor using `ret`.
class MethodCFGPrinter(cls: String, access: Int, mthd: String, typ: String, ret: CFG.Graph[TFType[Annot]] => Unit,superName:String,exceptions:  Boolean)
  extends MethodVisitor(Opcodes.ASM5)
{
  // Nodes & edges
  type Et = TFType[Annot]

  type N = Node
  type E = CFG.E[Et]


  // node sets & edge set
  private val es = mutable.Buffer.empty[E]
  private val static = (access & Opcodes.ACC_STATIC) != 0
//  printf("access:%d   -->   static:%b\n", access, static)

  private val mi = MethodInfo(static,cls,mthd,Parser.mpar(typ),access)

  private var id_counter = 0
  // Creating new nodes.
  private def newNode(line: Int): N = {
    if (nextIsInvariant) {
      cur.annot = List(lastLDC.asInstanceOf[String])
      nextIsInvariant = false
      cur
    } else {
      val node = new Node(mi,id_counter)
      id_counter += 1
      node.linenr = line
      node
    }
  }

  private def newNode(s:String, line: Int): N = {
    val node = newNode(line)
    node.str = s
    node
  }

  private def newNode(label:Label, line: Int): N = {
    val node =  newNode(line)
    node.labels += label.toString
    node
  }

  // We have a map from our labels to nodes.
  // Although the labels are not strictly necessary,
  // we add them as they appear in the instructions.
  private val labels = mutable.Map.empty[Label, N]
  private def labelNode: Label => N = { l =>
    labels.getOrElseUpdate(l, newNode(l,-1))
  }

  // special nodes for start end return
  private var startNode  = newNode(s"start($mthd)", -1)
  private var secondNode = newNode("",-1)
  private var returnNode = newNode(s"return($mthd)",-1)
  private var exceptionNode = newNode(s"ex($mthd)",-1)

  // a set to keep track of all ATHROW nodes
  private var athrowNodes = mutable.Set.empty[N]

  // ex
  private val exTbl = mutable.Buffer.empty[(Label, Label, Label, String)]

  // This is the current or the previous node, depending on your perspective.
  private var cur = secondNode
  private var lab: Option[String] = None

  // A helper function that adds edges to the graph.
  private var equeue = mutable.Queue.empty[(N, Either[Instr,Annot], N)]
  private def pushEdgesInQueue(): Unit = {
    if (equeue.nonEmpty) {
      val first = equeue.head._1
      val last = equeue.last._3
      es.append(new CFG.E(first, last, BasicBlock(equeue.map(_._2).toList)))
      // If the BasicBlock (treated as one label node) ends with ATHROW, add the preceding node as an ATHROW node
      if (equeue.last._2 == Left(ATHROW)) {
        athrowNodes += first
      }
      equeue.clear()
    }
  }
  private def addEdge(from: N, to: N, s:Either[Instr,Annot], ft: Boolean = true): Unit = {
    to.varNames = from.varNames
    s match {
      case Left(instr) =>
        instr match {
          case i: MethodInstr =>
            pushEdgesInQueue()
            es.append(new CFG.E(from, to, Returns(i)))
          case i: JumpInstr =>
            pushEdgesInQueue()
            if (ft)
              es.append(new CFG.E(from, to, Fall(i)))
            else
              es.append(new CFG.E(from, to, Jump(i)))
          case _ =>
            equeue.enqueue((from, s, to))
            if (instr.isInstanceOf[RetInstr])
              pushEdgesInQueue()
        }
      case Right(annot) =>
        equeue.enqueue((from, s, to))
    }

  }

  // As labels come after instructions, we cannot push edges into the graph immediately.
  // First, we store the edge in `old` and then update the to node, if necessary.
  private var old: Option[(N,N,Either[Instr,Annot])] = None

  // This method pushes the `old` stored edge into the graph. It is used when we want to
  // clean up `old` to store a new edge.
  private def pushOld(): Unit = {
    for ((from,to,data) <- old) {
      from.lastLabel = lab
      if (data.isLeft && data.left.get.isInstanceOf[RetInstr]) {
        addEdge(from, returnNode , data)
      } else {
        addEdge(from, to, data)
        cur = to
      }
    }
    old = None
  }

  def propagateLocalVarName(i:Int, n:String, t:põder.util.typ.Type, start:Label, end: Label, es:mutable.Buffer[E]): Unit = {
    val visited = mutable.Set.empty[Node]
    def prop(s: Node) {
      visited += s
      s.varNames += i -> n
      for (e <- es if e.from == s && !visited.contains(e.to)) {
        prop(e.to)
      }
    }
    val s = labelNode(start)
    if (s == secondNode)
      prop(startNode)
    else
      prop(s)
  }


  // Called at the end of the method.
  private def pushEnd(): Unit = {
    if (old.isEmpty && es.isEmpty && mi.cls  == "java/lang/Throwable" && mi.mthd == "fillInStackTrace") {
      // some library methods have no implementation (empty list of instructions)
      // these should be handeled using isBuiltinFunction or by adding code here
      val mid = newNode(-1)
      es.append(new E(secondNode, mid, BasicBlock(List(Left(LDC(Cell("Põder-stack-trace-object")))))))
      old = Some((mid,returnNode,Left(xRETURN(A))))
    } else
      old = old.map{case (x,y,z) => (x,returnNode,z)}
    pushOld()
    pushEdgesInQueue()
    es.append(new E(startNode, secondNode, Start(mi.locals, mi.stackSize, access)))


    val edgeSet = es.filter{x =>
      x.data match {
        case Fall(GOTO(_)) => false
        case BasicBlock(block) => {
          block.last match {
            case Left(ATHROW) => false
            case _ => true
          }
        }
        case _ => true
      }
    }

    for ((l,n) <- labels) {
      n.labels += l.toString
    }
    var ns: Set[N] = Set(exceptionNode) ++ (for {e:E <- edgeSet.toSet; x <- Set(e.from, e.to)} yield x)

    startNode.is_start   = true
    returnNode.is_return = true
    exceptionNode.is_exn = true

    var n = 0
    val labelSeq: Map[String, Int] = labelSeqBuf.map{q => n+=1; q -> n}.toMap

    for (e <- es if !e.from.is_start){
      val is = e.data match {
        case BasicBlock(x) => x
        case Jump(i) => Seq()
        case Fall(i) => Seq()
        case Start(_, _, _) => Seq()
        case Returns(i) => Seq(Left(i))
        case Throws(i, _) => Seq()
      }
      if (is nonEmpty) {
        var exs = mutable.Set.empty[String]
        val lastLabel = e.from.lastLabel
        val filteredExTbl = exTbl.filter{case (st, en, gt, ex) => labelSeq(lastLabel.get) >= labelSeq(st.toString) && labelSeq(lastLabel.get) < labelSeq(en.toString)}
        val handlers = filteredExTbl.map(_._3).distinct // ordered set of all exception handlers
        var exFromNode = e.from
        for (handler <- handlers) {
          val toNode = labelNode(handler)
          // iterate over all the exceptions leading to this handler
          for (exEntry <- filteredExTbl if exEntry._3 == handler) {
            if (exceptions || is.last == Left(ATHROW) || is.last == Left(MONITOREXIT)) {
              edgeSet += new E(exFromNode, toNode, new Throws[Annot](is.toList, Left(exEntry._4)))
              exs += exEntry._4
            }
          }
          if (handler != handlers.last) {
            // for all handlers but the last one, add a label node and a new mid-node
            val newExNode = newNode(exFromNode.linenr)
            ns += newExNode
            edgeSet += new E(exFromNode, newExNode, new Throws[Annot](is.toList, Right(exs.toSet)))
            exFromNode = newExNode
            exs.clear()
          } else {
            // for the last handler, only add the label node, no new mid-node
            edgeSet += new E(exFromNode, exceptionNode, new Throws[Annot](is.toList, Right(exs.toSet)))

          }
        }

        if ((exceptions || is.last == Left(ATHROW)|| is.last == Left(MONITOREXIT)) && handlers.isEmpty) {
          edgeSet += new E(e.from, exceptionNode, new Throws[Annot](is.toList, Right(exs.toSet)))
        }
      }
    }

    annots foreach {
      case (l, e, LocalVar(i, m, t)) =>
        propagateLocalVarName(i,m,t,l,e,edgeSet)
    }

//    Node.registerMethod(cls,mthd,typ,startNode, returnNode)
    ret(new CFG.Graph(ns.toList, edgeSet.toList, startNode, returnNode, exceptionNode, athrowNodes.toList))
  }

  // Called when reading in the instruction `s`.
  private def pushNextEdge(s:Instr): Unit = {
    pushOld()
    old = Some(cur, newNode("",-1), Left(s))
  }

  private val annots = mutable.Buffer.empty[(Label, Label, Annot)]
  private def addAnnot(s:Annot, start:Label, end: Label) = {
    annots += ((start, end, s))
  }

  private val labelSeqBuf = mutable.Buffer.empty[String]
  // Called when reading in a label.
  private def pushLabel(l:Label): Unit = {
    labelSeqBuf += l.toString
    old = old.map { case
      (from, to, d) =>
        if (d.isLeft && !d.left.get.isInstanceOf[RetInstr]) {
          val nto = labelNode(l)
          (from, nto, d)
        } else {
          (from, to, d)
        }
    }
  }


  // Visitor functions

  // start of method code
  override def visitCode(): Unit = {
  }

  // generic instructions
  override def visitInsn(opcode: Int): Unit = {
    pushNextEdge(InstrUtil.INSTR(opcode)())
  }

  // ??? I do not know what this is
  override def visitFrame(`type`: Int, nLocal: Int, local: Array[AnyRef], nStack: Int, stack: Array[AnyRef]): Unit = {
//    printf("Frame(%d, %d, %s, %s, %s)\n", `type`, nLocal, local.mkString("[", ", ", "]"), nStack, stack.mkString("[",", ", "]"))
  }

  // field instructions
  override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String): Unit = {
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with FieldArg]
    i.a.write(owner, name, desc) // add the argument
    pushNextEdge(i)
  }

  // increment instructions
  override def visitIincInsn(`var`: Int, increment: Int): Unit = {
    pushNextEdge(IINC(Cell(`var`.toShort, increment.toShort)))
  }

  // integer instructions
  override def visitIntInsn(opcode: Int, operand: Int): Unit = {
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with IntArg]
    i.a.write(operand) // add the argument
    pushNextEdge(i)
  }

  // the INVOKEDYNAMIC instruction
  override def visitInvokeDynamicInsn(name: String, desc: String, bsm: Handle, bsmArgs: AnyRef*): Unit = {
    pushNextEdge(INVOKEDYNAMIC(Cell(("", name,desc)),Cell((bsm.getOwner,bsm.getName,bsm.getDesc,bsm.isInterface,bsmArgs.map(_.toString).toList))))
  }

  // jump instructions (arg is a label)
  override def visitJumpInsn(opcode: Int, label: Label): Unit = {
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with LabelArg]
//    val l = lableToString(label)
    i.a.write(label.toString)
    pushNextEdge(i)                           // add the fall-through edge
    addEdge(cur, labelNode(label), Left(i), ft = false) // add the jump edge
  }

  override def visitLineNumber(line: Int, start: Label): Unit = {
    cur.linenr = line
    cur.labels += start.toString
  }

  // label & line number
  override def visitLabel(start: Label): Unit = {
//    val ls = lableToString(start)
    pushLabel(start)
    if (cur == secondNode) {
      labels += start -> cur
    } else
      cur = labelNode(start)
    pushOld()
    lab = Some(start.toString)
    pushEdgesInQueue()
  }

  var lastLDC: Any = _

  // big constants
  override def visitLdcInsn(cst: scala.Any): Unit = {
    cst match {
      case x: Type => {
        põder.util.typ.Type.fromAsm(x) match {
          case Left(value)  => lastLDC = value
          case Right(value) => lastLDC = value
        }
      }
      case (_: Long) | (_: Float) | (_: Double) | (_: Integer) | (_:String) =>
        lastLDC = cst
    }
    pushNextEdge(LDC(Cell(lastLDC)))
  }

  // ???
  override def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int): Unit = {
    addAnnot(LocalVar(index, name, Parser.tpar(desc)._1), start, end)
//    printf("LocalVariable(%s, %s, %s, %s, %s, %d)\n", name, desc, signature, lableToString(start), lableToString(end), index)
  }

  // lookup switch instruction
  override def visitLookupSwitchInsn(dflt: Label, keys: Array[Int], labels: Array[Label]): Unit = {
//    pushNextEdge(LOOKUPSWITCH(Cell(lableToString(dflt),keys,labels)))
  }

  var nextIsInvariant = false

  // method instruction
  override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean): Unit = {
    if (owner=="P6der" && name=="invariant") {
      nextIsInvariant = true
      // TODO: Remove nop
      old = None // Some((old.get._1, old.get._2, Left[Instr,Annot](NOP)))
    } else {
      val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with MethodArg]
      i.a.write(owner, name, desc)
      pushNextEdge(i)
    }
  }

  // instrcutions with variable arguments
  override def visitVarInsn(opcode: Int, `var`: Int): Unit = {
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with VarArg]
    i.a.write(`var`.toByte)
    pushNextEdge(i)
  }

  // instructions with type arguments
  override def visitTypeInsn(opcode: Int, `type`: String): Unit = {
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr with TypeArg]
    i.a.write(`type`)
    pushNextEdge(i)
  }

//  ???
  override def visitTryCatchBlock(start: Label, end: Label, handler: Label, `type`: String): Unit = {
    exTbl += ((start, end, handler, `type`))
  }

  // table switch instructions
  override def visitTableSwitchInsn(min: Int, max: Int, dflt: Label, labels: Label*): Unit = {
//    pushNextEdge(TABLESWITCH(Cell(min, max,lableToString(dflt),labels.map(lableToString).toArray)))
  }

  override def visitParameter(name: String, access: Int): Unit = {
//    printf("Parameter(%s, %d)\n", name, access)
  }

  // multianewarray instruction --- need an example
  override def visitMultiANewArrayInsn(desc: String, dims: Int): Unit = {
//    printf("MultiANewArrayInsn(%s, %d)\n", desc, dims)
  }

  override def visitMaxs(maxStack: Int, maxLocals: Int): Unit = {
    mi.locals = maxLocals
    mi.stackSize = maxStack
  }


  override def visitEnd(): Unit = {
    pushEnd()
  }
}

// Now we simply collect the CFG of methods. Later, we may also collect class shapes.
class ClassCFGPrinter(val exceptions: Boolean) extends ClassVisitor(Opcodes.ASM7) {
  val methodGraphs = mutable.Map.empty[(String,String, MethodType), CFG.Graph[TFType[Annot]]]
  var packageName = ""
  var className = ""
  var superName = ""

  override def visit(version: Int, access: Int, name: String, signature: String, sup: String, interfaces: Array[String]): Unit = {
    val classNamePat = "(.*/)([^/]*)".r
    name match {
      case classNamePat(pkg, clss) => packageName = pkg
      case _ => ()
    }
    superName = sup
    className = name
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor = {
//    printf("Field(%d, %s, %s, %s, %s, %s)\n", access, name, desc, signature, value)
    null
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exc: Array[String]): MethodVisitor = {
    val ex = Option(exc) match {
      case Some(x) => x.mkString("[", ", ","]")
      case None    => "null"
    }
//    printf("Method(%d, %s, %s, %s, %s)\n", access, name, desc, signature, ex)
    new MethodCFGPrinter(className, access, name, desc, methodGraphs.update((className,name, Parser.mpar(desc)),_), superName, exceptions)
  }

  //  override def visitSource(source: String, debug: String): Unit = {
  //    println(s"$source")
  //  }

  override def visitEnd(): Unit = {
//    println("EndClass")
  }
}
