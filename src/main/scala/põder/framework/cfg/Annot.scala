package põder.framework.cfg

import põder.util.typ.Type

sealed abstract class Annot

case class LocalVar(i: Int, n: String, typ: Type) extends Annot {
  override def toString: String = s"LocalVar($i, $n, ${typ.toText})"
}