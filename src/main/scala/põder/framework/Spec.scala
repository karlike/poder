package põder.framework

import põder.framework.cfg.{MethodInstr, _}
import põder.util.PerfCount
import põder.util.json.JsonF
import põder.util.typ.{MethodType, Parser}

// Semantics of a CFG is a combination of edge and node semantics over a lattice.
// node => dl => Unit --- side-effect == fork
// node => dl         --- side-effect == fork
abstract class CfgSem[node,edge] {
  type key
  val Key: JsonOpF[key]
  val startKeys: Seq[key]

  type dl
  val L: Lattice[dl]

  def setVerificationMode(b:Boolean)

  val sNodes: PartialFunction[node, (node=>key=>dl) => (node=>key=>dl=>Unit) => key => dl]
  val sEdges: PartialFunction[edge, (node=>key=>dl) => (node=>key=>dl=>Unit) => (node, Boolean) => (node, Boolean) => (key => dl) => key => dl]

  def postProcess(): Unit
}

abstract class CfgMapSem[node,edge] {
  type key
  val Key: JsonOpF[key]
  val startKeys: Seq[key]

  type dl
  val L: Lattice[dl]

  def setVerificationMode(b:Boolean)

  val sNodes: PartialFunction[node, (node=>key=>dl) => (node=>key=>dl=>Unit) => (node, Boolean) => key => dl]
  val sEdges: PartialFunction[edge, (node=>key=>dl) => (node=>key=>dl=>Unit) => (node, Boolean) => (key => dl) => key => dl]

  def postProcess(): Unit
}


// We can compile a CFG and its semantics into a constraint system.
object MakeCSys {
  def apply[n,e](x: CfgSem[n,e], prev: n => Seq[(e, n)]): CSys[(n,x.key),x.dl] = new CSys[(n,x.key),x.dl] {

    override val L: Lattice[x.dl] = x.L
    override def tf(get: ((n, x.key)) => x.dl, set: ((n, x.key)) => x.dl => Unit, inVerification: Boolean = false)
                   (p: (n, x.key)): x.dl = {
      PerfCount.sub("transfer-fun") {
        x.setVerificationMode(inVerification)
        val get1: n => x.key => x.dl = n => k => get(n,k)
        def set1: n => x.key => x.dl => Unit = n => k => v => set((n,k))(v)
        val c = x.sNodes(p._1)(get1)(set1)(p._2)
        prev(p._1).map {
          case (edge, node) =>
            x.sEdges(edge)(get1)(set1)(node,true)(p._1, true)(get1(node))(p._2)
        }.foldLeft(c) { case (d1, d2) => L.join(d1, d2) }
      }
    }

    override def postProcess(): Unit = x.postProcess()
  }
}

// simple semantics of java bytecode edges
abstract class SimpleCfgBcSem[node](val forward: Boolean = true){
  protected var inVerification = false
  def setVerificationMode(b: Boolean): Unit = inVerification = b

  type dl >: Null
  implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl]

  def sNode   (get: node=>dl, set: node=>dl=>Unit) (nd:node): dl
  def sBranch (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(instr:JumpInstr, thn:Boolean):dl
  def sNormal (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(instr:Instr) :dl
  def sThrows (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(instr:Instr, ex:Either[String,Set[String]]): dl
  def sAnnot  (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(ant: Annot): dl
  def sStart  (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(locals:Int, stack:Int, access:Int): dl
  def senter  (get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(instr: MethodInstr, typ:MethodType, static:Boolean): Seq[dl]
  def scombine(get: node=>dl, set: node=>dl=>Unit, to: node, to_final:Boolean, st: dl)(instr: MethodInstr, typ:MethodType, static:Boolean,caller:dl): dl

  def isBuiltinFunction(instr: MethodInstr, typ:MethodType, static:Boolean): Boolean = false

  def postProcess(): Unit = ()
}

// map-based analysis
abstract class MapAnalysis[node](val forward: Boolean = true) {
  protected var inVerification = false
  def setVerificationMode(b: Boolean): Unit = inVerification = b

  type key
  val Key: JsonOpF[key]
  val startKeys: Seq[key]

  type dl >: Null
  implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl]

  def sNode   (get: node=>key=>dl, set: node=>key=>dl=>Unit) (k:key,nd:node): dl
  def sBranch (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,instr:JumpInstr, thn:Boolean):dl
  def sNormal (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,instr:Instr) :dl
  def sThrows (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,instr:Instr, ex:Either[String,Set[String]]): dl
  def sAnnot  (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,ant: Annot): dl
  def sStart  (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,locals:Int, stack:Int, access:Int): dl
  def senter  (get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,instr: MethodInstr, typ:MethodType, static:Boolean): Seq[dl]
  def scombine(get: node=>key=>dl, set: node=>key=>dl=>Unit, from:node, from_start: Boolean, to: node, to_final:Boolean, st: key=>dl)(k:key,instr: MethodInstr, typ:MethodType, static:Boolean,caller:key=>dl): dl

  def isBuiltinFunction(instr: MethodInstr, typ:MethodType, static:Boolean): Boolean = false

  def postProcess(): Unit = ()
}

object MapAnalysis2Sem {

  def apply[node](s: MapAnalysis[node],
                  getMethodStartNode: (String, String, MethodType) => node,
                  getMethodReturnNode: (String, String, MethodType) => node,
                  getMethodThrowingNodes: (String, String, MethodType) => List[node],
                  reverseBB: Boolean)
  = new CfgSem[node, TFType[Annot]] {

    import s._

    override type key = s.key
    override val Key: JsonOpF[key] = s.Key

    override type dl = s.dl
    override val L: Lattice[dl] = s.L

    override val startKeys: Seq[s.key] = s.startKeys

    override def setVerificationMode(b:Boolean): Unit = s.setVerificationMode(b)

    override val sNodes: PartialFunction[node, (node => key => dl) => (node => key => dl => Unit) => key => dl] =
      { case nd => get => set => k => s.sNode(get,set)(k,nd) }

    private def checkBuiltin: PartialFunction[(MethodInstr,MethodType,Boolean), Option[String]] = {
      case (INVOKEDYNAMIC(_,_),_,_) => None
      case (m,y,b) if m.a.x.isDefined =>
        if (m.a.x.get._1.startsWith("[")) {
          if (m.a.x.get._1 == "clone")
            None
          else
            Some("java/lang/Object")
        } else {
          if (isBuiltinFunction(m, y, b)) None else Some(m.a.x.get._1)
        }
      case _ => None
    }

    def iterInstr(xs:Seq[Either[Instr,Annot]]): (node => key => dl) => (node => key => dl => Unit) => (node, Boolean) => (node, Boolean) => (key => dl) => key => dl = get => set => (nodef, _) => (nodet, _) => d => {
      if (xs.isEmpty)
        d
      else if (xs.length == 1) {
        xs.head match {
          case Left(instr) => k => sNormal(get,set,nodef,from_start = true, nodet,to_final = true, d)(k,instr)
          case Right(annot) => k => sAnnot(get,set,nodef,from_start = true, nodet,to_final = true, d)(k,annot)
        }
      } else {
        val d0: key => dl = xs.head match {
          case Left(instr) => k => sNormal(get,set,nodef,from_start = true, nodet,to_final = false, d)(k,instr)
          case Right(annot) => k => sAnnot(get,set,nodef,from_start = true, nodet,to_final = false, d)(k,annot)
        }
        val d1 = xs.tail.init.foldLeft(d0) {
          case (e, Left(instr)) => k => sNormal(get,set,nodef,from_start = false,nodet,to_final = false,e)(k,instr)
          case (e, Right(annot)) => k => sAnnot(get,set,nodef,from_start = false,nodet,to_final = false,e)(k,annot)
        }
        xs.last match {
          case Left(instr) => k => sNormal(get,set,nodef,from_start = false,nodet,to_final = true,d1)(k,instr)
          case Right(annot) => k => sAnnot(get,set,nodef,from_start = false,nodet,to_final = true,d1)(k,annot)
        }
      }
    }

    val tfEdges: PartialFunction[TFType[Annot], (node => s.key => s.dl) => (node => s.key => s.dl => Unit) => (node, Boolean) => (node, Boolean) => (s.key => s.dl) => s.key => s.dl] = {
      case BasicBlock(xs) if reverseBB => iterInstr(xs.reverse)
      case BasicBlock(xs) => iterInstr(xs)
      case Jump(instr) => get => set => (fr,fr_s) => (to,to_f) => st => k => sBranch(get,set,fr,fr_s,to,to_f,st)(k,instr,thn = true)
      case Fall(instr) => get => set => (fr,fr_s) => (to,to_f) => st => k => sBranch(get,set,fr,fr_s,to,to_f,st)(k,instr,thn = false)
      case Start(locals, start, access) => get => set => (fr,fr_s) => (to,to_f) => st => k => s.sStart(get,set,fr,fr_s,to,to_f,st)(k,locals,start,access)
      case Returns(instr) => get => set => (fr,fr_s) => (to, to_f) => d => k =>
        if (L.leq(d(k), L.bot))
          L.bot
        else if (instr.a.x.isEmpty)
          L.top
        else {
          val (_, m, t) = instr.a.x.get
          val types = põder.util.typ.Parser.mpar(t)
          val isStatic = instr.isInstanceOf[INVOKESTATIC]
          checkBuiltin((instr, types, isStatic)) match {
            case None =>
              sNormal(get,set,fr,fr_s,to,to_f,d)(k,instr)
            case Some(k1) =>
              val startNode = getMethodStartNode(k1, m, Parser.mpar(t))
              val returnNode = getMethodReturnNode(k1, m, Parser.mpar(t))
              L.Join {
                for {q <- senter(get,set,fr,fr_s,startNode,to_final = to_f,d)(k,instr, types, isStatic)} yield {
                  set(startNode)(k)(q)
                  scombine(get,set,fr,fr_s,to,to_f,get(returnNode))(k,instr,types,isStatic,d)
                }
              }
          }
        }
      // TODO: "main" method ATHROW nodes currently not processed. Analyse all ATHROW nodes elsewhere, only combine here.
      // TODO: only combine with the results of the branches throwing this particular exception, not all ATHROW branches
      case Throws(xs, exception) => get => set => (fr, fr_s) => (to, to_f) => d => {
        // xs = list of instructions that can throw this exception
        xs.foldLeft(d) {
          case (e, Left(instr)) => k => sThrows(get,set,fr,fr_s,to,to_f,d)(k,instr, exception)
          case (e, Right(an)) => k => sAnnot(get,set,fr,fr_s,to,to_f,d)(k,an)
        }
      }
    }

    override val sEdges: PartialFunction[TFType[Annot], (node => key => dl) => (node => key => dl => Unit) => (node, Boolean) => (node, Boolean) => (key => dl) => key => dl] = {
      case t: BasicBlock[Annot] => PerfCount.sub("normal")  { tfEdges(t) }
      case t: Jump[Annot]       => PerfCount.sub("jump")    { tfEdges(t) }
      case t: Fall[Annot]       => PerfCount.sub("fall")    { tfEdges(t) }
      case t: Start[Annot]      => PerfCount.sub("start")   { tfEdges(t) }
      case t: Returns[Annot]    => PerfCount.sub("returns") { tfEdges(t) }
      case t: Throws[Annot]     => PerfCount.sub("throw")   { tfEdges(t) }
    }

    override def postProcess(): Unit = s.postProcess()
  }
}


object SimpleCfgBcSem2MapAnalysis {

  def apply[node](s: SimpleCfgBcSem[node]): MapAnalysis[node] = new MapAnalysis[node] {
    override type key = Unit
    override val Key: JsonOpF[Unit] = new JsonOpF[Unit] {
      override def toJson[jv](x: Unit)(implicit j: JsonF[jv]): jv = j.Null
    }
    override val startKeys: Seq[Unit] = Seq(())
    override type dl = s.dl
    override implicit val L: Lattice[dl] with WidenOpF[dl] with NarrowOpF[dl] = s.L

    override def sNode(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit)(k: Unit, nd: node): s.dl = {
      val get1:node => dl = n => get(n)(())
      val set1:node => dl => Unit = n => v => set(n)(())(v)
      s.sNode(get1,set1)(nd)
    }

    override def sBranch(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, instr: JumpInstr, thn: Boolean): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.sBranch(get1, set1, to, to_final, st(()))(instr, thn)
    }


    override def sNormal(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, instr: Instr): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.sNormal(get1, set1, to, to_final, st(()))(instr)
    }

    override def sThrows(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, instr: Instr, ex: Either[String, Set[String]]): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.sThrows(get1, set1, to, to_final, st(()))(instr, ex)
    }

    override def sAnnot(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, ant: Annot): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.sAnnot(get1, set1, to, to_final, st(()))(ant)
    }

    override def sStart(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, locals: Int, stack: Int, access: Int): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.sStart(get1, set1, to, to_final, st(()))(locals,stack,access)
    }

    override def senter(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, instr: MethodInstr, typ: MethodType, static: Boolean): Seq[s.dl] = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.senter(get1, set1, to, to_final, st(()))(instr,typ,static)
    }

    override def scombine(get: node => Unit => s.dl, set: node => Unit => s.dl => Unit, fr: node, fr_s: Boolean, to: node, to_final: Boolean, st: Unit => s.dl)(k: Unit, instr: MethodInstr, typ: MethodType, static: Boolean, caller: Unit => s.dl): s.dl = {
      val get1: node => dl = n => get(n)(())
      val set1: node => dl => Unit = n => v => set(n)(())(v)
      s.scombine(get1, set1, to, to_final, st(()))(instr,typ,static,caller(()))
    }

    override def isBuiltinFunction(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean = s.isBuiltinFunction(instr, typ, static)
  }
}
