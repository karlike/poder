package põder.framework.solver

import põder.framework._
import põder.log.Log

import scala.collection.mutable

object SelectSolver {
  abstract class Slvr {
    def apply[Var,D >: Null]:
      ( CSys[Var, D],
        Var => Option[(D,Boolean)],
        Lattice[D] with WidenOpF[D] with NarrowOpF[D],
        () => Unit,
        () => Unit) => Solver[Var,D]
  }
  val allSol: Map[String, Slvr] = Map{
    "slr" -> new Slvr { override def apply[Var, D >: Null] = new WLSolver [Var,D](_,_,_,_,_) }
//    "rec" -> new Slvr { override def apply[Var, D >: Null] = new RecSolver[Var,D](_,_,_,_,_) }
//    "td"  -> new Slvr { override def apply[Var, D >: Null] = new TDSolver [Var,D](_,_,_,_,_) }
  }
}

class SelectSolver[Var, D >: Null]
  ( solverName: String,
    p: Var => Option[(D,Boolean)],
    s: CSys[Var, D],
    L: Lattice[D] with WidenOpF[D] with NarrowOpF[D],
    onWait:  () => Unit,
    onFinished:() => Unit
   )
{
  private val loadedResult: mutable.Map[Var,Boolean] = mutable.Map.empty
  private def cacheLoad(f: Var => Option[(D,Boolean)]): Var => Option[(D,Boolean)] = v => {
    val p = f(v)
    p.foreach(q => loadedResult += v -> q._2)
    p
  }

  private[this] val sol = {
    def getSol(name:String) = SelectSolver.allSol(name).apply(s,cacheLoad(p),L,onWait,onFinished)
    if (SelectSolver.allSol isDefinedAt solverName) {
      Log.logCommon(s"Solving using '$solverName'")
      getSol(solverName)
    } else {
      getSol("slr")
    }
  }

  def solve(vs: Set[Var]): Map[Var, D] = {
    val r = sol.solve(vs)
    onFinished()
    r
  }

  def stopButton(): Unit  = sol.stopButton()
  def runButton(): Unit  = sol.runButton()
  def stepButton(): Unit = sol.stepButton()
  def killButton(): Unit = sol.killButton()
  def currentNode: Option[Var] = sol.currentNode
  def updatedSet: Set[Var] = sol.updatedSet
  def widenedSet: Set[Var] = sol.widenedSet
  def clearSets(): Unit = sol.clearSets()
  def st: collection.Map[Var, D] = sol.st


  def verify(ds: Map[Var, D]): Boolean = {
    var clean = true
    def checkLeq(v: Var, d: D, d1: D): Unit = {
      if (!L.leq(d1, d)) {
        println(s"Verification failed for $v, value:\n$d1\nshould be smaller than:\n$d")
        clean = false
      }
    }

    def get(x: Var, y: Var): D = {
      ds.getOrElse(y, {
        println(s"Verification failed for $x, requires variable $y")
        clean = false
        L.bot
      })
    }

    def set(x: Var, y: Var): D => Unit = d => {
      val d1 = ds.getOrElse(y, {
        println(s"Verification failed for $x, sets variable $y")
        clean = false
        L.bot
      })
      checkLeq(x, d1, d)
    }

    for ((v, d) <- ds if !(loadedResult.get(v) contains true)) {
      Log.pushNode(v)
      val d1 = s.tf(get(v, _), set(v, _), inVerification = true)(v)
      Log.popNode()
      checkLeq(v, d, d1)
    }

    clean
  }

  def verify_reachable(previousSol: Var => Option[(D, Boolean)], startVars: Set[Var]): Boolean = {
    var clean = true
    val ds = mutable.Set.empty[Var]
    var vars = startVars

    def checkLeq(v: Var, d: D, d1: D): Unit = {
      if (!L.leq(d1, d)) {
        println(s"Verification failed for $v, value:\n$d1\nshould be smaller than:\n$d")
        clean = false
      }
    }

    def get(x: Var, y: Var): D = {
      solve_one(y)
    }

    def set(x: Var, y: Var): D => Unit = d => {
      vars += y
    }

    def solve_one(v: Var): D = {
      Log.pushNode(v)
      var d1 = L.bot
      if (!(ds contains v)) {
        ds += v
        d1 = s.tf(get(v, _), set(v, _), inVerification = true)(v)
      }
      val prevSol = previousSol(v)
      Log.popNode()
      if (prevSol.isDefined) {
        val d = prevSol.get._1
        checkLeq(v, d, d1)
        d
      } else {
        L.bot
      }
    }

    while (vars.nonEmpty) {
      val v = vars.head
      vars -= v
      if (!(loadedResult.get(v) contains true)) {
        solve_one(v)
      }
    }
    clean
  }
}
