package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.PerfCount

import scala.collection.mutable

class RecSolver[Var, D >: Null](
   s: CSys[Var, D],
   p: Var => Option[(D,Boolean)],
   L: Lattice[D] with WidenOpF[D] with NarrowOpF[D],
   onWait:  () => Unit,
   onFinished:() => Unit)
  extends Solver[Var,D](s, L, onWait, onFinished)
{
  def getOldVal(v:Var):D = p(v).map(_._1).getOrElse(L.bot)

  var run = false
  var step = false
  var kill = false
  var stop = false

  def stopButton(): Unit = {
    stop = true
    run  = false
    step = false
  }
  def runButton(): Unit = {
    stop = false
    run = true
  }
  def stepButton(): Unit = {
    stop = false
    step = true
  }
  def killButton(): Unit = kill = true

  val updated = mutable.Set.empty[Var]
  override def updatedSet: Set[Var] = updated.toSet

  val widened = mutable.Set.empty[Var]
  override def widenedSet: Set[Var] = widened.toSet

  override def clearSets(): Unit = {
    updated.clear()
    widened.clear()
  }

  override val st   = mutable.Map.empty[Var, D]
  val infl = mutable.Map.empty[Var, Set[Var]]
  val stbl = mutable.Set.empty[Var]
  val ord  = mutable.Map.empty[Var, Int]
  var ordmax = 10

  var currentNode: Option[Var] = None

  def box(x: D, y: D): D = {
    if (L.leq(y, x)) L.narrow(x, y) else L.widen(x, y)
  }

  def solve(vs: Set[Var]): Map[Var, D] = {
    def eval(x: Var)(y:Var): D = {
      infl(y) = infl.getOrElse(y,Set.empty) + x
      sol(y)
      if (ord(y) <= ord(x))
        widened += x
      st(y)
    }

    def interWaitLoop(v: Var): Unit = {
      PerfCount.timeout()
      currentNode = Some(v)

      if (!run && !step )
        onWait()


      while (!run && !step && !kill) {
        Thread.sleep(100)
      }

      if (!kill) {
        step = false
      }
      PerfCount.timein()
    }

    def set(wait:Var=>Unit)(v:Var)(d:D): Unit = {
      sol(v)
      val old = st.getOrElseUpdate(v,getOldVal(v))
      val test = !(L.leq(d, old) && L.leq(old, d))// d != old
      if (test){
        Log.log("updating value ...")
        updated += v
        if (widened contains v)
          st(v) = box(old, d)
        else
          st(v) = d
        wait(v)
        val vinfl = infl.getOrElse(v,Set())
        infl(v) = Set()
        vinfl foreach (stbl -= _)
        vinfl foreach sol
      }
    }


    def sol(v:Var):Unit = {
      if (stop) {
        stop = false
        run  = false
        step = false
        interWaitLoop(v)
      }

      if (!(ord contains v)) {
        ord(v) = ordmax
        ordmax += 1
      }

      if (!(st contains v)) {
        p(v) match {
          case Some((d,certain)) =>
            st(v) = d
            if (certain) {
              stbl += v
              return
            }
          case None =>
            st(v) = L.bot
        }
      }

      if (stbl contains v)
        return

      stbl += v

      Log.pushNode(v)
      whileNoException(interWaitLoop(v)){
        set(interWaitLoop)(v)(s.tf(eval(v), set(_ => ()))(v))
      }
      Log.popNode()
    }

    vs foreach sol

    if (run)
      onWait()

//    printf("valmis!\n")
    st.toMap
  }
}
