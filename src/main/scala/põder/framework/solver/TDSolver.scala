package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.PerfCount

import scala.collection.mutable

class TDSolver[Var, D >: Null](
                         s: CSys[Var, D],
                         p: Var => Option[(D,Boolean)],
                         L: Lattice[D] with WidenOpF[D] with NarrowOpF[D],
                         onWait:  () => Unit,
                         onFinished:() => Unit)
  extends Solver[Var,D](s, L, onWait, onFinished)
{
  def getOldVal(v:Var):D = p(v).map(_._1).getOrElse(L.bot)

  var run = false
  var step = false
  var kill = false
  var stop = false

  def stopButton(): Unit = {
    stop = true
    run  = false
    step = false
  }
  def runButton(): Unit = {
    stop = false
    run = true
  }
  def stepButton(): Unit = {
    stop = false
    step = true
  }
  def killButton(): Unit = kill = true


  val updated = mutable.Set.empty[Var]
  val called = mutable.Set.empty[Var]
  val stable = mutable.Set.empty[Var]
  val wpoint = mutable.Set.empty[Var]
  val widened = mutable.Set.empty[Var]

  val sigma = mutable.Map.empty[Var, D]
  val infl  = mutable.Map.empty[Var, Set[Var]]

  def box(v:Var)(x: D, y: D): D = {
    if (L.leq(y, x)) L.narrow(x, y) else {
      updated += v
      L.widen(x, y)
    }
  }

    def solve(x: Var): Unit = {
      def interWaitLoop(v: Var): Unit = {
        PerfCount.timeout()
        currentNode = Some(v)

        if (!run && !step)
          onWait()


        while (!run && !step && !kill) {
          Thread.sleep(100)
        }

        if (!kill)
          step = false

        PerfCount.timein()
      }
      def set(wait: Var => Unit)(y: Var)(d: D): Unit = {
        if (sigma(x) != d) {
          sigma += x -> d
          updated += x
          destabilize(x)
          solve(x)
        }
      }
      def eval(y: Var): D = {
        if (called contains y) wpoint += y
        solve(y)
        infl += y -> (infl(y) ++ Set(x))
        sigma(y)
      }
      if (stop) {
        stop = false
        run  = false
        step = false
        interWaitLoop(x)
      }
      p(x) match {
        case Some((d,certain)) =>
          sigma(x) = d
          if (certain) {
            stable += x
            return
          }
      }

      if ((!(stable contains x)) || (called contains x)) {
        called += x
        stable += x
        var tmp: D = null
        if (wpoint contains x){
          wpoint -= x
          Log.pushNode(x)
          tmp = whileNoException(interWaitLoop(x)){
            box(x)(sigma(x),s.tf(eval,set(interWaitLoop))(x))
          }
          Log.popNode()
          infl += x -> (infl(x) ++ Set(x))
        } else {
          Log.pushNode(x)
          tmp = s.tf(eval, set(interWaitLoop))(x)
          Log.popNode()
        }
        called -= x
        set(interWaitLoop)(x)(tmp)
      }
    }

    def destabilize(x: Var): Unit = {
      val w = infl(x)
      infl += x -> Set()
      for (y <- w if stable contains y) {
        stable -= y
        destabilize(y)
      }
    }

  override def updatedSet: Set[Var] = updated.toSet
  override def widenedSet: Set[Var] = wpoint.toSet
  override def clearSets(): Unit = {
    updated.clear()
    widened.clear()
  }
  override def st: collection.Map[Var, D] = sigma.toMap
  var currentNode: Option[Var] = None

  override def solve(vs: Set[Var]): Map[Var, D] = {
    vs.foreach(solve)
    if (run)
      onWait()
    sigma.toMap
  }
}
