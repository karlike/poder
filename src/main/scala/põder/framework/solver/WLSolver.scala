package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.PerfCount

import scala.collection.mutable

final class WLSolver[Var, D >: Null](
   s: CSys[Var, D],
   p: Var => Option[(D,Boolean)],
   L: Lattice[D] with WidenOpF[D] with NarrowOpF[D],
   onWait:  () => Unit,
   onFinished:() => Unit)
  extends Solver[Var,D](s, L, onWait, onFinished)
{
  def getOldVal(v:Var):D = p(v).map(_._1).getOrElse(L.bot)

  var run = false
  var step = false
  var kill = false
  var stop = false

  def stopButton(): Unit = {
    stop = true
    run  = false
    step = false
  }
  def runButton(): Unit = {
    stop = false
    run = true
  }
  def stepButton(): Unit = {
    stop = false
    step = true
  }
  def killButton(): Unit = kill = true

  val updated = mutable.HashSet.empty[Var]
  override def updatedSet: Set[Var] = updated.toSet

  val widened = mutable.HashSet.empty[Var]
  override def widenedSet: Set[Var] = widened.toSet

  override def clearSets(): Unit = {
    updated.clear()
    widened.clear()
  }

  override val st = mutable.HashMap.empty[Var, D]

  val sided = mutable.HashSet.empty[Var]
  val worklist = mutable.HashSet.empty[Var]
  val infl = mutable.HashMap.empty[Var, Set[Var]]
  val stbl = mutable.HashSet.empty[Var]
  val ord  = mutable.HashMap.empty[Var, Int]
  var ordmax = 10

  var currentNode: Option[Var] = None

  def box(x: D, y: D): D = {
    if (L.leq(y, x)) L.narrow(x, y) else L.widen(x, y)
  }

  def eval(x: Var)(y:Var): D = {
    if (!st.contains(y))
      solve_one(y)
    infl(y) = infl.getOrElse(y, Set.empty) + x
    if ((ord contains x) && (ord contains y)) {
      if (ord(y) >= ord(x))
        widened += x
    } else {
      if (! (ord contains y)) {
        ord(y) = ordmax
        ordmax += 1
        ord(x) = ordmax
        ordmax+= 1
      }
    }
    st(y)
  }

  def interWaitLoop(v: Var): Unit = {
    PerfCount.timeout()
    currentNode = Some(v)

    if (!run && !step)
      onWait()


    while (!run && !step && !kill) {
      Thread.sleep(100)
    }

    if (!kill)
      step = false

    PerfCount.timein()
  }

  def set(wait:Var=>Unit)(v:Var)(d:D): Unit = {
    if (!st.contains(v))
      solve_one(v)
    val old = st.getOrElseUpdate(v, getOldVal(v))
    val d1 =
      if (widened contains v) box(old, d)
      else if (sided contains(v)) L.join(old,d)
      else d
    val test = !L.equal(d1, old) // d != old
    if (test) {
//      Log.log("updating value ...")
      updated += v
      st(v) = d1
      wait(v)
      val vinfl = infl.getOrElse(v, Set()) ++ (if (widened contains v) Set(v) else Set())
      infl(v) = Set()
      vinfl foreach (stbl -= _)
      worklist ++= vinfl
    } else {
      wait(v)
    }
  }

  def side(v:Var)(d:D): Unit = {
    sided += v
    set((_ => ()))(v)(d)
  }

  def solve_one(v:Var):Unit = {
    if (stop) {
      stop = false
      run  = false
      step = false
      interWaitLoop(v)
    }

    if (!(st contains v)) {
      p(v) match {
        case Some((d,certain)) =>
          st(v) = d
          if (certain) {
            stbl += v
            return
          }
        case None =>
          st(v) = L.bot
      }
    }

    if (stbl contains v)
      return

    stbl += v

    Log.pushNode(v)
    whileNoException(interWaitLoop(v)) {
      val d = s.tf(eval(v), side)(v)
      set(interWaitLoop)(v)(d)
    }
    Log.popNode()
  }

  def solve(vs: Set[Var]): Map[Var, D] = {
    worklist ++= vs
    while (worklist.nonEmpty) {
      val h = worklist.head
      worklist -= h
      solve_one(h)
    }

    if (run) {
      PerfCount.timeout()
      onWait()
      PerfCount.timein()
    }

    // printf("valmis!\n")

    st.toMap
  }
}
