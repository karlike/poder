package põder.framework.solver

import põder.framework._
import põder.log.Log

abstract class Solver[Var,D >: Null]
  (s: CSys[Var, D],
   L: Lattice[D] with WidenOpF[D] with NarrowOpF[D],
   onWait:  () => Unit,
   onFinished:() => Unit)
{
  def stopButton(): Unit
  def runButton() : Unit
  def stepButton(): Unit
  def killButton(): Unit
  def updatedSet  : Set[Var]
  def widenedSet  : Set[Var]
  def clearSets() : Unit
  def st          : collection.Map[Var, D]
  def currentNode : Option[Var]

  def whileNoException[G,F](wait: =>G)(f: =>F):F = {
    try {
      f
    } catch {
      case _: BreakTransferFunction =>
        wait
        whileNoException(wait)(f)
    }
  }

  def solve(vs: Set[Var]): Map[Var, D]
}
