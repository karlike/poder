package põder.util.json

import scalafx.scene.control.TreeItem

trait JsonF[jval] {
  def Obj(ms: (String,jval)*): jval
  def Obj(m: Traversable[(String,jval)]): jval
  def Arr(a: jval*): jval
  def Arr(as: Traversable[jval]): jval
  def Bool(b: Boolean): jval
  def Str(s:String): jval
  def Num(i:BigDecimal): jval
  def Null: jval

  def fromVal[T](x:T): jval
}

sealed abstract class JVal {
  def toText: String
}

object Json extends JsonF[JVal] {
  case class JObj(m: Map[String, JVal]) extends JVal {
    override def toText: String = {
      if (m.isEmpty)
        "{}"
      else if (m.toSeq.length == 1)
        m.map { case (k, v) => k ++ ": " ++ v.toText }
          .mkString("{", "," , "}")
      else
        m.map { case (k, v) => k ++ ": " ++ v.toText }
          .mkString("{\n", ",\n", "}")
    }
  }

  case class JArr(a: Array[JVal]) extends JVal {
    override def toText: String =
      a.map(_.toText).mkString("[", "\n, ", "]")
  }

  case class JBool(b: Boolean) extends JVal {
    override def toText: String =
      b.toString
  }

  case class JStr(s: String) extends JVal {
    override def toText: String = s
  }

  case class JNum(i: BigDecimal) extends JVal {
    override def toText: String = i.toString
  }

  case object JNull extends JVal {
    override def toText: String = "null"
  }

  def fromVal[T](y: T): JVal = y match {
    case null            => JNull
    case m:Map[_,_] => JObj(m.map{case (k,v) => k.toString -> fromVal(v)})
    case q:Array[_]      => JArr(q.map(fromVal))
    case b:Boolean       => JBool(b)
    case s:String        => JStr(s)
    case i:Int           => JNum(BigDecimal(i))
    case i:BigDecimal    => JNum(i)
    case _               => JStr(y.toString)
  }

  override def Obj(m: (String, JVal)*): JVal = JObj(m.toMap)
  override def Obj(m: Traversable[(String, JVal)]): JVal = JObj(m.toMap)
  override def Arr(a: JVal*): JVal = JArr(a.toArray)
  override def Arr(as: Traversable[JVal]): JVal = JArr(as.toArray)
  override def Bool(b: Boolean): JVal = JBool(b)
  override def Str(s: String): JVal = JStr(s)
  override def Num(i: BigDecimal): JVal = JNum(i)
  override def Null: JVal = JNull

  def toTreeItems(v: JVal): Seq[TreeItem[String]] =
    v match {
      case JObj(m)  =>
        var pa = List.empty[TreeItem[String]]
        for {(n,o) <- m} {
          val ti = new TreeItem(n)
          ti.expanded = true
          toTreeItems(o).foreach(ti.children.append(_))
          pa :+= ti
        }
        pa
      case JArr(a)  =>
        a.flatMap(toTreeItems)
//        val pa = new TreeItem("array")
//        pa.expanded = true
//        for {o <- a} {
//          toTreeItems(o).foreach(pa.children.append(_))
//        }
//        Seq(pa)
      case JBool(b) => Seq(new TreeItem(b.toString))
      case JStr(s)  => Seq(new TreeItem(s))
      case JNum(i)  => Seq(new TreeItem(i.toString))
      case JNull    => Seq(new TreeItem("null"))
    }

}


