package põder.util

import java.io.{FileInputStream, FileOutputStream, NotSerializableException, ObjectInputStream, ObjectOutputStream}

import põder.framework.cfg.{Annot, CFG, Node, TFType}
import põder.util.typ.MethodType

import scala.collection.mutable
import scala.reflect.io.{File, Path}

object Project {
  def apply(proj_path: Path): Option[Project] = {
    val proj = File(proj_path).createDirectory()
    if (!proj.exists)
      return None
    val cf = proj./(Path("conf"))
    if (!cf.exists)
      return None
    val cfs = new FileInputStream(cf.jfile)
    val ip = new ObjectInputStream(cfs)
    val (path,lib, poder_ver, java_ver, source_base, mainClass, analysisName, exceptions, juliet, stable) =
      ip.readObject().asInstanceOf[(String, String, String, String, String, String, String, Boolean, Boolean, Boolean)]
    ip.close()
    val p = new Project(path, lib)
    if (p.java_ver == java_ver) {
      p.poder_ver = poder_ver
      p.java_ver = java_ver
      p.source_base = source_base
      p.mainClass = mainClass
      p.analysisName = analysisName
      p.exceptions = exceptions
      p.juliet = juliet
      p.stable = stable
      Some(p)
    } else {
      None
    }
  }
}

final class Project
  (val path: String,
   var lib_path : String,
   val no_store: Boolean = false)
  extends Serializable
{
  var poder_ver: String                = "1.0"
  var java_ver : String                = System.getProperty("java.version")
  val graphs  : mutable.Map[(String,String,MethodType),CFG.Graph[TFType[Annot]]] = mutable.Map.empty
  val superMap: mutable.Map[String, String] = mutable.Map.empty

  var source_base  = ""

  var mainClass    = "Main"
  var analysisName = "modular"
  var exceptions   = false
  var juliet       = false
  var stable       = false

  val libPrefixes = Set("java/", "javax/","org/ietf/jgss", "org/omg", "org/w3c/dom", "org/xml", "javafx/", "jdk/", "sun/nio/")
  def isLibrary(cls:String): Boolean = {
    libPrefixes.exists(cls.startsWith)
  }

  def fullMainPath: String = {
    source_base ++ File.separator ++ mainClass.replace(".",File.separator) ++ ".class"
  }

  def versionPath: String = {
    File.separator ++ "poder_v" ++ poder_ver ++ "_" ++ java_ver
  }

  def analysisPath: String = {
     versionPath ++ File.separator ++ analysisName
  }

  def createOnDisk(): Unit = {
    if (no_store)
      return
    val proj = (Path(path) / Path(analysisPath)).createDirectory()
    if (proj.exists) {
      val cf = proj./(Path("conf")).createFile(false)
      val cfs = new FileOutputStream(cf.jfile)
      val op = new ObjectOutputStream(cfs)
      val data: (String, String, String, String, String, String, String, Boolean, Boolean, Boolean) =
        (path, lib_path, poder_ver, java_ver, source_base, mainClass, analysisName, exceptions, juliet, stable)
      op.writeObject(data)
      op.close()
    }
  }

  def loadGraph(cls:String, mthd: String, typ: MethodType): Option[CFG.Graph[TFType[Annot]]] = {
    if (cls==null)
      return None
    def loadFile: Option[CFG.Graph[TFType[Annot]]] = {
      val dirname = cls.replace("/", ".")++"."++mthd++typ.toShort.replace("/", ".")
      val base = if (isLibrary(cls)) lib_path else path
      val d = Path(base) / Path(analysisPath) / Path(dirname) / Path("graph")
      if (!d.exists)
        return None
      val cfs = new FileInputStream(d.jfile)
      val ip = new ObjectInputStream(cfs)
      val cfg = ip.readObject().asInstanceOf[CFG.Graph[TFType[Annot]]]
      ip.close()
      graphs += (cls,mthd,typ) -> cfg
      Some(cfg)
    }
    if (no_store)
      graphs.get((cls,mthd,typ))
    else {
      loadSuper(cls)
      graphs.get((cls,mthd,typ)).orElse(loadFile)
    }
  }

  def storeGraph(cls:String, mthd: String, typ: MethodType, cfg:CFG.Graph[TFType[Annot]]): Unit = {
    graphs += (cls, mthd, typ) -> cfg
    if (no_store)
      return
    val dirname = cls.replace("/", ".") ++ "." ++ mthd ++ typ.toShort.replace("/", ".")
    val base = if (isLibrary(cls)) lib_path else path
    val d = (Path(base) / Path(analysisPath) / Path(dirname)).createDirectory(force = false, failIfExists = false)
    val gfpath = d / Path("graph")
    if (gfpath.exists)
      return
    val gf = gfpath.createFile(false)
    val cfs = new FileOutputStream(gf.jfile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(cfg)
    op.close()
    storeSuper(cls, superMap(cls))
  }

  private def loadSuper(cls:String): Unit = {
    if (no_store || cls == null)
      return
    if(superMap.keySet contains cls)
      return
    val base = if (isLibrary(cls)) lib_path else path
    val gfpath = (Path(base) / Path(analysisPath) / Path(cls.replace("/", ".")++".super"))
    if (gfpath.exists) {
      val cfs = new FileInputStream(gfpath.jfile)
      val ip = new ObjectInputStream(cfs)
      val sup = ip.readObject().asInstanceOf[String]
      ip.close()
      superMap += cls -> sup
    }
  }

  private def storeSuper(cls:String, sup: String): Unit = {
    if (no_store)
      return
    val base = if (isLibrary(cls)) lib_path else path
    val gfpath = (Path(base) / Path(analysisPath) / Path(cls.replace("/", ".")++".super"))
    if (gfpath.exists)
      return
    val gf = gfpath.createFile(false)
    val cfs = new FileOutputStream(gf.jfile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(sup)
    op.close()
  }

  def saveResult[T](node:Node, value: T): Unit = {
    if (no_store)
      return
    val dirname = node.methodInfo.cls.replace("/", ".") ++ "." ++ node.methodInfo.mthd ++ node.methodInfo.mthdTyp.toShort.replace("/", ".")
    val base = if (isLibrary(node.methodInfo.cls)) lib_path else path
    val d = (Path(base) / Path(analysisPath) / Path(dirname)).createDirectory(force = false, failIfExists = false)
    val gfpath = d / Path(node.id.toString)
    if (gfpath.exists)
      gfpath.delete()
    val gf = gfpath.createFile(false)
    val cfs = new FileOutputStream(gf.jfile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(value)
    op.close()
  }
  def loadResult[T](node:Node): Option[T] = {
    if (no_store)
      None
    else {
      val dirname = node.methodInfo.cls.replace("/", ".")++"."++node.methodInfo.mthd++node.methodInfo.mthdTyp.toShort.replace("/", ".")
      val base = if (isLibrary(node.methodInfo.cls)) lib_path else path
      val d = Path(base) / Path(analysisPath) / Path(dirname) / Path(node.id.toString)
      if (!d.exists) {
//        println(s"result for ${node.methodInfo.mthd}:${node.linenr}:${node.id} not found")
        return None
      }
      val cfs = new FileInputStream(d.jfile)
      val ip = new ObjectInputStream(cfs)

      val value = ip.readObject()
      ip.close()

      value match {
        case t: T =>
//          println(s"result for ${node.methodInfo.mthd}:${node.linenr}:${node.id} loaded")
          Some(t)
        case _ =>
//          println(s"result for ${node.methodInfo.mthd}:${node.linenr}:${node.id} not loaded")
          None
      }
    }
  }
}
