package põder.util

import põder.log.Log

import scala.collection.mutable

object Util {
  object Cell {
    def apply[T](): Cell[T] = new Cell[T]

    def apply[T](t:T): Cell[T] = {
      val c = new Cell[T]
      c.write(t)
      c
    }
  }

  final class Cell[T] extends Serializable{
    var x:Option[T] = None

    def write(t: T): Unit = x = Some(t)

    override def toString: String =
      x.fold("?")(_.toString)
  }
}

object lookup {
  def apply[T,Q](xs:Seq[(T,Q)])(t:T):Option[Q] =
    try {
      val (x,v) = xs.head
      if (t==x) Some(v) else lookup(xs.tail)(t)
    } catch {
      case _: Throwable => None
    }
}

object PerfCount {
  private def tab(s:String): String =
    if (s.isEmpty) s
    else "\t" ++ s.replaceAll("\n","\n\t")

  private class PerfTreeNode(var name: String, var parent: Option[PerfTreeNode] = None) {
    val children: mutable.Map[String, PerfTreeNode] = mutable.Map.empty
    var timeNs : Long = 0
    var started: Option[Long] = None

    def status: String =
      if (children.isEmpty)
        f"{ $name%s: ${timeNs/1000000000d}%.2f s }"
      else {
        f"{ $name%s: ${timeNs / 1000000000d}%.2f s:\n" ++
          tab(children.values.map(_.status).mkString("\n")) ++ "}"
      }
  }
  private val r = new PerfTreeNode("total")
  private var c = r

  private def inTree(n:String, p: PerfTreeNode = c): Boolean =
    p.name == n || p.parent.exists(inTree(n,_))

  def sub[F](n: String, reportTime: Double = Double.MaxValue)(f: =>F): F = synchronized {
    if (!inTree(n)) {
      c = c.children.getOrElseUpdate(n, new PerfTreeNode(n, Some(c)))
      c.started = Some(System.nanoTime())
      val r: F = f
      val t = System.nanoTime()
      c.started match {
        case Some(v) =>
          val nt = t - v
          c.timeNs += nt
          if (reportTime < nt/1000000000d)
            Log.logCommon(f"Subtask $n%s took ${nt/1000000000d}%.2f s.")
          c.started = None
        case None =>
      }
      c = c.parent.get
      r
    } else {
      f
    }
  }

  def timeout(): Unit = {
    val t = System.nanoTime()
    def f(p:PerfTreeNode): Unit = {
      p.started match {
        case Some(v) =>
          p.timeNs += t - v
          p.started = None
        case None =>
      }
      p.parent.foreach(f)
    }
    f(c)
  }

  def timein(): Unit = {
    val t = System.nanoTime()
    def f(p:PerfTreeNode): Unit = {
      if (p.started.isEmpty)
        p.started = Some(t)
      p.parent.foreach(f)
    }
    f(c)
  }

  def status: String =
    r.children.values.map(_.status).mkString("\n")

  def statusFlat: String = {
    val m: mutable.Map[String, Long] = mutable.Map.empty
    def add(p: PerfTreeNode): Unit = {
      m.update(p.name, m.getOrElse(p.name,0L)+p.timeNs)
      p.children.values.foreach(add)
    }
    r.children.values.foreach(add)
    m.map(p => f"${p._1}%s: ${p._2/1000000000d}%.2f s").mkString("\n")
  }
}
