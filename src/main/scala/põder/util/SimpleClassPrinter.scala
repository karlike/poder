package põder.util

import org.objectweb.asm.Opcodes._
import org.objectweb.asm._

object SimpleMethodPrinter extends MethodVisitor(ASM5) {

  override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
    printf("Annotation(%s, %s)\n", desc, visible.toString)
    null
  }

  override def visitAnnotationDefault(): AnnotationVisitor = {
    printf("AnnotationDefault()\n")
    null
  }

  override def visitAttribute(attr: Attribute): Unit = {
    printf("Attribute(%s)\n", attr)
  }

  override def visitInsnAnnotation(typeRef: Int, typePath: TypePath, desc: String, visible: Boolean): AnnotationVisitor = {
    printf("InsnAnnotation(%d, %s, %s, %s)\n", typeRef, typePath, desc, visible.toString)
    null
  }

  override def visitLineNumber(line: Int, start: Label): Unit = {
    printf("LineNumber(%d, %s)\n", line, start)
  }

  override def visitLocalVariableAnnotation(typeRef: Int, typePath: TypePath, start: Array[Label], end: Array[Label], index: Array[Int], desc: String, visible: Boolean): AnnotationVisitor = {
    printf("LocalVariableAnnotation(%d, %s, %s, %s, %s, %s,%s)\n", typeRef, typePath, start.mkString("[", ", ", "]"), end.mkString("[", ", ", "]"), index.mkString("[", ", ", "]"), desc, visible)
    null
  }

  override def visitParameterAnnotation(parameter: Int, desc: String, visible: Boolean): AnnotationVisitor = {
    printf("ParameterAnnotation(%d, %s, %s)\n", parameter, desc, visible)
    null
  }

  override def visitTryCatchAnnotation(typeRef: Int, typePath: TypePath, desc: String, visible: Boolean): AnnotationVisitor = {
    printf("TryCatchAnnotation(%d, %s, %s, %s)\n", typeRef, typePath, desc, visible)
    null
  }

  override def visitTypeAnnotation(typeRef: Int, typePath: TypePath, desc: String, visible: Boolean): AnnotationVisitor = {
    printf("TypeAnnotation(%d, %s, %s, %s)\n", typeRef, typePath, desc, visible)
    null
  }

  override def visitCode(): Unit = {
    printf("Code()\n")
  }

  // generic instructions
  override def visitInsn(opcode: Int): Unit = {
    printf("Insn(%d)\n", opcode)
  }

  // ???
  override def visitFrame(`type`: Int, nLocal: Int, local: Array[AnyRef], nStack: Int, stack: Array[AnyRef]): Unit = {
    printf("Frame(%d, %d, %s, %s, %s)\n", `type`, nLocal, local.mkString("[", ", ", "]"), nStack, stack.mkString("[",", ", "]"))
  }

  // field instructions
  override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String): Unit = {
    printf("FieldInsn(%d, %s, %s, %s)\n", opcode, owner, name, desc)
  }

  // increment instructions
  override def visitIincInsn(`var`: Int, increment: Int): Unit = {
    printf("IincInsn(%d, %d)\n", `var`, increment)
  }

  // integer instructions
  override def visitIntInsn(opcode: Int, operand: Int): Unit = {
    printf("IntInsn(%d, %d)\n", opcode, operand)
  }

  // ???
  override def visitInvokeDynamicInsn(name: String, desc: String, bsm: Handle, bsmArgs: AnyRef*): Unit = {
    printf("InvokeDynamicInsn(%s, %s, %s, %s)\n", name, desc, bsm.toString, bsmArgs.mkString("[", ", ", "]"))
  }

  override def visitJumpInsn(opcode: Int, label: Label): Unit = {
    printf("JumpInsn(%d, %s)\n", opcode, label)
  }

//  override def visitLineNumber(line: Int, start: Label): Unit = {
//    printf("LineNumber(%d, %s)\n", line, start)
//  }

  override def visitLabel(label: Label): Unit = {
    printf("Label(%s)\n", label)
  }

  override def visitLdcInsn(cst: scala.Any): Unit = {
    printf("LdcInsn(%s)\n", cst)
  }

  override def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int): Unit = {
    printf("LocalVariable(%s, %s, %s, %s, %s, %d)\n", name, desc, signature, start, end, index)
  }

  override def visitLookupSwitchInsn(dflt: Label, keys: Array[Int], labels: Array[Label]): Unit = {
    printf("LookupSwitchInsn(%s, %s, %s)\n", dflt, keys.mkString("[", ", ", "]"), labels.mkString("[", ", ", "]"))
  }

  override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean): Unit = {
    printf("MethodInsn(%d, %s, %s, %s, %b)\n", opcode, owner, name, desc, itf)
  }

  override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String): Unit = {
    printf("MethodInsn(%d, %s, %s, %s)\n", opcode, owner, name, desc)
  }

  override def visitVarInsn(opcode: Int, `var`: Int): Unit = {
    printf("VarInsn(%d, %d)\n", opcode, `var`)
  }

  override def visitTypeInsn(opcode: Int, `type`: String): Unit = {
    printf("TypeInsn(%d, %s)\n", opcode, `type`)
  }

  override def visitTryCatchBlock(start: Label, end: Label, handler: Label, `type`: String): Unit = {
    printf("TryCatchBlock(%s, %s, %s, %s)\n", start, end, handler, `type`)
  }

  override def visitTableSwitchInsn(min: Int, max: Int, dflt: Label, labels: Label*): Unit = {
    printf("TableSwitchInsn(%d, %d, %s, %s)\n", min, max, dflt, labels.mkString("{", ",","}"))
  }

  override def visitParameter(name: String, access: Int): Unit = {
    printf("Parameter(%s, %d)\n", name, access)
  }

  override def visitMultiANewArrayInsn(desc: String, dims: Int): Unit = {
    printf("MultiANewArrayInsn(%s, %d)\n", desc, dims)
  }

  override def visitMaxs(maxStack: Int, maxLocals: Int): Unit = {
    printf("Maxs(%d, %d)\n", maxStack, maxLocals)
  }

  override def visitEnd(): Unit = {
    printf("End\n\n")
  }
}

object SimpleClassPrinter extends ClassVisitor(ASM5) {
  override def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]): Unit = {
    printf("Class(%d, %d, %s, %s, %s, %s)\n", version, access, name, signature, superName, interfaces.mkString("[", ", ","]"))
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor = {
    printf("Field(%d, %s, %s, %s, %s)\n", access, name, desc, signature, value)
    null
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
    val ex = Option(exceptions) match {
      case Some(x) => x.mkString("[", ", ","]")
      case None    => "null"
    }
    printf("Field(%d, %s, %s, %s, %s)\n", access, name, desc, signature, ex)
    SimpleMethodPrinter
  }

//  override def visitSource(source: String, debug: String): Unit = {
//    println(s"$source")
//  }

  override def visitEnd(): Unit = {
    println("EndClass")
  }
}
