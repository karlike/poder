package põder.util

import põder.dom.Logic._
import põder.dom._
import smtlib.trees.Commands._
import smtlib.trees.CommandsResponses._
import smtlib.trees.Terms._

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent._


//object Test {
//
//  def vars(ts:SExpr): Set[String] = {
//    ts match {
//      case Let(binding, bindings, term) => {
//        val body = vars(term)
//        val defs = (binding +: bindings).map(_.name.name).toSet
//        val r = (binding +: bindings).foldLeft(Set.empty[String]) {
//          case (x, y) => x ++ vars(y.term)
//        }
//        (body -- defs) ++ r
//      }
//      case Forall(sortedVar, sortedVars, term) => {
//        val body = vars(term)
//        val defs = (sortedVar +: sortedVars).map(_.name.name).toSet
//        body -- defs
//      }
//      case Exists(sortedVar, sortedVars, term) => {
//        val body = vars(term)
//        val defs = (sortedVar +: sortedVars).map(_.name.name).toSet
//        body -- defs
//      }
//      case QualifiedIdentifier(id, sort) => Set(id.symbol.name)
//      case AnnotatedTerm(term, attribute, attributes) => vars(term)
//      case FunctionApplication(fun, terms) =>
//        terms.map(vars).toSet.flatten
//      case _ => Set.empty
//    }
//  }
//
//  def substB(f: String => Term)(varBinding: VarBinding): VarBinding = {
//    varBinding.copy(term = subst(f)(varBinding.term))
//  }
//
//  def substN(f: String => String): Term => Term = {
//    subst{ n => QualifiedIdentifier(Identifier(SSymbol(f(n))))}
//  }
//
//  def subst(f: String => Term)(t:Term): Term = t match {
//    case Let(binding, bindings, term) => Let(substB(f)(binding),bindings.map(substB(f)),subst(f)(term))
//    case Forall(sortedVar, sortedVars, term) => Forall(sortedVar, sortedVars, subst(f)(term))
//    case Exists(sortedVar, sortedVars, term) => Exists(sortedVar, sortedVars, subst(f)(term))
//    case QualifiedIdentifier(id, sort) => f(id.symbol.name)
//    case AnnotatedTerm(term, attribute, attributes) => AnnotatedTerm(subst(f)(term), attribute, attributes)
//    case FunctionApplication(fun, terms) => FunctionApplication(fun, terms.map(subst(f)))
//    case _ => t
//  }
//
//  def reduce(pats:Seq[Term],x: Set[Term]): Seq[(Term,Seq[String])] = {
//    val magic = "___"
//    val intvars = Set("i", "j", "k", "n")
//    val patvars = Set("x", "y", "z")
//    val v = x.flatMap(vars)
//    for {
//      pat <- pats
//      ar  =  vars(pat) intersect patvars
//      ir  =  vars(pat) intersect intvars
//      p   =  substN(x => if (ir contains x) x++magic else x)(pat)
//      xs  <- v.toSeq.combinations(ar.size)
//      as  =  patvars zip xs
//    } yield {
//      (subst{ x =>
//        as find (_._1 == x) match {
//          case Some(value) =>
//            QualifiedIdentifier(Identifier(SSymbol(value._2)))
//          case None =>
//            QualifiedIdentifier(Identifier(SSymbol(x)))
//        }
//      }(p), ir.map(_++magic).toSeq)
//    }
//  }
//}

class Tester {
  val z3 = SMTHelper
//  def reducer(pats: Seq[Term], x: Set[Term]): Set[Term] = {
//    var d = Set.empty[Term]
//    z3.push()
//    x.foreach(z3.assert)
//    if (z3.sat().contains(true)) {
//      for {(t,is) <- Test.reduce(pats, x)} {
//        z3.push()
//        is.foreach(z3.declareConst(_,"Int"))
//        z3.assert(t)
//        if (z3.sat().contains(true)) {
//          val as = z3.getAssignment(is)
//          d += Test.subst(x => lookup(as)(x).fold[Term](QualifiedIdentifier(Identifier(SSymbol(x))))(q => q))(t)
//        }
//        z3.pop()
//      }
//    }
//    z3.pop()
//    d
//  }
}

object SMTHelper {
  var monitor = Option.empty[() => Unit]

  var done = false
  var z3 = smtlib.interpreters.Z3Interpreter.buildDefault

  def eval(s:SExpr): SExpr = z3.eval(s)
  def reset(): Boolean = {
    z3.eval(Reset()).isInstanceOf[SuccessfulResponse]
  }
  def push(): Boolean = {
    z3.eval(Push(1)).isInstanceOf[SuccessfulResponse]
  }
  def pop(): Boolean = {
    z3.eval(Pop(1)).isInstanceOf[SuccessfulResponse]
  }
  def assert(t: Term): Boolean = {
    val q = z3.eval(Assert(t))
//    val out = new PrintWriter(System.out, true)
//    SMTHelper.z3.printer.printSExpr(q, out)
//    out.println()
//    out.flush()
    q.isInstanceOf[SuccessfulResponse]
  }

  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

  def sat(): Option[Boolean] = {
    PerfCount.sub("sat",0.5){
      Await.result(sat_future(),Duration.Inf)
    }
  }

  private def sat_future():Future[Option[Boolean]] = {
    val p = Promise[Option[Boolean]]()
    val f = Future(sat_inner())
    f.onComplete(p.tryComplete)
    ExecutionContext.global.execute(() => {
      def loop(): Unit = {
        if (!p.isCompleted || !f.isCompleted) {
          monitor match {
            case Some(g) => {
              try {
                g()
                Thread.sleep(200)
                loop()
              } catch {
                case ex: Throwable =>
                  p.tryFailure(ex)
                  z3.kill()
                  z3 = smtlib.interpreters.Z3Interpreter.buildDefault
                  push()
              }
            }
            case None =>
              Thread.sleep(1000)
              loop()
          }
        }
      }
      loop()
    })
    p.future
  }

  private def sat_inner():Option[Boolean] = {
    val z = z3.eval(CheckSat())
    z match {
      case status: CheckSatStatus =>
        if (status.status == SatStatus) {
          Some(true)
        }
        else if (status.status == UnsatStatus)
          Some(false)
        else
          None
      case _ => None
    }
  }

  def getAssignment(vars: Seq[String]): Seq[(String,Term)] = {
    def l(x:String): Term = QualifiedIdentifier(Identifier(SSymbol(x)))
    if (vars.isEmpty)
      Seq.empty
    else {
      val r = z3.eval(GetValue(l(vars.head), vars.tail.map(l)))
      r match {
        case GetValueResponseSuccess(x) =>
          x.flatMap {
            case (QualifiedIdentifier(Identifier(SSymbol(z), _), _), y) => Seq((z, y))
            case _ => Seq.empty
          }
        case q => Seq.empty
      }
    }
  }
  def declareConst(x:String, k:String): Boolean =
    z3.eval(DeclareConst(SSymbol(x),Sort(Identifier(SSymbol(k))))).isInstanceOf[SuccessfulResponse]

  def declareFun(x:String, as:Seq[String], r:String): Boolean = {
    val args = as.map(k => Sort(Identifier(SSymbol(k))))
    z3.eval(DeclareFun(SSymbol(x), args, Sort(Identifier(SSymbol(r))))).isInstanceOf[SuccessfulResponse]
  }

  def simplify(t: Term): Term = {
    z3.in.write("(simplify ")
    z3.printer.printSExpr(t, z3.in)
    z3.in.write(")\n")
    z3.in.flush()
    var r = z3.parser.parseTerm
    r
  }

}

object smtTest {
  val z3 = SMTHelper
  val t = new Tester

  def readFile(fn: String): List[Command] = {
    val is = new java.io.FileReader(fn)
    val lexer = new smtlib.lexer.Lexer(is)
    val parser = new smtlib.parser.Parser(lexer)

    val script: List[Command] = {
      var cmds = new ListBuffer[Command]
      var cmd = parser.parseCommand
      while (cmd != null)
        cmds.append(cmd)
      cmds.toList
    }
    script
  }

  def parseTerm(s: String): Term = {
    val is = new java.io.StringReader(s)
    val lexer = new smtlib.lexer.Lexer(is)
    val parser = new smtlib.parser.Parser(lexer)
    parser.parseTerm
  }

  import LogicHelper._

  def printAndCheck(x: põder.dom.Logic, y: põder.dom.Logic): Unit = {
    print(s"state:${x.toSimpleString}  \tquery:${y.toSimpleString}")
    println("  \tresult: " ++ x.query(y).toString)
  }

  def main(sts:Array[String]): Unit = {
    val c = SVar(0)
    val d = SVar(1)
    val eq = BinIntProp("=")
    val dirty = IProp("d")
//    printAndCheck(eq(c,d), True)
//    printAndCheck(eq(c,d), False)
//    printAndCheck(eq(c,d), eq(c,d))
//    printAndCheck(eq(c,d), eq(c,IVal(5)))
//    printAndCheck(True, True)
//    printAndCheck(True, False)
//    printAndCheck(True, dirty(c))
//    printAndCheck(False, True)
//    printAndCheck(False, False)
//    printAndCheck(False, dirty(c))
//    printAndCheck(dirty(c), True)
//    printAndCheck(dirty(c), False)
//    printAndCheck(dirty(c), dirty(c))
//    printAndCheck(dirty(c), dirty(d))
  }

//  def main(sts:Array[String]): Unit = {
//    val a = app(LVar(1))
//    val b = app(LVar(2))
//    val c = app(LVar(3))
//    val a0 = app(BinPropOp("="), a, IVal(10))
//    val a1 = and(app(BinPropOp("="), a, IVal(1)),app(BinPropOp("="), b, IVal(2)))
//    val a2 = and(app(BinPropOp("="), a, IVal(2)),app(BinPropOp("="), b, IVal(3)))
//    val a3 = and(app(BinPropOp("="), a, IVal(3)),app(BinPropOp("="), b, IVal(4)))
//    val av = insertForall(a0)
//
//    val pa = and(app(BinPropOp(">="), a, app(Ident("a",IntType))), not(app(BinPropOp("="), IVal(0), app(Ident("a",IntType)))),not(app(BinPropOp("="), IVal(1), app(Ident("a",IntType)))))
//
//    val q = insertExists(inForall(av => or(pa,not(av)))(av))
//    z3.push()
//    z3.eval(SetOption(ProduceAssignments(true)))
//    z3.declareConst("a","Int")
//    val q1 = LogLat.tr(q)
//    println(smtlib.printer.RecursivePrinter.toString(q1))
//    z3.assert(q1)
//    if (z3.sat().contains(true)) {
//      printf("sat\n")
//      val z = z3.getAssignment(Seq("a"))
////          val z = t.reducer(Seq(p1, p2), Set(a1))
//      z.map(_._2).foreach(q =>
//        printf("result -- %s\n", smtlib.printer.RecursivePrinter.toString(q)))
//    }
//    z3.pop()
//  }
}
