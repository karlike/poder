package põder.util.typ

object Type {
  def fromAsmBase(t:org.objectweb.asm.Type): Type = {
    t.getSort match {
      case org.objectweb.asm.Type.VOID => VoidT
      case org.objectweb.asm.Type.BOOLEAN => BoolT
      case org.objectweb.asm.Type.CHAR => CharT
      case org.objectweb.asm.Type.BYTE => ByteT
      case org.objectweb.asm.Type.SHORT => ShortT
      case org.objectweb.asm.Type.INT => IntT
      case org.objectweb.asm.Type.FLOAT => FloatT
      case org.objectweb.asm.Type.LONG => LongT
      case org.objectweb.asm.Type.DOUBLE => DoubleT
      case org.objectweb.asm.Type.ARRAY => ArrayT(fromAsmBase(t.getElementType))
      case org.objectweb.asm.Type.OBJECT => ObjectT(t.getClassName)
    }
  }

  def fromAsm(t:org.objectweb.asm.Type): Either[Type,MethodType] = {
    t.getSort match {
      case org.objectweb.asm.Type.METHOD  =>
        Right(MethodType(t.getArgumentTypes.map(fromAsmBase).toList, fromAsmBase(t.getReturnType)))
      case _ => Left(fromAsmBase(t))
    }
  }
}

sealed abstract class Type {
  def toText:String
  def toShort:String
}
case object VoidT            extends Type {
  override def toText: String = "void"
  override def toShort: String = "V"
}
case object BoolT            extends Type {
  override def toText: String = "bool"
  override def toShort: String = "Z"
}
case object CharT            extends Type {
  override def toText: String = "char"
  override def toShort: String = "C"
}
case object ByteT            extends Type {
  override def toText: String = "byte"
  override def toShort: String = "B"
}
case object ShortT           extends Type {
  override def toText: String = "short"
  override def toShort: String = "S"
}
case object IntT             extends Type {
  override def toText: String = "int"
  override def toShort: String = "I"
}
case object FloatT           extends Type {
  override def toText: String = "float"
  override def toShort: String = "F"
}
case object LongT            extends Type {
  override def toText: String = "long"
  override def toShort: String = "J"
}
case object DoubleT          extends Type {
  override def toText: String = "double"
  override def toShort: String = "D"
}
case class ObjectT(s:String) extends Type {
  override def toText: String = s
  override def toShort: String = "L"++s++";"
}
case class ArrayT(e:Type)    extends Type {
  def toText: String = e.toText++"[]"
  override def toShort: String = "["++e.toShort
}

case class MethodType(args: List[Type], ret: Type) {
  def toShort: String = {
    "("++args.map(_.toShort).mkString("")++")"++ret.toShort
  }
  def toText: String = {
    "("++args.map(_.toText).mkString(",")++")"++ret.toText
  }
}

object Parser {
  object ParseError extends Throwable

  type P[T] = String => (T,String)

  def tpar: P[Type] = s =>
    s.head match {
      case 'V' => (VoidT,s.tail)
      case 'Z' => (BoolT,s.tail)
      case 'C' => (CharT,s.tail)
      case 'B' => (ByteT,s.tail)
      case 'S' => (ShortT,s.tail)
      case 'I' => (IntT,s.tail)
      case 'F' => (FloatT,s.tail)
      case 'J' => (LongT,s.tail)
      case 'D' => (DoubleT,s.tail)
      case 'L' =>
        val (c,r) = s.tail.span(_!=';')
        (ObjectT(c),r.tail)
      case '[' =>
        val (t,r) = tpar(s.tail)
        (ArrayT(t),r)
    }

  def tryp[T](d: T)(p: P[T]): P[T] = s =>
    if (s.isEmpty)
      (d, s)
    else try {
      p(s)
    } catch {
      case _: Throwable => (d, s)
    }

  def repeatp[T](p: P[T]): P[List[T]] = s =>
    try {
      val (t,r) = p(s)
      val (y,e) = repeatp(p)(r)
      (t::y, e)
    } catch {
      case _: Throwable => (List(), s)
    }

  def charp(c: Char): P[Char] = s => {
    if (s.head == c) (c, s.tail)
    else throw ParseError
  }

  def pure[T](x:T): P[T] = (x,_)


  implicit class Parser[T](val p:P[T]) {
    def @>[Q](f: T => Q): P[Q] = s => {
      val (x, r) = p(s)
      (f(x), r)
    }

    def <*>[Q](q: P[Q]): P[(T,Q)] = s => {
      val (x, r) = p(s)
      val (y, t) = q(r)
      ((x,y), t)
    }

    def *>[Q](q: P[Q]): P[Q] =
      (p <*> q) @> (_._2)


    def <*[Q](q: P[Q]): P[T] =
      (p <*> q) @> (_._1)

    def parse (s:String): T =
      p(s)._1
  }


  def mpar(s:String): MethodType = {
    val parser =
      (charp('(') *> repeatp(tpar) <* charp(')') <*> tpar) @> {case (q, t) => MethodType(q,t)}
    parser parse s
  }
}
