name := "põder"
version := "1.0"

scalaVersion := "2.12.10"

fork in (IntegrationTest, run) := true

libraryDependencies += "org.ow2.asm" % "asm" % "7.1"
libraryDependencies += "com.github.vlsi.mxgraph" % "jgraphx" % "3.9.8.1"
libraryDependencies += "com.regblanc" %% "scala-smtlib" % "0.2.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"

// scalafx

libraryDependencies += "org.scalafx" %% "scalafx" % "11-R16"

// Determine OS version of JavaFX binaries
lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux")   => "linux"
  case n if n.startsWith("Mac")     => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}

lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
libraryDependencies ++= javaFXModules.map( m =>
  "org.openjfx" % s"javafx-$m" % "11" classifier osName
)

assemblyJarName in assembly := name.value++"-"++version.value++".jar"

mainClass in Compile := Some(name.value++".P6der")

// META-INF discarding
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}